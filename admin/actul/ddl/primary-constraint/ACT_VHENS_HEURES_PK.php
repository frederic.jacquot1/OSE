<?php

//@formatter:off

return [
    'name'    => 'ACT_VHENS_HEURES_PK',
    'table'   => 'ACT_VHENS_HEURES',
    'index'   => 'ACT_VHENS_HEURES_PK',
    'columns' => [
        'Z_ELEMENT_PEDAGOGIQUE_ID',
        'Z_TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
