<?php

//@formatter:off

return [
    'name'    => 'ACT_ETAPE_PK',
    'table'   => 'ACT_ETAPE',
    'index'   => 'ACT_ETAPE_PK',
    'columns' => [
        'ANNEE_ID',
        'SOURCE_CODE',
    ],
];

//@formatter:on
