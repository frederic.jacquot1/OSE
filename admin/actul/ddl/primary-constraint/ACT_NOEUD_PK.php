<?php

//@formatter:off

return [
    'name'    => 'ACT_NOEUD_PK',
    'table'   => 'ACT_NOEUD',
    'index'   => 'ACT_NOEUD_PK',
    'columns' => [
        'SOURCE_CODE',
    ],
];

//@formatter:on
