<?php

//@formatter:off

return [
    'name'    => 'ACT_LIEN_PK',
    'table'   => 'ACT_LIEN',
    'index'   => 'ACT_LIEN_PK',
    'columns' => [
        'Z_NOEUD_SUP_ID',
        'Z_NOEUD_INF_ID',
    ],
];

//@formatter:on
