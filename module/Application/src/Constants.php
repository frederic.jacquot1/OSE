<?php

namespace Application;

class Constants
{
    const DATE_FORMAT     = 'd/m/Y';
    const DATETIME_FORMAT = 'd/m/Y H:i';
    const DATE_SORT       = 'ymd';
    const BDD             = 'doctrine.entitymanager.orm_default';
}