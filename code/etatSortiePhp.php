<?php
/**
 * @var $document    Unicaen\OpenDocument\Document    Document permettant de travailler à sa mise en page
 * @var $etatSortie    Application\Entity\Db\EtatSortie    Etat de sortie
 * @var $data    array    Tableau de données issu de la requête exécutée
 * @var $filtres    array    Liste des filtres avec les valeurs transmises (format COLONNE => Valeur)
 * @var $entityManager    Doctrine\ORM\EntityManager    Gestionaire d'entités Doctrine (permet d'accéder à la base de données)
 * @var $options
 *
 * Pour l'utiliser, mettre en traitement PHP le contenu suivant :
 * UnicaenCode:etatSortiePhp
 *
 * /!\ Une fois au point, ne pas copier le présent commentaire ni la balise d'ouverture du PHP "<?php", ne copier que le code ci-dessous
 */