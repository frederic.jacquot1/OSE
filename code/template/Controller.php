<?php

namespace <namespace>;

use Application\Controller\AbstractController;


/**
 * Description of <classname>
 *
 * @author <author>
 */
class <classname> extends AbstractController
{

    public function indexAction()
    {
        return [];
    }

}