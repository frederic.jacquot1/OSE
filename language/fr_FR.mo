��          �      �       0     1     H     M     U     l     �     �     �     �     �     �     �  L       P     n     v     ~     �     �  `   �  \     �   {  �     X   �     �                     	       
                              Additional information File Message No Exception available Previous exceptions Stack trace bdd .EP_CODE__UN bdd .ETAPE_SOURCE_UN bdd .MEP_FR_SERVICE_FK bdd .MEP_FR_SERVICE_REF_FK bdd .SERVICE__UN bdd ORA-01722 Project-Id-Version: OSE
POT-Creation-Date: 2019-02-19 17:47+0100
PO-Revision-Date: 2019-02-19 17:50+0100
Last-Translator: Laurent Lécluse <laurent.lecluse@unicaen.fr>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: translate;setLabel;setLegend;_
X-Poedit-SearchPath-0: module/Application
X-Poedit-SearchPathExcluded-0: public/vendor
X-Poedit-SearchPathExcluded-1: vendor/unicaen/import/public/ace-builds-master
 Informations supplémentaires Fichier Message Aucune erreur trouvée Erreurs précédentes Pile d'exécution Un enseignement existe déjà avec le même code. Merci de choisir un autre code d'enseignement. Une formation existe déjà avec le même code. Merci de choisir un autre code de formation. Vous ne pouvez pas dévalider ces heures d'enseignement, car des demandes de mise en paiement ou des mises en paiement ont été faites. Vous ne pouvez pas dévalider ces heures de référentiel, car des demandes de mise en paiement ou des mises en paiement ont été faites. Vous ne pouvez pas enregistrer cet enseignement car il en existe déjà un de similaire. Nombre invalide 