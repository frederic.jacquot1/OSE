<?php

//@formatter:off

return [
    'name'        => 'MISSION_HCFK',
    'table'       => 'MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
