<?php

//@formatter:off

return [
    'name'        => 'TBL_PAIEMENT_MISSION_FK',
    'table'       => 'TBL_PAIEMENT',
    'rtable'      => 'MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MISSION_ID' => 'ID',
    ],
];

//@formatter:on
