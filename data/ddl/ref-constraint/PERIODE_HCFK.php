<?php

//@formatter:off

return [
    'name'        => 'PERIODE_HCFK',
    'table'       => 'PERIODE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
