<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_HMFK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
