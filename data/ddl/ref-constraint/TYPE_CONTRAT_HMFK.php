<?php

//@formatter:off

return [
    'name'        => 'TYPE_CONTRAT_HMFK',
    'table'       => 'TYPE_CONTRAT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
