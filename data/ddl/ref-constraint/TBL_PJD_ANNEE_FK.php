<?php

//@formatter:off

return [
    'name'        => 'TBL_PJD_ANNEE_FK',
    'table'       => 'TBL_PIECE_JOINTE_DEMANDE',
    'rtable'      => 'ANNEE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
