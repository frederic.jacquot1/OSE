<?php

//@formatter:off

return [
    'name'        => 'EMPLOYEUR_HCFK',
    'table'       => 'EMPLOYEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
