<?php

//@formatter:off

return [
    'name'        => 'TYPE_HEURES_TYPE_HEURES_FK',
    'table'       => 'TYPE_HEURES',
    'rtable'      => 'TYPE_HEURES',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_HEURES_ELEMENT_ID' => 'ID',
    ],
];

//@formatter:on
