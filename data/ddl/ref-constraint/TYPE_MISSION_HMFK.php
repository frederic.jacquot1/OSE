<?php

//@formatter:off

return [
    'name'        => 'TYPE_MISSION_HMFK',
    'table'       => 'TYPE_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
