<?php

//@formatter:off

return [
    'name'        => 'AGREMENT_STRUCTURE_FK',
    'table'       => 'AGREMENT',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
