<?php

//@formatter:off

return [
    'name'        => 'MISSION_TYPE_MISSION_FK',
    'table'       => 'MISSION',
    'rtable'      => 'TYPE_MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_MISSION_ID' => 'ID',
    ],
];

//@formatter:on
