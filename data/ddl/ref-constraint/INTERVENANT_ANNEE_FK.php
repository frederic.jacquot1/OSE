<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_ANNEE_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
