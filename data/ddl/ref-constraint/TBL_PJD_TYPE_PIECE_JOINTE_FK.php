<?php

//@formatter:off

return [
    'name'        => 'TBL_PJD_TYPE_PIECE_JOINTE_FK',
    'table'       => 'TBL_PIECE_JOINTE_DEMANDE',
    'rtable'      => 'TYPE_PIECE_JOINTE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_PIECE_JOINTE_ID' => 'ID',
    ],
];

//@formatter:on
