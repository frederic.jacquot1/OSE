<?php

//@formatter:off

return [
    'name'        => 'PIECE_JOINTE_FICHIER_PJFK',
    'table'       => 'PIECE_JOINTE_FICHIER',
    'rtable'      => 'PIECE_JOINTE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'PIECE_JOINTE_ID' => 'ID',
    ],
];

//@formatter:on
