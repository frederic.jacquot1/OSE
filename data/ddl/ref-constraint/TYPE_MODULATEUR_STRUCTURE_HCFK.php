<?php

//@formatter:off

return [
    'name'        => 'TYPE_MODULATEUR_STRUCTURE_HCFK',
    'table'       => 'TYPE_MODULATEUR_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
