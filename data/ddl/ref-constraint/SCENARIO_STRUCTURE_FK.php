<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_STRUCTURE_FK',
    'table'       => 'SCENARIO',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
