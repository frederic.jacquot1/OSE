<?php

//@formatter:off

return [
    'name'        => 'SERVICE_HMFK',
    'table'       => 'SERVICE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
