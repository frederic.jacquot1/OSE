<?php

//@formatter:off

return [
    'name'        => 'PAYS_HCFK',
    'table'       => 'PAYS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
