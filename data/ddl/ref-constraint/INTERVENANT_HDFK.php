<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_HDFK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
