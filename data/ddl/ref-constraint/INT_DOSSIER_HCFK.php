<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_HCFK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
