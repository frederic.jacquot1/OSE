<?php

//@formatter:off

return [
    'name'        => 'VHR_SERVICE_REFERENTIEL_FK',
    'table'       => 'VOLUME_HORAIRE_REF',
    'rtable'      => 'SERVICE_REFERENTIEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SERVICE_REFERENTIEL_ID' => 'ID',
    ],
];

//@formatter:on
