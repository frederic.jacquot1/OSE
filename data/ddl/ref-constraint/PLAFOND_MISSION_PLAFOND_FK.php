<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_MISSION_PLAFOND_FK',
    'table'       => 'PLAFOND_MISSION',
    'rtable'      => 'PLAFOND',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ID' => 'ID',
    ],
];

//@formatter:on
