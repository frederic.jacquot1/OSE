<?php

//@formatter:off

return [
    'name'        => 'VHIT_FK',
    'table'       => 'VOLUME_HORAIRE',
    'rtable'      => 'TYPE_INTERVENTION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENTION_ID' => 'ID',
    ],
];

//@formatter:on
