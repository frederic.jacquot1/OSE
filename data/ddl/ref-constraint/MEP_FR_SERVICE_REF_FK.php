<?php

//@formatter:off

return [
    'name'        => 'MEP_FR_SERVICE_REF_FK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'FORMULE_RESULTAT_SERVICE_REF',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'FORMULE_RES_SERVICE_REF_ID' => 'ID',
    ],
];

//@formatter:on
