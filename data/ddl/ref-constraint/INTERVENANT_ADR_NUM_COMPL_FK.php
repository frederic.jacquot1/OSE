<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_ADR_NUM_COMPL_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'ADRESSE_NUMERO_COMPL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_NUMERO_COMPL_ID' => 'ID',
    ],
];

//@formatter:on
