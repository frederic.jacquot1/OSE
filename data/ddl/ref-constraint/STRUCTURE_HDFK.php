<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_HDFK',
    'table'       => 'STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
