<?php

//@formatter:off

return [
    'name'        => 'TYPE_MISSION_ANNEE_FK',
    'table'       => 'TYPE_MISSION',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
