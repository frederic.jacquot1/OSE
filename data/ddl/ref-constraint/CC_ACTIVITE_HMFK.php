<?php

//@formatter:off

return [
    'name'        => 'CC_ACTIVITE_HMFK',
    'table'       => 'CC_ACTIVITE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
