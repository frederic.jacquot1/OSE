<?php

//@formatter:off

return [
    'name'        => 'LIEN_SOURCE_FK',
    'table'       => 'LIEN',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
