<?php

//@formatter:off

return [
    'name'        => 'SERVICE_REFERENTIEL_TAG_FK',
    'table'       => 'SERVICE_REFERENTIEL',
    'rtable'      => 'TAG',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TAG_ID' => 'ID',
    ],
];

//@formatter:on
