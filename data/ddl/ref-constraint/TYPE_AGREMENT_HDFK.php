<?php

//@formatter:off

return [
    'name'        => 'TYPE_AGREMENT_HDFK',
    'table'       => 'TYPE_AGREMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
