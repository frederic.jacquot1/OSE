<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STRUCT_ETAT_REALISE_FK',
    'table'       => 'PLAFOND_STRUCTURE',
    'rtable'      => 'PLAFOND_ETAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ETAT_REALISE_ID' => 'ID',
    ],
];

//@formatter:on
