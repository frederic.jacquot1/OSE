<?php

//@formatter:off

return [
    'name'        => 'TBCH_ETAPE_FKV1',
    'table'       => 'TBL_CHARGENS',
    'rtable'      => 'ETAPE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ETAPE_ENS_ID' => 'ID',
    ],
];

//@formatter:on
