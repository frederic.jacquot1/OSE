<?php

//@formatter:off

return [
    'name'        => 'TBL_PLA_INT_ETAT_FK',
    'table'       => 'TBL_PLAFOND_INTERVENANT',
    'rtable'      => 'PLAFOND_ETAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ETAT_ID' => 'ID',
    ],
];

//@formatter:on
