<?php

//@formatter:off

return [
    'name'        => 'VHMNP_FK',
    'table'       => 'VOLUME_HORAIRE',
    'rtable'      => 'MOTIF_NON_PAIEMENT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MOTIF_NON_PAIEMENT_ID' => 'ID',
    ],
];

//@formatter:on
