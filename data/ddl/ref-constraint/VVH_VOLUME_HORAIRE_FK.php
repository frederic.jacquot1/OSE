<?php

//@formatter:off

return [
    'name'        => 'VVH_VOLUME_HORAIRE_FK',
    'table'       => 'VALIDATION_VOL_HORAIRE',
    'rtable'      => 'VOLUME_HORAIRE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VOLUME_HORAIRE_ID' => 'ID',
    ],
];

//@formatter:on
