<?php

//@formatter:off

return [
    'name'        => 'TYPE_RESSOURCE_HCFK',
    'table'       => 'TYPE_RESSOURCE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
