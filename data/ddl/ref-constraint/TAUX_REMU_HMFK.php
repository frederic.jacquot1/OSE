<?php

//@formatter:off

return [
    'name'        => 'TAUX_REMU_HMFK',
    'table'       => 'TAUX_REMU',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
