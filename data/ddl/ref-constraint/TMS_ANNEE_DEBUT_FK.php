<?php

//@formatter:off

return [
    'name'        => 'TMS_ANNEE_DEBUT_FK',
    'table'       => 'TYPE_MODULATEUR_STRUCTURE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_DEBUT_ID' => 'ID',
    ],
];

//@formatter:on
