<?php

//@formatter:off

return [
    'name'        => 'ROLE_HMFK',
    'table'       => 'ROLE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
