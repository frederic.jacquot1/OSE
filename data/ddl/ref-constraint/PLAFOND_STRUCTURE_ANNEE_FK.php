<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STRUCTURE_ANNEE_FK',
    'table'       => 'PLAFOND_STRUCTURE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
