<?php

//@formatter:off

return [
    'name'        => 'TYPE_MISSION_TAUX_REMU_MAJ_FK',
    'table'       => 'TYPE_MISSION',
    'rtable'      => 'TAUX_REMU',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TAUX_REMU_MAJORE_ID' => 'ID',
    ],
];

//@formatter:on
