<?php

//@formatter:off

return [
    'name'        => 'TBL_CSD_GTF_FK',
    'table'       => 'TBL_CHARGENS_SEUILS_DEF',
    'rtable'      => 'GROUPE_TYPE_FORMATION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'GROUPE_TYPE_FORMATION_ID' => 'ID',
    ],
];

//@formatter:on
