<?php

//@formatter:off

return [
    'name'        => 'TBL_LIEN_SCENARIO_FK',
    'table'       => 'TBL_LIEN',
    'rtable'      => 'SCENARIO',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SCENARIO_ID' => 'ID',
    ],
];

//@formatter:on
