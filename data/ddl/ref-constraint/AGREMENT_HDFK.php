<?php

//@formatter:off

return [
    'name'        => 'AGREMENT_HDFK',
    'table'       => 'AGREMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
