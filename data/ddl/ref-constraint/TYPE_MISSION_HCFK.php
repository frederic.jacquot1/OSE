<?php

//@formatter:off

return [
    'name'        => 'TYPE_MISSION_HCFK',
    'table'       => 'TYPE_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
