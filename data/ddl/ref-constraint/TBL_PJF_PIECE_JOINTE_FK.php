<?php

//@formatter:off

return [
    'name'        => 'TBL_PJF_PIECE_JOINTE_FK',
    'table'       => 'TBL_PIECE_JOINTE_FOURNIE',
    'rtable'      => 'PIECE_JOINTE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'PIECE_JOINTE_ID' => 'ID',
    ],
];

//@formatter:on
