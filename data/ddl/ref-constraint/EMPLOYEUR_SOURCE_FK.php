<?php

//@formatter:off

return [
    'name'        => 'EMPLOYEUR_SOURCE_FK',
    'table'       => 'EMPLOYEUR',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
