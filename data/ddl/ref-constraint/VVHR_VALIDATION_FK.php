<?php

//@formatter:off

return [
    'name'        => 'VVHR_VALIDATION_FK',
    'table'       => 'VALIDATION_VOL_HORAIRE_REF',
    'rtable'      => 'VALIDATION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
