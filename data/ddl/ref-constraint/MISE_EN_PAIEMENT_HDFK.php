<?php

//@formatter:off

return [
    'name'        => 'MISE_EN_PAIEMENT_HDFK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
