<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_EP_HDFK',
    'table'       => 'CENTRE_COUT_EP',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
