<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_ADR_NUMERO_COMPL',
    'table'       => 'STRUCTURE',
    'rtable'      => 'ADRESSE_NUMERO_COMPL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_NUMERO_COMPL_ID' => 'ID',
    ],
];

//@formatter:on
