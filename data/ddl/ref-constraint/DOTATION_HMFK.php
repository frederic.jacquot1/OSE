<?php

//@formatter:off

return [
    'name'        => 'DOTATION_HMFK',
    'table'       => 'DOTATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
