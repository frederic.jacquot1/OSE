<?php

//@formatter:off

return [
    'name'        => 'TAUX_REMU_HCFK',
    'table'       => 'TAUX_REMU',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
