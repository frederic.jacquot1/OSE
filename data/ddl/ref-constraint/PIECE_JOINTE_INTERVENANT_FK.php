<?php

//@formatter:off

return [
    'name'        => 'PIECE_JOINTE_INTERVENANT_FK',
    'table'       => 'PIECE_JOINTE',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
