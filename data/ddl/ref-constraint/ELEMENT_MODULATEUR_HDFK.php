<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_MODULATEUR_HDFK',
    'table'       => 'ELEMENT_MODULATEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
