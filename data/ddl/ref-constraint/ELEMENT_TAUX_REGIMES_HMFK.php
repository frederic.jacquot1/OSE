<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_TAUX_REGIMES_HMFK',
    'table'       => 'ELEMENT_TAUX_REGIMES',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
