<?php

//@formatter:off

return [
    'name'        => 'TAUX_REMU_HDFK',
    'table'       => 'TAUX_REMU',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
