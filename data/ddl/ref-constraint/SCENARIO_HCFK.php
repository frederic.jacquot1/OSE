<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_HCFK',
    'table'       => 'SCENARIO',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
