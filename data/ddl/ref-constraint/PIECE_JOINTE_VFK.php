<?php

//@formatter:off

return [
    'name'        => 'PIECE_JOINTE_VFK',
    'table'       => 'PIECE_JOINTE',
    'rtable'      => 'VALIDATION',
    'delete_rule' => 'SET NULL',
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
