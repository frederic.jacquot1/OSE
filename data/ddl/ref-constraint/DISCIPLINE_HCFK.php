<?php

//@formatter:off

return [
    'name'        => 'DISCIPLINE_HCFK',
    'table'       => 'DISCIPLINE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
