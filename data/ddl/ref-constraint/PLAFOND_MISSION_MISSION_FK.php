<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_MISSION_MISSION_FK',
    'table'       => 'PLAFOND_MISSION',
    'rtable'      => 'TYPE_MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_MISSION_ID' => 'ID',
    ],
];

//@formatter:on
