<?php

//@formatter:off

return [
    'name'        => 'MOTIF_MODIFICATION_SERVIC_HMFK',
    'table'       => 'MOTIF_MODIFICATION_SERVICE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
