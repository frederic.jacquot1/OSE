<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_DEROGATION_HMFK',
    'table'       => 'PLAFOND_DEROGATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
