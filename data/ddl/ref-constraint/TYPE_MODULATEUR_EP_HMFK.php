<?php

//@formatter:off

return [
    'name'        => 'TYPE_MODULATEUR_EP_HMFK',
    'table'       => 'TYPE_MODULATEUR_EP',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
