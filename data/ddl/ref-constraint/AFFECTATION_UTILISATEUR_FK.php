<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_UTILISATEUR_FK',
    'table'       => 'AFFECTATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'UTILISATEUR_ID' => 'ID',
    ],
];

//@formatter:on
