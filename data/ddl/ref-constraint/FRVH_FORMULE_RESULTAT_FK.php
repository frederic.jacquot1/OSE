<?php

//@formatter:off

return [
    'name'        => 'FRVH_FORMULE_RESULTAT_FK',
    'table'       => 'FORMULE_RESULTAT_VH',
    'rtable'      => 'FORMULE_RESULTAT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'FORMULE_RESULTAT_ID' => 'ID',
    ],
];

//@formatter:on
