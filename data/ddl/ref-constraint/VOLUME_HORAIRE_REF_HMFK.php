<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_REF_HMFK',
    'table'       => 'VOLUME_HORAIRE_REF',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
