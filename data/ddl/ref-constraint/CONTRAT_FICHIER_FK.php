<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_FICHIER_FK',
    'table'       => 'CONTRAT_FICHIER',
    'rtable'      => 'CONTRAT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'CONTRAT_ID' => 'ID',
    ],
];

//@formatter:on
