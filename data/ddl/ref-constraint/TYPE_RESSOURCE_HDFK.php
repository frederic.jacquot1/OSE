<?php

//@formatter:off

return [
    'name'        => 'TYPE_RESSOURCE_HDFK',
    'table'       => 'TYPE_RESSOURCE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
