<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_ANNEE_FKV1',
    'table'       => 'TYPE_INTERVENTION',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_FIN_ID' => 'ID',
    ],
];

//@formatter:on
