<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_ADR_VOIRIE_FK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'VOIRIE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_VOIRIE_ID' => 'ID',
    ],
];

//@formatter:on
