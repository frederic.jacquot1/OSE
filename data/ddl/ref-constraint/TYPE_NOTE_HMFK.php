<?php

//@formatter:off

return [
    'name'        => 'TYPE_NOTE_HMFK',
    'table'       => 'TYPE_NOTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
