<?php

//@formatter:off

return [
    'name'        => 'VH_CHARGE_HDFK',
    'table'       => 'VOLUME_HORAIRE_CHARGE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
