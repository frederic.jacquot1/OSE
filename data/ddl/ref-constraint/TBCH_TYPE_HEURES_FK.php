<?php

//@formatter:off

return [
    'name'        => 'TBCH_TYPE_HEURES_FK',
    'table'       => 'TBL_CHARGENS',
    'rtable'      => 'TYPE_HEURES',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_HEURES_ID' => 'ID',
    ],
];

//@formatter:on
