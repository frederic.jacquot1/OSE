<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_HMFK',
    'table'       => 'AFFECTATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
