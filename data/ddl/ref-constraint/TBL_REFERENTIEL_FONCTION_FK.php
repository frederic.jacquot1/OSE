<?php

//@formatter:off

return [
    'name'        => 'TBL_REFERENTIEL_FONCTION_FK',
    'table'       => 'TBL_REFERENTIEL',
    'rtable'      => 'FONCTION_REFERENTIEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'FONCTION_REFERENTIEL_ID' => 'ID',
    ],
];

//@formatter:on
