<?php

//@formatter:off

return [
    'name'        => 'VOIRIE_SOURCE_FK',
    'table'       => 'VOIRIE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
