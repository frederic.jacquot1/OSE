<?php

//@formatter:off

return [
    'name'        => 'STATUT_HCFK',
    'table'       => 'STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
