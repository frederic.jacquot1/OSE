<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_HDFK',
    'table'       => 'CENTRE_COUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
