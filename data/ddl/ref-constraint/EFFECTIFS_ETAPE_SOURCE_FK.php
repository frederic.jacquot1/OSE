<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_ETAPE_SOURCE_FK',
    'table'       => 'EFFECTIFS_ETAPE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
