<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_HMFK',
    'table'       => 'STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
