<?php

//@formatter:off

return [
    'name'        => 'NOEUD_HCFK',
    'table'       => 'NOEUD',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
