<?php

//@formatter:off

return [
    'name'        => 'VOIRIE_HDFK',
    'table'       => 'VOIRIE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
