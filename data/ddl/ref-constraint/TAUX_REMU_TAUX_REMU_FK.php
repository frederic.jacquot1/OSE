<?php

//@formatter:off

return [
    'name'        => 'TAUX_REMU_TAUX_REMU_FK',
    'table'       => 'TAUX_REMU',
    'rtable'      => 'TAUX_REMU',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TAUX_REMU_ID' => 'ID',
    ],
];

//@formatter:on
