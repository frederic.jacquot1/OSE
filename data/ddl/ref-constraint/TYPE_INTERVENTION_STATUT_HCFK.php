<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_STATUT_HCFK',
    'table'       => 'TYPE_INTERVENTION_STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
