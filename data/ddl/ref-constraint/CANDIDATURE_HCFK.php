<?php

//@formatter:off

return [
    'name'        => 'CANDIDATURE_HCFK',
    'table'       => 'CANDIDATURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
