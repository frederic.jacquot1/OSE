<?php

//@formatter:off

return [
    'name'        => 'DISCIPLINE_HMFK',
    'table'       => 'DISCIPLINE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
