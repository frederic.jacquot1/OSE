<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_HDFK',
    'table'       => 'EFFECTIFS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
