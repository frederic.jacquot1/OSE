<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_DEROGATION_HDFK',
    'table'       => 'PLAFOND_DEROGATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
