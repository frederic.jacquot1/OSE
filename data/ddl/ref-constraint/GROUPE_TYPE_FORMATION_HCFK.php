<?php

//@formatter:off

return [
    'name'        => 'GROUPE_TYPE_FORMATION_HCFK',
    'table'       => 'GROUPE_TYPE_FORMATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
