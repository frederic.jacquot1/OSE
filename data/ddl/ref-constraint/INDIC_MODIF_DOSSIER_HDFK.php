<?php

//@formatter:off

return [
    'name'        => 'INDIC_MODIF_DOSSIER_HDFK',
    'table'       => 'INDIC_MODIF_DOSSIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
