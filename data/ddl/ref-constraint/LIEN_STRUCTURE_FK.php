<?php

//@formatter:off

return [
    'name'        => 'LIEN_STRUCTURE_FK',
    'table'       => 'LIEN',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
