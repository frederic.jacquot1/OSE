<?php

//@formatter:off

return [
    'name'        => 'TMS_STRUCTURE_FK',
    'table'       => 'TYPE_MODULATEUR_STRUCTURE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
