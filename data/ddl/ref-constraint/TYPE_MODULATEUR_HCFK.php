<?php

//@formatter:off

return [
    'name'        => 'TYPE_MODULATEUR_HCFK',
    'table'       => 'TYPE_MODULATEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
