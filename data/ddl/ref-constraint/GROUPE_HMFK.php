<?php

//@formatter:off

return [
    'name'        => 'GROUPE_HMFK',
    'table'       => 'GROUPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
