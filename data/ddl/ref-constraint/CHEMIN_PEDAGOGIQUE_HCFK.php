<?php

//@formatter:off

return [
    'name'        => 'CHEMIN_PEDAGOGIQUE_HCFK',
    'table'       => 'CHEMIN_PEDAGOGIQUE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
