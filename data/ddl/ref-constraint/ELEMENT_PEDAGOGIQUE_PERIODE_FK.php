<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_PEDAGOGIQUE_PERIODE_FK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'PERIODE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PERIODE_ID' => 'ID',
    ],
];

//@formatter:on
