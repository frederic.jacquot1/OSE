<?php

//@formatter:off

return [
    'name'        => 'PIECE_JOINTE_FICHIER_FFK',
    'table'       => 'PIECE_JOINTE_FICHIER',
    'rtable'      => 'FICHIER',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'FICHIER_ID' => 'ID',
    ],
];

//@formatter:on
