<?php

//@formatter:off

return [
    'name'        => 'SNS_SCENARIO_NOEUD_FK',
    'table'       => 'SCENARIO_NOEUD_SEUIL',
    'rtable'      => 'SCENARIO_NOEUD',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'SCENARIO_NOEUD_ID' => 'ID',
    ],
];

//@formatter:on
