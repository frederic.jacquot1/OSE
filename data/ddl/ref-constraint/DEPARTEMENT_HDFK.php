<?php

//@formatter:off

return [
    'name'        => 'DEPARTEMENT_HDFK',
    'table'       => 'DEPARTEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
