<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_SOURCE_FK',
    'table'       => 'SCENARIO_NOEUD',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
