<?php

//@formatter:off

return [
    'name'        => 'ETAPE_DOMAINE_FONCTIONNEL_FK',
    'table'       => 'ETAPE',
    'rtable'      => 'DOMAINE_FONCTIONNEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'DOMAINE_FONCTIONNEL_ID' => 'ID',
    ],
];

//@formatter:on
