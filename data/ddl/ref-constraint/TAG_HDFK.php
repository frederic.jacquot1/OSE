<?php

//@formatter:off

return [
    'name'        => 'TAG_HDFK',
    'table'       => 'TAG',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
