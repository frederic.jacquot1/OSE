<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_CIVILITE_FK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'CIVILITE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'CIVILITE_ID' => 'ID',
    ],
];

//@formatter:on
