<?php

//@formatter:off

return [
    'name'        => 'PARAMETRE_HDFK',
    'table'       => 'PARAMETRE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
