<?php

//@formatter:off

return [
    'name'        => 'FONCTION_REFERENTIEL_HCFK',
    'table'       => 'FONCTION_REFERENTIEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
