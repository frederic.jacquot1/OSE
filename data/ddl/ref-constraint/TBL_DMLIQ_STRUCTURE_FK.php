<?php

//@formatter:off

return [
    'name'        => 'TBL_DMLIQ_STRUCTURE_FK',
    'table'       => 'TBL_DMEP_LIQUIDATION',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
