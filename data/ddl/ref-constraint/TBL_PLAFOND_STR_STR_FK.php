<?php

//@formatter:off

return [
    'name'        => 'TBL_PLAFOND_STR_STR_FK',
    'table'       => 'TBL_PLAFOND_STRUCTURE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
