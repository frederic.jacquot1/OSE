<?php

//@formatter:off

return [
    'name'        => 'VALIDATION_HDFK',
    'table'       => 'VALIDATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
