<?php

//@formatter:off

return [
    'name'        => 'GROUPE_HCFK',
    'table'       => 'GROUPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
