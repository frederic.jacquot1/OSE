<?php

//@formatter:off

return [
    'name'        => 'TIS_ANNEE_FIN_FK',
    'table'       => 'TYPE_INTERVENTION_STRUCTURE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_FIN_ID' => 'ID',
    ],
];

//@formatter:on
