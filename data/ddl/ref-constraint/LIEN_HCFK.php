<?php

//@formatter:off

return [
    'name'        => 'LIEN_HCFK',
    'table'       => 'LIEN',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
