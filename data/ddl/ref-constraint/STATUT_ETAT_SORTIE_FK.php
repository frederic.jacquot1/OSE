<?php

//@formatter:off

return [
    'name'        => 'STATUT_ETAT_SORTIE_FK',
    'table'       => 'STATUT',
    'rtable'      => 'ETAT_SORTIE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'CONTRAT_ETAT_SORTIE_ID' => 'ID',
    ],
];

//@formatter:on
