<?php

//@formatter:off

return [
    'name'        => 'AGREMENT_HCFK',
    'table'       => 'AGREMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
