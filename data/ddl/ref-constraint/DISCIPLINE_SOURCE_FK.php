<?php

//@formatter:off

return [
    'name'        => 'DISCIPLINE_SOURCE_FK',
    'table'       => 'DISCIPLINE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
