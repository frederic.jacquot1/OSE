<?php

//@formatter:off

return [
    'name'        => 'STATUT_HMFK',
    'table'       => 'STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
