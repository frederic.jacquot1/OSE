<?php

//@formatter:off

return [
    'name'        => 'ETABLISSEMENT_HMFK',
    'table'       => 'ETABLISSEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
