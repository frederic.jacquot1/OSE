<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_HMFK',
    'table'       => 'EFFECTIFS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
