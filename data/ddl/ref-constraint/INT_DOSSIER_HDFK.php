<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_HDFK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
