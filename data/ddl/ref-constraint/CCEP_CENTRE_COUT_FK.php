<?php

//@formatter:off

return [
    'name'        => 'CCEP_CENTRE_COUT_FK',
    'table'       => 'CENTRE_COUT_EP',
    'rtable'      => 'CENTRE_COUT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'CENTRE_COUT_ID' => 'ID',
    ],
];

//@formatter:on
