<?php

//@formatter:off

return [
    'name'        => 'NOEUD_ANNEE_FK',
    'table'       => 'NOEUD',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
