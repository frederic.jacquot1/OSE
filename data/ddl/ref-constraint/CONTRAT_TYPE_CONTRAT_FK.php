<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_TYPE_CONTRAT_FK',
    'table'       => 'CONTRAT',
    'rtable'      => 'TYPE_CONTRAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_CONTRAT_ID' => 'ID',
    ],
];

//@formatter:on
