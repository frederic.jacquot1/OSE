<?php

//@formatter:off

return [
    'name'        => 'MOTIF_NON_PAIEMENT_HDFK',
    'table'       => 'MOTIF_NON_PAIEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
