<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_CENTRE_COUT_FK',
    'table'       => 'CENTRE_COUT',
    'rtable'      => 'CENTRE_COUT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'PARENT_ID' => 'ID',
    ],
];

//@formatter:on
