<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_STATUT_HDFK',
    'table'       => 'TYPE_INTERVENTION_STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
