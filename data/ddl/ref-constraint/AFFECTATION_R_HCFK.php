<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_R_HCFK',
    'table'       => 'AFFECTATION_RECHERCHE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
