<?php

//@formatter:off

return [
    'name'        => 'TBL_LIEN_STRUCTURE_FK',
    'table'       => 'TBL_LIEN',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
