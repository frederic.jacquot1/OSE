<?php

//@formatter:off

return [
    'name'        => 'RSV_TYPE_INTERVENANT_FK',
    'table'       => 'REGLE_STRUCTURE_VALIDATION',
    'rtable'      => 'TYPE_INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
