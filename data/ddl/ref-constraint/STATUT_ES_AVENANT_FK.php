<?php

//@formatter:off

return [
    'name'        => 'STATUT_ES_AVENANT_FK',
    'table'       => 'STATUT',
    'rtable'      => 'ETAT_SORTIE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'AVENANT_ETAT_SORTIE_ID' => 'ID',
    ],
];

//@formatter:on
