<?php

//@formatter:off

return [
    'name'        => 'DOMAINE_FONCTIONNEL_HDFK',
    'table'       => 'DOMAINE_FONCTIONNEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
