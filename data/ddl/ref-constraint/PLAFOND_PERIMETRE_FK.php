<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_PERIMETRE_FK',
    'table'       => 'PLAFOND',
    'rtable'      => 'PLAFOND_PERIMETRE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_PERIMETRE_ID' => 'ID',
    ],
];

//@formatter:on
