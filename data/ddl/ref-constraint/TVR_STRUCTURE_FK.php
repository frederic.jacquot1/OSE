<?php

//@formatter:off

return [
    'name'        => 'TVR_STRUCTURE_FK',
    'table'       => 'TBL_VALIDATION_REFERENTIEL',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
