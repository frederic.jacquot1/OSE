<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_LIEN_HDFK',
    'table'       => 'SCENARIO_LIEN',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
