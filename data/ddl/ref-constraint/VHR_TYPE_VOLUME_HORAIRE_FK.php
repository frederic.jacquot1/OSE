<?php

//@formatter:off

return [
    'name'        => 'VHR_TYPE_VOLUME_HORAIRE_FK',
    'table'       => 'VOLUME_HORAIRE_REF',
    'rtable'      => 'TYPE_VOLUME_HORAIRE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_VOLUME_HORAIRE_ID' => 'ID',
    ],
];

//@formatter:on
