<?php

//@formatter:off

return [
    'name'        => 'OFFRE_EMPLOI_HDFK',
    'table'       => 'OFFRE_EMPLOI',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
