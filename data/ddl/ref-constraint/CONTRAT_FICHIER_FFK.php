<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_FICHIER_FFK',
    'table'       => 'CONTRAT_FICHIER',
    'rtable'      => 'FICHIER',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'FICHIER_ID' => 'ID',
    ],
];

//@formatter:on
