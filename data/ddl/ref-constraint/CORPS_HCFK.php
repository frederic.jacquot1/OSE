<?php

//@formatter:off

return [
    'name'        => 'CORPS_HCFK',
    'table'       => 'CORPS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
