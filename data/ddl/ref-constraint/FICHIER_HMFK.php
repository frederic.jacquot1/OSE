<?php

//@formatter:off

return [
    'name'        => 'FICHIER_HMFK',
    'table'       => 'FICHIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
