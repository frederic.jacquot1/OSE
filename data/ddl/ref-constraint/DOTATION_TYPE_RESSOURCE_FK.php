<?php

//@formatter:off

return [
    'name'        => 'DOTATION_TYPE_RESSOURCE_FK',
    'table'       => 'DOTATION',
    'rtable'      => 'TYPE_RESSOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_RESSOURCE_ID' => 'ID',
    ],
];

//@formatter:on
