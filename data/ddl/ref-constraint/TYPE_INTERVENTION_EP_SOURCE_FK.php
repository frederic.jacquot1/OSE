<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_EP_SOURCE_FK',
    'table'       => 'TYPE_INTERVENTION_EP',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
