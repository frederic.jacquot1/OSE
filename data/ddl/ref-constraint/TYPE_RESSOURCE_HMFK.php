<?php

//@formatter:off

return [
    'name'        => 'TYPE_RESSOURCE_HMFK',
    'table'       => 'TYPE_RESSOURCE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
