<?php

//@formatter:off

return [
    'name'        => 'WE_SUIV_WE_FK',
    'table'       => 'WF_ETAPE_DEP',
    'rtable'      => 'WF_ETAPE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ETAPE_SUIV_ID' => 'ID',
    ],
];

//@formatter:on
