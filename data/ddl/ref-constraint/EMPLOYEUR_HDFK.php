<?php

//@formatter:off

return [
    'name'        => 'EMPLOYEUR_HDFK',
    'table'       => 'EMPLOYEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
