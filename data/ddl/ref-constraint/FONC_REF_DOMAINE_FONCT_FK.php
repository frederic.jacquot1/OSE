<?php

//@formatter:off

return [
    'name'        => 'FONC_REF_DOMAINE_FONCT_FK',
    'table'       => 'FONCTION_REFERENTIEL',
    'rtable'      => 'DOMAINE_FONCTIONNEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'DOMAINE_FONCTIONNEL_ID' => 'ID',
    ],
];

//@formatter:on
