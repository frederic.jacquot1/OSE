<?php

//@formatter:off

return [
    'name'        => 'TVR_ANNEE_FK',
    'table'       => 'TBL_VALIDATION_REFERENTIEL',
    'rtable'      => 'ANNEE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
