<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_EP_HCFK',
    'table'       => 'CENTRE_COUT_EP',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
