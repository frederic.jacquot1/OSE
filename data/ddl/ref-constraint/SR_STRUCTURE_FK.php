<?php

//@formatter:off

return [
    'name'        => 'SR_STRUCTURE_FK',
    'table'       => 'SERVICE_REFERENTIEL',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
