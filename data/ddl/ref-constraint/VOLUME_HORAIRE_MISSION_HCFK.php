<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_MISSION_HCFK',
    'table'       => 'VOLUME_HORAIRE_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
