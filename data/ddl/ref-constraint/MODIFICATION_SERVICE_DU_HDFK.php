<?php

//@formatter:off

return [
    'name'        => 'MODIFICATION_SERVICE_DU_HDFK',
    'table'       => 'MODIFICATION_SERVICE_DU',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
