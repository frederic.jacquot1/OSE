<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_ADR_PAYS_FK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'PAYS',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_PAYS_ID' => 'ID',
    ],
];

//@formatter:on
