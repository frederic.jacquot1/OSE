<?php

//@formatter:off

return [
    'name'        => 'GRADE_SOURCE_FK',
    'table'       => 'GRADE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
