<?php

//@formatter:off

return [
    'name'        => 'VHM_CONTRAT_FK',
    'table'       => 'VOLUME_HORAIRE_MISSION',
    'rtable'      => 'CONTRAT',
    'delete_rule' => 'SET NULL',
    'index'       => NULL,
    'columns'     => [
        'CONTRAT_ID' => 'ID',
    ],
];

//@formatter:on
