<?php

//@formatter:off

return [
    'name'        => 'PARAMETRE_HMFK',
    'table'       => 'PARAMETRE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
