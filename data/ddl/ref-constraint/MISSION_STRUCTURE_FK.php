<?php

//@formatter:off

return [
    'name'        => 'MISSION_STRUCTURE_FK',
    'table'       => 'MISSION',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
