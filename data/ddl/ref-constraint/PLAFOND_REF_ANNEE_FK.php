<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_REF_ANNEE_FK',
    'table'       => 'PLAFOND_REFERENTIEL',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
