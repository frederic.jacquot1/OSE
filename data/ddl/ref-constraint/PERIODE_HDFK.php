<?php

//@formatter:off

return [
    'name'        => 'PERIODE_HDFK',
    'table'       => 'PERIODE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
