<?php

//@formatter:off

return [
    'name'        => 'MISE_EN_PAIEMENT_PERIODE_FK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'PERIODE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PERIODE_PAIEMENT_ID' => 'ID',
    ],
];

//@formatter:on
