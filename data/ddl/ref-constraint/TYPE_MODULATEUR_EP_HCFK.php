<?php

//@formatter:off

return [
    'name'        => 'TYPE_MODULATEUR_EP_HCFK',
    'table'       => 'TYPE_MODULATEUR_EP',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
