<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_INTERVENANT_FK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
