<?php

//@formatter:off

return [
    'name'        => 'TYPE_AGREMENT_HCFK',
    'table'       => 'TYPE_AGREMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
