<?php

//@formatter:off

return [
    'name'        => 'GRADE_HMFK',
    'table'       => 'GRADE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
