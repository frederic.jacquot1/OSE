<?php

//@formatter:off

return [
    'name'        => 'PAYS_HDFK',
    'table'       => 'PAYS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
