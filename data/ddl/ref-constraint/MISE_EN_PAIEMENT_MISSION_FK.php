<?php

//@formatter:off

return [
    'name'        => 'MISE_EN_PAIEMENT_MISSION_FK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MISSION_ID' => 'ID',
    ],
];

//@formatter:on
