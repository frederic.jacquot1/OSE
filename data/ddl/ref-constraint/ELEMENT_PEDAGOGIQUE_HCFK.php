<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_PEDAGOGIQUE_HCFK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
