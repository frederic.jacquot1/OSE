<?php

//@formatter:off

return [
    'name'        => 'SEUIL_CH_STRUCTURE_FK',
    'table'       => 'SEUIL_CHARGE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
