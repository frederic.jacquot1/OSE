<?php

//@formatter:off

return [
    'name'        => 'TYPE_CONTRAT_HCFK',
    'table'       => 'TYPE_CONTRAT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
