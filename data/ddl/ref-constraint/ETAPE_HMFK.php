<?php

//@formatter:off

return [
    'name'        => 'ETAPE_HMFK',
    'table'       => 'ETAPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
