<?php

//@formatter:off

return [
    'name'        => 'TBL_LIEN_SCENARIO_LIEN_FK',
    'table'       => 'TBL_LIEN',
    'rtable'      => 'SCENARIO_LIEN',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SCENARIO_LIEN_ID' => 'ID',
    ],
];

//@formatter:on
