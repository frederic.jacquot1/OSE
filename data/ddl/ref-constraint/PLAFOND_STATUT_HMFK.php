<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STATUT_HMFK',
    'table'       => 'PLAFOND_STATUT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
