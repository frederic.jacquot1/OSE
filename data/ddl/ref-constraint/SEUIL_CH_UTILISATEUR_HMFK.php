<?php

//@formatter:off

return [
    'name'        => 'SEUIL_CH_UTILISATEUR_HMFK',
    'table'       => 'SEUIL_CHARGE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
