<?php

//@formatter:off

return [
    'name'        => 'TBL_MISSION_MISSION_FK',
    'table'       => 'TBL_MISSION',
    'rtable'      => 'MISSION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'MISSION_ID' => 'ID',
    ],
];

//@formatter:on
