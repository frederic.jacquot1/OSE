<?php

//@formatter:off

return [
    'name'        => 'SERVICE_REFERENTIEL_SOURCE_FK',
    'table'       => 'SERVICE_REFERENTIEL',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
