<?php

//@formatter:off

return [
    'name'        => 'TYPE_FORMATION_HCFK',
    'table'       => 'TYPE_FORMATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
