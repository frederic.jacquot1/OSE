<?php

//@formatter:off

return [
    'name'        => 'VALIDATION_HMFK',
    'table'       => 'VALIDATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
