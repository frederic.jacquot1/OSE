<?php

//@formatter:off

return [
    'name'        => 'AGREMENT_HMFK',
    'table'       => 'AGREMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
