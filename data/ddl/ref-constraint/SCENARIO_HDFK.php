<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_HDFK',
    'table'       => 'SCENARIO',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
