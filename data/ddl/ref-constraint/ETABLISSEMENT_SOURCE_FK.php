<?php

//@formatter:off

return [
    'name'        => 'ETABLISSEMENT_SOURCE_FK',
    'table'       => 'ETABLISSEMENT',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
