<?php

//@formatter:off

return [
    'name'        => 'PARAMETRE_HCFK',
    'table'       => 'PARAMETRE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
