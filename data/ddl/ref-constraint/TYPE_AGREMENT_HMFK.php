<?php

//@formatter:off

return [
    'name'        => 'TYPE_AGREMENT_HMFK',
    'table'       => 'TYPE_AGREMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
