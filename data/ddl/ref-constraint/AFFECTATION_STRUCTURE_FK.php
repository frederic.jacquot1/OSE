<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_STRUCTURE_FK',
    'table'       => 'AFFECTATION',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
