<?php

//@formatter:off

return [
    'name'        => 'CHEMIN_PEDAGOGIQUE_HMFK',
    'table'       => 'CHEMIN_PEDAGOGIQUE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
