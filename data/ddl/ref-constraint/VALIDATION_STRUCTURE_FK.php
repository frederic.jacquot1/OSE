<?php

//@formatter:off

return [
    'name'        => 'VALIDATION_STRUCTURE_FK',
    'table'       => 'VALIDATION',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
