<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_EP_HMFK',
    'table'       => 'CENTRE_COUT_EP',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
