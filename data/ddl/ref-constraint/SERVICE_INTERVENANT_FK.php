<?php

//@formatter:off

return [
    'name'        => 'SERVICE_INTERVENANT_FK',
    'table'       => 'SERVICE',
    'rtable'      => 'INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
