<?php

//@formatter:off

return [
    'name'        => 'TMS_ANNEE_FIN_FK',
    'table'       => 'TYPE_MODULATEUR_STRUCTURE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_FIN_ID' => 'ID',
    ],
];

//@formatter:on
