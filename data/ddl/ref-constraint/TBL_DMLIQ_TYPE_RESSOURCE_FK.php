<?php

//@formatter:off

return [
    'name'        => 'TBL_DMLIQ_TYPE_RESSOURCE_FK',
    'table'       => 'TBL_DMEP_LIQUIDATION',
    'rtable'      => 'TYPE_RESSOURCE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_RESSOURCE_ID' => 'ID',
    ],
];

//@formatter:on
