<?php

//@formatter:off

return [
    'name'        => 'STATUT_ANNEE_FK',
    'table'       => 'STATUT',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
