<?php

//@formatter:off

return [
    'name'        => 'VH_PERIODE_FK',
    'table'       => 'VOLUME_HORAIRE',
    'rtable'      => 'PERIODE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PERIODE_ID' => 'ID',
    ],
];

//@formatter:on
