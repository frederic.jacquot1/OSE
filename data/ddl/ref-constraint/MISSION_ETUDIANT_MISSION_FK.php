<?php

//@formatter:off

return [
    'name'        => 'MISSION_ETUDIANT_MISSION_FK',
    'table'       => 'MISSION_ETUDIANT',
    'rtable'      => 'MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MISSION_ID' => 'ID',
    ],
];

//@formatter:on
