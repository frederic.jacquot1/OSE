<?php

//@formatter:off

return [
    'name'        => 'TI_STATUT_ANNEE_FK',
    'table'       => 'TYPE_INTERVENTION_STATUT',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
