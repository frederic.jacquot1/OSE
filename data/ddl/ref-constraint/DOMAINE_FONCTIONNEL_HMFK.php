<?php

//@formatter:off

return [
    'name'        => 'DOMAINE_FONCTIONNEL_HMFK',
    'table'       => 'DOMAINE_FONCTIONNEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
