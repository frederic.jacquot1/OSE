<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_NOEUD_FK',
    'table'       => 'SCENARIO_NOEUD',
    'rtable'      => 'NOEUD',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'NOEUD_ID' => 'ID',
    ],
];

//@formatter:on
