<?php

//@formatter:off

return [
    'name'        => 'EP_DISCIPLINE_FK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'DISCIPLINE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'DISCIPLINE_ID' => 'ID',
    ],
];

//@formatter:on
