<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_TYPE_MISSION_HCFK',
    'table'       => 'CENTRE_COUT_TYPE_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
