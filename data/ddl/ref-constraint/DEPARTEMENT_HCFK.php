<?php

//@formatter:off

return [
    'name'        => 'DEPARTEMENT_HCFK',
    'table'       => 'DEPARTEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
