<?php

//@formatter:off

return [
    'name'        => 'TYPE_HEURES_HMFK',
    'table'       => 'TYPE_HEURES',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
