<?php

//@formatter:off

return [
    'name'        => 'NOEUD_HMFK',
    'table'       => 'NOEUD',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
