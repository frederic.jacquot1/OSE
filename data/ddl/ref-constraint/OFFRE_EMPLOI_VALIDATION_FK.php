<?php

//@formatter:off

return [
    'name'        => 'OFFRE_EMPLOI_VALIDATION_FK',
    'table'       => 'OFFRE_EMPLOI',
    'rtable'      => 'VALIDATION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
