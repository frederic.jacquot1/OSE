<?php

//@formatter:off

return [
    'name'        => 'MEP_FR_SERVICE_FK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'FORMULE_RESULTAT_SERVICE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'FORMULE_RES_SERVICE_ID' => 'ID',
    ],
];

//@formatter:on
