<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_TAG_FK',
    'table'       => 'VOLUME_HORAIRE',
    'rtable'      => 'TAG',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TAG_ID' => 'ID',
    ],
];

//@formatter:on
