<?php

//@formatter:off

return [
    'name'        => 'MISE_EN_PAIEMENT_HMFK',
    'table'       => 'MISE_EN_PAIEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
