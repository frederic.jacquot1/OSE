<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_DEROGATION_HCFK',
    'table'       => 'PLAFOND_DEROGATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
