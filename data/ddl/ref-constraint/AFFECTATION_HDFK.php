<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_HDFK',
    'table'       => 'AFFECTATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
