<?php

//@formatter:off

return [
    'name'        => 'SERVICE_SOURCE_FK',
    'table'       => 'SERVICE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
