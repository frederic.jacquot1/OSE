<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_STATUT_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'STATUT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STATUT_ID' => 'ID',
    ],
];

//@formatter:on
