<?php

//@formatter:off

return [
    'name'        => 'CCS_STRUCTURE_FK',
    'table'       => 'CENTRE_COUT_STRUCTURE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
