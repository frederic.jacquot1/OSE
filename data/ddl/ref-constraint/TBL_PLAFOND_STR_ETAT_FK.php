<?php

//@formatter:off

return [
    'name'        => 'TBL_PLAFOND_STR_ETAT_FK',
    'table'       => 'TBL_PLAFOND_STRUCTURE',
    'rtable'      => 'PLAFOND_ETAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ETAT_ID' => 'ID',
    ],
];

//@formatter:on
