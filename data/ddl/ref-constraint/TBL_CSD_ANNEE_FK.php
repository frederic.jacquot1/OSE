<?php

//@formatter:off

return [
    'name'        => 'TBL_CSD_ANNEE_FK',
    'table'       => 'TBL_CHARGENS_SEUILS_DEF',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
