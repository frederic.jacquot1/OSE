<?php

//@formatter:off

return [
    'name'        => 'TBL_LIEN_NOEUD_SUP_FK',
    'table'       => 'TBL_LIEN',
    'rtable'      => 'NOEUD',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'NOEUD_SUP_ID' => 'ID',
    ],
];

//@formatter:on
