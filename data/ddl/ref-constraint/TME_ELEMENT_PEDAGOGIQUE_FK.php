<?php

//@formatter:off

return [
    'name'        => 'TME_ELEMENT_PEDAGOGIQUE_FK',
    'table'       => 'TYPE_MODULATEUR_EP',
    'rtable'      => 'ELEMENT_PEDAGOGIQUE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'ELEMENT_PEDAGOGIQUE_ID' => 'ID',
    ],
];

//@formatter:on
