<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_STATUT_ANNEE_FK',
    'table'       => 'PLAFOND_STATUT',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
