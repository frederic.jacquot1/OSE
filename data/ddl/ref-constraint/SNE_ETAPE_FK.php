<?php

//@formatter:off

return [
    'name'        => 'SNE_ETAPE_FK',
    'table'       => 'SCENARIO_NOEUD_EFFECTIF',
    'rtable'      => 'ETAPE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ETAPE_ID' => 'ID',
    ],
];

//@formatter:on
