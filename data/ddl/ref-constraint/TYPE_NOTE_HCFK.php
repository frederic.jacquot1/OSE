<?php

//@formatter:off

return [
    'name'        => 'TYPE_NOTE_HCFK',
    'table'       => 'TYPE_NOTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
