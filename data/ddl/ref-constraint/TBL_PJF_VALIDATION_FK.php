<?php

//@formatter:off

return [
    'name'        => 'TBL_PJF_VALIDATION_FK',
    'table'       => 'TBL_PIECE_JOINTE_FOURNIE',
    'rtable'      => 'VALIDATION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
