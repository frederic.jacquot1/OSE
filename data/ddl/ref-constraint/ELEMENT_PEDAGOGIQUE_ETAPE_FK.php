<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_PEDAGOGIQUE_ETAPE_FK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'ETAPE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ETAPE_ID' => 'ID',
    ],
];

//@formatter:on
