<?php

//@formatter:off

return [
    'name'        => 'CANDIDATURE_HDFK',
    'table'       => 'CANDIDATURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
