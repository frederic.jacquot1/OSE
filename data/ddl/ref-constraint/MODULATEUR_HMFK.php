<?php

//@formatter:off

return [
    'name'        => 'MODULATEUR_HMFK',
    'table'       => 'MODULATEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
