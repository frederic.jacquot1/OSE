<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_STRUCTURE_FK',
    'table'       => 'CONTRAT',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
