<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_R_STRUCTURE_FK',
    'table'       => 'AFFECTATION_RECHERCHE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
