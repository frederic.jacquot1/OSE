<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_MISSION_ANNEE_FK',
    'table'       => 'PLAFOND_MISSION',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
