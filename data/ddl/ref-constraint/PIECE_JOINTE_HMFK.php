<?php

//@formatter:off

return [
    'name'        => 'PIECE_JOINTE_HMFK',
    'table'       => 'PIECE_JOINTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
