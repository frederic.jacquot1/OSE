<?php

//@formatter:off

return [
    'name'        => 'FRVHR_FORMULE_RESULTAT_FK',
    'table'       => 'FORMULE_RESULTAT_VH_REF',
    'rtable'      => 'FORMULE_RESULTAT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'FORMULE_RESULTAT_ID' => 'ID',
    ],
];

//@formatter:on
