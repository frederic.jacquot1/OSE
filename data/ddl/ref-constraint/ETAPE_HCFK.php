<?php

//@formatter:off

return [
    'name'        => 'ETAPE_HCFK',
    'table'       => 'ETAPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
