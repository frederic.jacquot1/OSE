<?php

//@formatter:off

return [
    'name'        => 'NOTE_HMFK',
    'table'       => 'NOTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
