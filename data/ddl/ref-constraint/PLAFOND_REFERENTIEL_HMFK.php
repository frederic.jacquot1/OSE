<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_REFERENTIEL_HMFK',
    'table'       => 'PLAFOND_REFERENTIEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
