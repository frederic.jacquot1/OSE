<?php

//@formatter:off

return [
    'name'        => 'VH_CHARGE_ELEMENT_PEDAGO_FK',
    'table'       => 'VOLUME_HORAIRE_CHARGE',
    'rtable'      => 'ELEMENT_PEDAGOGIQUE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ELEMENT_PEDAGOGIQUE_ID' => 'ID',
    ],
];

//@formatter:on
