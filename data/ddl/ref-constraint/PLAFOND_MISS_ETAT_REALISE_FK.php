<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_MISS_ETAT_REALISE_FK',
    'table'       => 'PLAFOND_MISSION',
    'rtable'      => 'PLAFOND_ETAT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PLAFOND_ETAT_REALISE_ID' => 'ID',
    ],
];

//@formatter:on
