<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_TAUX_REGIMES_HDFK',
    'table'       => 'ELEMENT_TAUX_REGIMES',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
