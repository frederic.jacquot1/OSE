<?php

//@formatter:off

return [
    'name'        => 'PIECE_JOINTE_HDFK',
    'table'       => 'PIECE_JOINTE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
