<?php

//@formatter:off

return [
    'name'        => 'SERVICE_HDFK',
    'table'       => 'SERVICE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
