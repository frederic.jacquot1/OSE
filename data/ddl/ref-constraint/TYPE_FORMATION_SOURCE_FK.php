<?php

//@formatter:off

return [
    'name'        => 'TYPE_FORMATION_SOURCE_FK',
    'table'       => 'TYPE_FORMATION',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
