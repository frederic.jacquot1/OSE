<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_HMFK',
    'table'       => 'VOLUME_HORAIRE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
