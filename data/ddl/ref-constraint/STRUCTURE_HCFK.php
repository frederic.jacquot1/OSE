<?php

//@formatter:off

return [
    'name'        => 'STRUCTURE_HCFK',
    'table'       => 'STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
