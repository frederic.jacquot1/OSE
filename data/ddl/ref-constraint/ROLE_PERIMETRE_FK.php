<?php

//@formatter:off

return [
    'name'        => 'ROLE_PERIMETRE_FK',
    'table'       => 'ROLE',
    'rtable'      => 'PERIMETRE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'PERIMETRE_ID' => 'ID',
    ],
];

//@formatter:on
