<?php

//@formatter:off

return [
    'name'        => 'OFFRE_EMPLOI_STRUCTURE_FK',
    'table'       => 'OFFRE_EMPLOI',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
