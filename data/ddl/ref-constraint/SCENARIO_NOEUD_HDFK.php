<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_HDFK',
    'table'       => 'SCENARIO_NOEUD',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
