<?php

//@formatter:off

return [
    'name'        => 'CS_TYPE_INTERVENANT_FK',
    'table'       => 'CAMPAGNE_SAISIE',
    'rtable'      => 'TYPE_INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
