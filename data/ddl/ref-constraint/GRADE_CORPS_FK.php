<?php

//@formatter:off

return [
    'name'        => 'GRADE_CORPS_FK',
    'table'       => 'GRADE',
    'rtable'      => 'CORPS',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'CORPS_ID' => 'ID',
    ],
];

//@formatter:on
