<?php

//@formatter:off

return [
    'name'        => 'DOSSIER_CHAMP_AUTRE_TYPE_FK',
    'table'       => 'DOSSIER_CHAMP_AUTRE',
    'rtable'      => 'DOSSIER_CHAMP_AUTRE_TYPE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'DOSSIER_CHAMP_AUTRE_TYPE_ID' => 'ID',
    ],
];

//@formatter:on
