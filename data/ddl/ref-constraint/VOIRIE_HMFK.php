<?php

//@formatter:off

return [
    'name'        => 'VOIRIE_HMFK',
    'table'       => 'VOIRIE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
