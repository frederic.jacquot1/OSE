<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_ADR_PAYS_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'PAYS',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_PAYS_ID' => 'ID',
    ],
];

//@formatter:on
