<?php

//@formatter:off

return [
    'name'        => 'TPJS_ANNEE_FK',
    'table'       => 'TYPE_PIECE_JOINTE_STATUT',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
