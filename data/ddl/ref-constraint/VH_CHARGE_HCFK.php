<?php

//@formatter:off

return [
    'name'        => 'VH_CHARGE_HCFK',
    'table'       => 'VOLUME_HORAIRE_CHARGE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
