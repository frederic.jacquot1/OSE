<?php

//@formatter:off

return [
    'name'        => 'SEUIL_CH_TYPE_INTERVENTION_FK',
    'table'       => 'SEUIL_CHARGE',
    'rtable'      => 'TYPE_INTERVENTION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENTION_ID' => 'ID',
    ],
];

//@formatter:on
