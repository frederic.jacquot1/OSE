<?php

//@formatter:off

return [
    'name'        => 'EFFECTIFS_ETAPE_HDFK',
    'table'       => 'EFFECTIFS_ETAPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
