<?php

//@formatter:off

return [
    'name'        => 'SERVICE_ETABLISSEMENT_FK',
    'table'       => 'SERVICE',
    'rtable'      => 'ETABLISSEMENT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ETABLISSEMENT_ID' => 'ID',
    ],
];

//@formatter:on
