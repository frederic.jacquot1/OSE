<?php

//@formatter:off

return [
    'name'        => 'TBL_CSD_TYPE_INTERVENTION_FK',
    'table'       => 'TBL_CHARGENS_SEUILS_DEF',
    'rtable'      => 'TYPE_INTERVENTION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENTION_ID' => 'ID',
    ],
];

//@formatter:on
