<?php

//@formatter:off

return [
    'name'        => 'TBL_PLAFOND_REF_FR_FK',
    'table'       => 'TBL_PLAFOND_REFERENTIEL',
    'rtable'      => 'FONCTION_REFERENTIEL',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'FONCTION_REFERENTIEL_ID' => 'ID',
    ],
];

//@formatter:on
