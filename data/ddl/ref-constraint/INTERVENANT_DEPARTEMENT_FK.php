<?php

//@formatter:off

return [
    'name'        => 'INTERVENANT_DEPARTEMENT_FK',
    'table'       => 'INTERVENANT',
    'rtable'      => 'DEPARTEMENT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'DEPARTEMENT_NAISSANCE_ID' => 'ID',
    ],
];

//@formatter:on
