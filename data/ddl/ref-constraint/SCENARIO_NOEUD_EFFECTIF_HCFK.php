<?php

//@formatter:off

return [
    'name'        => 'SCENARIO_NOEUD_EFFECTIF_HCFK',
    'table'       => 'SCENARIO_NOEUD_EFFECTIF',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
