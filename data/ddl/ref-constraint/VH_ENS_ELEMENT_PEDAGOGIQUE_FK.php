<?php

//@formatter:off

return [
    'name'        => 'VH_ENS_ELEMENT_PEDAGOGIQUE_FK',
    'table'       => 'VOLUME_HORAIRE_ENS',
    'rtable'      => 'ELEMENT_PEDAGOGIQUE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ELEMENT_PEDAGOGIQUE_ID' => 'ID',
    ],
];

//@formatter:on
