<?php

//@formatter:off

return [
    'name'        => 'TYPE_MISSION_HDFK',
    'table'       => 'TYPE_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
