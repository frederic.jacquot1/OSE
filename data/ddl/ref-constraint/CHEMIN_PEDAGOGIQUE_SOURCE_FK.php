<?php

//@formatter:off

return [
    'name'        => 'CHEMIN_PEDAGOGIQUE_SOURCE_FK',
    'table'       => 'CHEMIN_PEDAGOGIQUE',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
