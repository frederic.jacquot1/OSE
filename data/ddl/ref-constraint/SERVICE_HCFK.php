<?php

//@formatter:off

return [
    'name'        => 'SERVICE_HCFK',
    'table'       => 'SERVICE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
