<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_PEDAGOGIQUE_ANNEE_FK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
