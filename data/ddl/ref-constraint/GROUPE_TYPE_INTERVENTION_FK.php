<?php

//@formatter:off

return [
    'name'        => 'GROUPE_TYPE_INTERVENTION_FK',
    'table'       => 'GROUPE',
    'rtable'      => 'TYPE_INTERVENTION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENTION_ID' => 'ID',
    ],
];

//@formatter:on
