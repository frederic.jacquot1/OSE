<?php

//@formatter:off

return [
    'name'        => 'TYPE_MODULATEUR_STRUCTURE_HDFK',
    'table'       => 'TYPE_MODULATEUR_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
