<?php

//@formatter:off

return [
    'name'        => 'TVR_VALIDATION_FK',
    'table'       => 'TBL_VALIDATION_REFERENTIEL',
    'rtable'      => 'VALIDATION',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'VALIDATION_ID' => 'ID',
    ],
];

//@formatter:on
