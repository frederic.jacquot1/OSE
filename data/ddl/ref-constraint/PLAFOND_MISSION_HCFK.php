<?php

//@formatter:off

return [
    'name'        => 'PLAFOND_MISSION_HCFK',
    'table'       => 'PLAFOND_MISSION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
