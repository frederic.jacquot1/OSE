<?php

//@formatter:off

return [
    'name'        => 'TIS_STRUCTURE_FK',
    'table'       => 'TYPE_INTERVENTION_STRUCTURE',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
