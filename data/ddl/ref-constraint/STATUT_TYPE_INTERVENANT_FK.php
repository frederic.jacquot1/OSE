<?php

//@formatter:off

return [
    'name'        => 'STATUT_TYPE_INTERVENANT_FK',
    'table'       => 'STATUT',
    'rtable'      => 'TYPE_INTERVENANT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_INTERVENANT_ID' => 'ID',
    ],
];

//@formatter:on
