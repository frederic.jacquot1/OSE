<?php

//@formatter:off

return [
    'name'        => 'PAYS_HMFK',
    'table'       => 'PAYS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
