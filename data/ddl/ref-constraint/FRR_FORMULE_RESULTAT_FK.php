<?php

//@formatter:off

return [
    'name'        => 'FRR_FORMULE_RESULTAT_FK',
    'table'       => 'FORMULE_RESULTAT_SERVICE_REF',
    'rtable'      => 'FORMULE_RESULTAT',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'FORMULE_RESULTAT_ID' => 'ID',
    ],
];

//@formatter:on
