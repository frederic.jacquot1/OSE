<?php

//@formatter:off

return [
    'name'        => 'TYPE_INTERVENTION_STRUCTU_HMFK',
    'table'       => 'TYPE_INTERVENTION_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
