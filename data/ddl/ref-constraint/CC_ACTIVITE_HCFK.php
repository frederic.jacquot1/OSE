<?php

//@formatter:off

return [
    'name'        => 'CC_ACTIVITE_HCFK',
    'table'       => 'CC_ACTIVITE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
