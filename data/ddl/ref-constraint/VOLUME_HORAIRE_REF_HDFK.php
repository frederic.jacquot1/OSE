<?php

//@formatter:off

return [
    'name'        => 'VOLUME_HORAIRE_REF_HDFK',
    'table'       => 'VOLUME_HORAIRE_REF',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
