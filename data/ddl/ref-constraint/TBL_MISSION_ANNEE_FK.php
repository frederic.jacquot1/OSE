<?php

//@formatter:off

return [
    'name'        => 'TBL_MISSION_ANNEE_FK',
    'table'       => 'TBL_MISSION',
    'rtable'      => 'ANNEE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ANNEE_ID' => 'ID',
    ],
];

//@formatter:on
