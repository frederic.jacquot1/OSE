<?php

//@formatter:off

return [
    'name'        => 'CCTM_CENTRE_COUT_FK',
    'table'       => 'CENTRE_COUT_TYPE_MISSION',
    'rtable'      => 'CENTRE_COUT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'CENTRE_COUT_ID' => 'ID',
    ],
];

//@formatter:on
