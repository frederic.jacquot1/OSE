<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_MODULATEUR_HCFK',
    'table'       => 'ELEMENT_MODULATEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
