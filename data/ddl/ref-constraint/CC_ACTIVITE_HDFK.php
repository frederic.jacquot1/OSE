<?php

//@formatter:off

return [
    'name'        => 'CC_ACTIVITE_HDFK',
    'table'       => 'CC_ACTIVITE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
