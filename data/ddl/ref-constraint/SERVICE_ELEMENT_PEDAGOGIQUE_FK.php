<?php

//@formatter:off

return [
    'name'        => 'SERVICE_ELEMENT_PEDAGOGIQUE_FK',
    'table'       => 'SERVICE',
    'rtable'      => 'ELEMENT_PEDAGOGIQUE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ELEMENT_PEDAGOGIQUE_ID' => 'ID',
    ],
];

//@formatter:on
