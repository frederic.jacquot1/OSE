<?php

//@formatter:off

return [
    'name'        => 'EMPLOYEUR_HMFK',
    'table'       => 'EMPLOYEUR',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
