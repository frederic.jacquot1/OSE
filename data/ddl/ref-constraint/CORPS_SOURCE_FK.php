<?php

//@formatter:off

return [
    'name'        => 'CORPS_SOURCE_FK',
    'table'       => 'CORPS',
    'rtable'      => 'SOURCE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'SOURCE_ID' => 'ID',
    ],
];

//@formatter:on
