<?php

//@formatter:off

return [
    'name'        => 'NOEUD_STRUCTURE_FK',
    'table'       => 'NOEUD',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
