<?php

//@formatter:off

return [
    'name'        => 'CONTRAT_MISSION_FK',
    'table'       => 'CONTRAT',
    'rtable'      => 'MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MISSION_ID' => 'ID',
    ],
];

//@formatter:on
