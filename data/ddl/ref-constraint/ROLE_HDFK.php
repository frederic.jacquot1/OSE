<?php

//@formatter:off

return [
    'name'        => 'ROLE_HDFK',
    'table'       => 'ROLE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
