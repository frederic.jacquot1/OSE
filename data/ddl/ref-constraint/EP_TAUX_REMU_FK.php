<?php

//@formatter:off

return [
    'name'        => 'EP_TAUX_REMU_FK',
    'table'       => 'ELEMENT_PEDAGOGIQUE',
    'rtable'      => 'TAUX_REMU',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TAUX_REMU_ID' => 'ID',
    ],
];

//@formatter:on
