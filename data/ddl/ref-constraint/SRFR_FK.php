<?php

//@formatter:off

return [
    'name'        => 'SRFR_FK',
    'table'       => 'SERVICE_REFERENTIEL',
    'rtable'      => 'FONCTION_REFERENTIEL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'FONCTION_ID' => 'ID',
    ],
];

//@formatter:on
