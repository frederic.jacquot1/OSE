<?php

//@formatter:off

return [
    'name'        => 'FONCTION_REFERENTIEL_HMFK',
    'table'       => 'FONCTION_REFERENTIEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
