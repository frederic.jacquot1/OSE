<?php

//@formatter:off

return [
    'name'        => 'SERVICE_REFERENTIEL_HDFK',
    'table'       => 'SERVICE_REFERENTIEL',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
