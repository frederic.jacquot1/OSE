<?php

//@formatter:off

return [
    'name'        => 'FICHIER_HDFK',
    'table'       => 'FICHIER',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
