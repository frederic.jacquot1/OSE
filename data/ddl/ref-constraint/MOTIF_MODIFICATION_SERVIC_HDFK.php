<?php

//@formatter:off

return [
    'name'        => 'MOTIF_MODIFICATION_SERVIC_HDFK',
    'table'       => 'MOTIF_MODIFICATION_SERVICE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
