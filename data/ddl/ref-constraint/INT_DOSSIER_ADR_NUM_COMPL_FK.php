<?php

//@formatter:off

return [
    'name'        => 'INT_DOSSIER_ADR_NUM_COMPL_FK',
    'table'       => 'INTERVENANT_DOSSIER',
    'rtable'      => 'ADRESSE_NUMERO_COMPL',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'ADRESSE_NUMERO_COMPL_ID' => 'ID',
    ],
];

//@formatter:on
