<?php

//@formatter:off

return [
    'name'        => 'TYPE_CONTRAT_HDFK',
    'table'       => 'TYPE_CONTRAT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
