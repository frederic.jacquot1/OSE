<?php

//@formatter:off

return [
    'name'        => 'TBL_CSD_STRUCTURE_FK',
    'table'       => 'TBL_CHARGENS_SEUILS_DEF',
    'rtable'      => 'STRUCTURE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'STRUCTURE_ID' => 'ID',
    ],
];

//@formatter:on
