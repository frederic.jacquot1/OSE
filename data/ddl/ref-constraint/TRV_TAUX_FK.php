<?php

//@formatter:off

return [
    'name'        => 'TRV_TAUX_FK',
    'table'       => 'TAUX_REMU_VALEUR',
    'rtable'      => 'TAUX_REMU',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TAUX_REMU_ID' => 'ID',
    ],
];

//@formatter:on
