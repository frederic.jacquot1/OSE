<?php

//@formatter:off

return [
    'name'        => 'GROUPE_HDFK',
    'table'       => 'GROUPE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
