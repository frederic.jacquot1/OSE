<?php

//@formatter:off

return [
    'name'        => 'AFFECTATION_R_HMFK',
    'table'       => 'AFFECTATION_RECHERCHE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
