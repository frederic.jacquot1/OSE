<?php

//@formatter:off

return [
    'name'        => 'CENTRE_COUT_STRUCTURE_HMFK',
    'table'       => 'CENTRE_COUT_STRUCTURE',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
