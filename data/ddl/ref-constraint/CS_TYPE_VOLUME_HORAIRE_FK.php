<?php

//@formatter:off

return [
    'name'        => 'CS_TYPE_VOLUME_HORAIRE_FK',
    'table'       => 'CAMPAGNE_SAISIE',
    'rtable'      => 'TYPE_VOLUME_HORAIRE',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_VOLUME_HORAIRE_ID' => 'ID',
    ],
];

//@formatter:on
