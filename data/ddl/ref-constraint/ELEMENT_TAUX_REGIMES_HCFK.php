<?php

//@formatter:off

return [
    'name'        => 'ELEMENT_TAUX_REGIMES_HCFK',
    'table'       => 'ELEMENT_TAUX_REGIMES',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_CREATEUR_ID' => 'ID',
    ],
];

//@formatter:on
