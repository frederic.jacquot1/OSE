<?php

//@formatter:off

return [
    'name'        => 'OFFRE_EMPLOI_TYPE_MISSION_FK',
    'table'       => 'OFFRE_EMPLOI',
    'rtable'      => 'TYPE_MISSION',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'TYPE_MISSION_ID' => 'ID',
    ],
];

//@formatter:on
