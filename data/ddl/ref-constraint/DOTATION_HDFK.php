<?php

//@formatter:off

return [
    'name'        => 'DOTATION_HDFK',
    'table'       => 'DOTATION',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_DESTRUCTEUR_ID' => 'ID',
    ],
];

//@formatter:on
