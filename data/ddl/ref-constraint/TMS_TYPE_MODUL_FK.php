<?php

//@formatter:off

return [
    'name'        => 'TMS_TYPE_MODUL_FK',
    'table'       => 'TYPE_MODULATEUR_STRUCTURE',
    'rtable'      => 'TYPE_MODULATEUR',
    'delete_rule' => 'CASCADE',
    'index'       => NULL,
    'columns'     => [
        'TYPE_MODULATEUR_ID' => 'ID',
    ],
];

//@formatter:on
