<?php

//@formatter:off

return [
    'name'        => 'SERV_REF_MOTIF_NON_PAIEMENT_FK',
    'table'       => 'SERVICE_REFERENTIEL',
    'rtable'      => 'MOTIF_NON_PAIEMENT',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'MOTIF_NON_PAIEMENT_ID' => 'ID',
    ],
];

//@formatter:on
