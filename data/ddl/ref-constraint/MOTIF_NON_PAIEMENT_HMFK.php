<?php

//@formatter:off

return [
    'name'        => 'MOTIF_NON_PAIEMENT_HMFK',
    'table'       => 'MOTIF_NON_PAIEMENT',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
