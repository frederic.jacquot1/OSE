<?php

//@formatter:off

return [
    'name'        => 'CORPS_HMFK',
    'table'       => 'CORPS',
    'rtable'      => 'UTILISATEUR',
    'delete_rule' => NULL,
    'index'       => NULL,
    'columns'     => [
        'HISTO_MODIFICATEUR_ID' => 'ID',
    ],
];

//@formatter:on
