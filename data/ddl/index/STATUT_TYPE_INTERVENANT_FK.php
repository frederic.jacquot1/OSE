<?php

//@formatter:off

return [
    'name'    => 'STATUT_TYPE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'STATUT',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
