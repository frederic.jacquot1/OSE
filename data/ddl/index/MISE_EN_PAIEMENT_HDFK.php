<?php

//@formatter:off

return [
    'name'    => 'MISE_EN_PAIEMENT_HDFK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
