<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_ADR_NUM_COMPL_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'ADRESSE_NUMERO_COMPL_ID',
    ],
];

//@formatter:on
