<?php

//@formatter:off

return [
    'name'    => 'VOIRIE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'VOIRIE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
