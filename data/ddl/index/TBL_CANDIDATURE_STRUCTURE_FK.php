<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CANDIDATURE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
