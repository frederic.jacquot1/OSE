<?php

//@formatter:off

return [
    'name'    => 'TYPE_HEURES_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_HEURES',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
