<?php

//@formatter:off

return [
    'name'    => 'PARAMETRE_HMFK',
    'unique'  => FALSE,
    'table'   => 'PARAMETRE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
