<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_PAYS_NAISS_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'PAYS_NAISSANCE_ID',
    ],
];

//@formatter:on
