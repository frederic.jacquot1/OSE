<?php

//@formatter:off

return [
    'name'    => 'OFFRE_EMPLOI_TYPE_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'OFFRE_EMPLOI',
    'columns' => [
        'TYPE_MISSION_ID',
    ],
];

//@formatter:on
