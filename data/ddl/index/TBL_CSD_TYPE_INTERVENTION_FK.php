<?php

//@formatter:off

return [
    'name'    => 'TBL_CSD_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS_SEUILS_DEF',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
