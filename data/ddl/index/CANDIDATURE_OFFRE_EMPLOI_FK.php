<?php

//@formatter:off

return [
    'name'    => 'CANDIDATURE_OFFRE_EMPLOI_FK',
    'unique'  => FALSE,
    'table'   => 'CANDIDATURE',
    'columns' => [
        'OFFRE_EMPLOI_ID',
    ],
];

//@formatter:on
