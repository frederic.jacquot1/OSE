<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCTURE_PLAFOND_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STRUCTURE',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
