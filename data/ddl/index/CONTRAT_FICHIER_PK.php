<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_FICHIER_PK',
    'unique'  => TRUE,
    'table'   => 'CONTRAT_FICHIER',
    'columns' => [
        'CONTRAT_ID',
        'FICHIER_ID',
    ],
];

//@formatter:on
