<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_HMFK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
