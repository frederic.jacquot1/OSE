<?php

//@formatter:off

return [
    'name'    => 'VVHR_VOLUME_HORAIRE_REF_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION_VOL_HORAIRE_REF',
    'columns' => [
        'VOLUME_HORAIRE_REF_ID',
    ],
];

//@formatter:on
