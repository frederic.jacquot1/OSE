<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_PAYS_NAT_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'PAYS_NATIONALITE_ID',
    ],
];

//@formatter:on
