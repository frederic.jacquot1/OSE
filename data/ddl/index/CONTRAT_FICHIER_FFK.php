<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_FICHIER_FFK',
    'unique'  => FALSE,
    'table'   => 'CONTRAT_FICHIER',
    'columns' => [
        'FICHIER_ID',
    ],
];

//@formatter:on
