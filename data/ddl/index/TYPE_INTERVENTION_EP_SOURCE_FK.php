<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_EP_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_EP',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
