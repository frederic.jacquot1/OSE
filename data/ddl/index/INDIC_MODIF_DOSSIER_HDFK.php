<?php

//@formatter:off

return [
    'name'    => 'INDIC_MODIF_DOSSIER_HDFK',
    'unique'  => FALSE,
    'table'   => 'INDIC_MODIF_DOSSIER',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
