<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_MISSION_FR_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_MISSION',
    'columns' => [
        'TYPE_MISSION_ID',
    ],
];

//@formatter:on
