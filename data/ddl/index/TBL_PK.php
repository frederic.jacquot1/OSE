<?php

//@formatter:off

return [
    'name'    => 'TBL_PK',
    'unique'  => TRUE,
    'table'   => 'TBL',
    'columns' => [
        'TBL_NAME',
    ],
];

//@formatter:on
