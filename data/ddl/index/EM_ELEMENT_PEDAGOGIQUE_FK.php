<?php

//@formatter:off

return [
    'name'    => 'EM_ELEMENT_PEDAGOGIQUE_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_MODULATEUR',
    'columns' => [
        'ELEMENT_ID',
    ],
];

//@formatter:on
