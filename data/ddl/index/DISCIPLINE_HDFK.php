<?php

//@formatter:off

return [
    'name'    => 'DISCIPLINE_HDFK',
    'unique'  => FALSE,
    'table'   => 'DISCIPLINE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
