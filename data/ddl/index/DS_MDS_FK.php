<?php

//@formatter:off

return [
    'name'    => 'DS_MDS_FK',
    'unique'  => FALSE,
    'table'   => 'MODIFICATION_SERVICE_DU',
    'columns' => [
        'MOTIF_ID',
    ],
];

//@formatter:on
