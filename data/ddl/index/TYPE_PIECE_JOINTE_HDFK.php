<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_PIECE_JOINTE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
