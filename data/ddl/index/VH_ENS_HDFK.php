<?php

//@formatter:off

return [
    'name'    => 'VH_ENS_HDFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_ENS',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
