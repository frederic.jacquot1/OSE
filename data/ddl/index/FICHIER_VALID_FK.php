<?php

//@formatter:off

return [
    'name'    => 'FICHIER_VALID_FK',
    'unique'  => FALSE,
    'table'   => 'FICHIER',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
