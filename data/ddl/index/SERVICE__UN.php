<?php

//@formatter:off

return [
    'name'    => 'SERVICE__UN',
    'unique'  => TRUE,
    'table'   => 'SERVICE',
    'columns' => [
        'INTERVENANT_ID',
        'ELEMENT_PEDAGOGIQUE_ID',
        'ETABLISSEMENT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
