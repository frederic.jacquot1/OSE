<?php

//@formatter:off

return [
    'name'    => 'ROLE_PRIVILEGE_PRIVILEGE_FK',
    'unique'  => FALSE,
    'table'   => 'ROLE_PRIVILEGE',
    'columns' => [
        'PRIVILEGE_ID',
    ],
];

//@formatter:on
