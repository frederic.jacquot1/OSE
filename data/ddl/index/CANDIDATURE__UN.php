<?php

//@formatter:off

return [
    'name'    => 'CANDIDATURE__UN',
    'unique'  => TRUE,
    'table'   => 'CANDIDATURE',
    'columns' => [
        'INTERVENANT_ID',
        'OFFRE_EMPLOI_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
