<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_LIEN_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'SCENARIO_LIEN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
