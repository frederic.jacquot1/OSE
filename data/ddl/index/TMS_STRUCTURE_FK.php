<?php

//@formatter:off

return [
    'name'    => 'TMS_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
