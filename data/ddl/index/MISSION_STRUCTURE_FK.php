<?php

//@formatter:off

return [
    'name'    => 'MISSION_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
