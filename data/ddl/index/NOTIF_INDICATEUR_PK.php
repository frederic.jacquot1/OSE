<?php

//@formatter:off

return [
    'name'    => 'NOTIF_INDICATEUR_PK',
    'unique'  => TRUE,
    'table'   => 'NOTIFICATION_INDICATEUR',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
