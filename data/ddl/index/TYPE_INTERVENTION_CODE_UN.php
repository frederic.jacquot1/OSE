<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
