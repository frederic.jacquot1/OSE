<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_STATUT_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
