<?php

//@formatter:off

return [
    'name'    => 'TBL_PLA_INT_TVH_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_INTERVENANT',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
