<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
