<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_ADR_VOIRIE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'ADRESSE_VOIRIE_ID',
    ],
];

//@formatter:on
