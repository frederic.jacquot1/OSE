<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_PK',
    'unique'  => TRUE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
