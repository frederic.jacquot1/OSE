<?php

//@formatter:off

return [
    'name'    => 'STATUT_HMFK',
    'unique'  => FALSE,
    'table'   => 'STATUT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
