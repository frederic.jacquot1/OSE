<?php

//@formatter:off

return [
    'name'    => 'ETABLISSEMENT_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'ETABLISSEMENT',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
