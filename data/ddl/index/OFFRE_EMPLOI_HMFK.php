<?php

//@formatter:off

return [
    'name'    => 'OFFRE_EMPLOI_HMFK',
    'unique'  => FALSE,
    'table'   => 'OFFRE_EMPLOI',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
