<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'CONTRAT',
    'columns' => [
        'MISSION_ID',
    ],
];

//@formatter:on
