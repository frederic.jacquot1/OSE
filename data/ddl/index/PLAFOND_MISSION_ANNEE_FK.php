<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_MISSION_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_MISSION',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
