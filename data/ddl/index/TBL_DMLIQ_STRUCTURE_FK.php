<?php

//@formatter:off

return [
    'name'    => 'TBL_DMLIQ_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_DMEP_LIQUIDATION',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
