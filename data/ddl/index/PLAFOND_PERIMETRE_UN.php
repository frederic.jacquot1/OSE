<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_PERIMETRE_UN',
    'unique'  => TRUE,
    'table'   => 'PLAFOND_PERIMETRE',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
