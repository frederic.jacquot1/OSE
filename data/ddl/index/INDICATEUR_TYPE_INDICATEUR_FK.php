<?php

//@formatter:off

return [
    'name'    => 'INDICATEUR_TYPE_INDICATEUR_FK',
    'unique'  => FALSE,
    'table'   => 'INDICATEUR',
    'columns' => [
        'TYPE_INDICATEUR_ID',
    ],
];

//@formatter:on
