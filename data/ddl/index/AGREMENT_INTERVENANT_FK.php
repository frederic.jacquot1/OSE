<?php

//@formatter:off

return [
    'name'    => 'AGREMENT_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'AGREMENT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
