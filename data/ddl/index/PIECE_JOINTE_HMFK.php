<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_HMFK',
    'unique'  => FALSE,
    'table'   => 'PIECE_JOINTE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
