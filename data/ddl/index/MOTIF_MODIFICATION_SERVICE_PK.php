<?php

//@formatter:off

return [
    'name'    => 'MOTIF_MODIFICATION_SERVICE_PK',
    'unique'  => TRUE,
    'table'   => 'MOTIF_MODIFICATION_SERVICE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
