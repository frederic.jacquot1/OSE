<?php

//@formatter:off

return [
    'name'    => 'CC_ACTIVITE_REF',
    'unique'  => FALSE,
    'table'   => 'CC_ACTIVITE',
    'columns' => [
        'REFERENTIEL',
    ],
];

//@formatter:on
