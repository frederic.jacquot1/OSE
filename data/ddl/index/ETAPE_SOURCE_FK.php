<?php

//@formatter:off

return [
    'name'    => 'ETAPE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'ETAPE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
