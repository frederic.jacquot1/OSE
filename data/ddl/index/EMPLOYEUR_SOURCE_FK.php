<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
