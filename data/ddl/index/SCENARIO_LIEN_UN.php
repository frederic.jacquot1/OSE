<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_LIEN_UN',
    'unique'  => TRUE,
    'table'   => 'SCENARIO_LIEN',
    'columns' => [
        'SCENARIO_ID',
        'LIEN_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
