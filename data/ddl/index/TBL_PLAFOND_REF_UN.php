<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_REF_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PLAFOND_REFERENTIEL',
    'columns' => [
        'FONCTION_REFERENTIEL_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'INTERVENANT_ID',
        'ANNEE_ID',
        'PLAFOND_ID',
    ],
];

//@formatter:on
