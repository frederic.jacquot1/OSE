<?php

//@formatter:off

return [
    'name'    => 'TIS_ANNEE_DEBUT_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'columns' => [
        'ANNEE_DEBUT_ID',
    ],
];

//@formatter:on
