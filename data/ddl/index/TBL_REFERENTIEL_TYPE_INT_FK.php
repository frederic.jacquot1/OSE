<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_TYPE_INT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
