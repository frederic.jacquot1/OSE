<?php

//@formatter:off

return [
    'name'    => 'VHM_CONTRAT_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_MISSION',
    'columns' => [
        'CONTRAT_ID',
    ],
];

//@formatter:on
