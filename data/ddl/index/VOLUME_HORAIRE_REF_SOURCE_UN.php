<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_REF_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'VOLUME_HORAIRE_REF',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
