<?php

//@formatter:off

return [
    'name'    => 'CANDIDATURE_PK',
    'unique'  => TRUE,
    'table'   => 'CANDIDATURE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
