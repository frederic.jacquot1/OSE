<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_CIVILITE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'CIVILITE_ID',
    ],
];

//@formatter:on
