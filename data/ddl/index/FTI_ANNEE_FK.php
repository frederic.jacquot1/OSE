<?php

//@formatter:off

return [
    'name'    => 'FTI_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_TEST_INTERVENANT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
