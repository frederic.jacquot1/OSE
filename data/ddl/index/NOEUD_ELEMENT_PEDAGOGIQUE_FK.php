<?php

//@formatter:off

return [
    'name'    => 'NOEUD_ELEMENT_PEDAGOGIQUE_FK',
    'unique'  => FALSE,
    'table'   => 'NOEUD',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
