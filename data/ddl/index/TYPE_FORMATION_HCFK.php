<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_FORMATION',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
