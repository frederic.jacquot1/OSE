<?php

//@formatter:off

return [
    'name'    => 'TYPE_HEURES_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_HEURES',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
