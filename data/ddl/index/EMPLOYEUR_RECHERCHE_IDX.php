<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_RECHERCHE_IDX',
    'unique'  => FALSE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'CRITERE_RECHERCHE',
    ],
];

//@formatter:on
