<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_VH_ETAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_VOLUME_HORAIRE',
    'columns' => [
        'PLAFOND_ETAT_ID',
    ],
];

//@formatter:on
