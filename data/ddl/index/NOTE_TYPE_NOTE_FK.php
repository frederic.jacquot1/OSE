<?php

//@formatter:off

return [
    'name'    => 'NOTE_TYPE_NOTE_FK',
    'unique'  => FALSE,
    'table'   => 'NOTE',
    'columns' => [
        'TYPE_NOTE_ID',
    ],
];

//@formatter:on
