<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'ANNEE_DEBUT_ID',
    ],
];

//@formatter:on
