<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_MISS_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION_MISSION',
    'columns' => [
        'MISSION_ID',
    ],
];

//@formatter:on
