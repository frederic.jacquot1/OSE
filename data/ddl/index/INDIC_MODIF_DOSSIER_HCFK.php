<?php

//@formatter:off

return [
    'name'    => 'INDIC_MODIF_DOSSIER_HCFK',
    'unique'  => FALSE,
    'table'   => 'INDIC_MODIF_DOSSIER',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
