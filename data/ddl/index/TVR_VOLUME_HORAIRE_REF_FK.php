<?php

//@formatter:off

return [
    'name'    => 'TVR_VOLUME_HORAIRE_REF_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'VOLUME_HORAIRE_REF_ID',
    ],
];

//@formatter:on
