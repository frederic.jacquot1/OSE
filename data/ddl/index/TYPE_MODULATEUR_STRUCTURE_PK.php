<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_STRUCTURE_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
