<?php

//@formatter:off

return [
    'name'    => 'VH_ENS_HCFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_ENS',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
