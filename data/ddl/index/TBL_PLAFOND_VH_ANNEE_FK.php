<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_VH_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_VOLUME_HORAIRE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
