<?php

//@formatter:off

return [
    'name'    => 'TBCH_SCENARIO_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'SCENARIO_ID',
    ],
];

//@formatter:on
