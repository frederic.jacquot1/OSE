<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION__UN',
    'unique'  => TRUE,
    'table'   => 'AFFECTATION',
    'columns' => [
        'ROLE_ID',
        'STRUCTURE_ID',
        'HISTO_DESTRUCTION',
        'UTILISATEUR_ID',
    ],
];

//@formatter:on
