<?php

//@formatter:off

return [
    'name'    => 'CANDIDATURE_HMFK',
    'unique'  => FALSE,
    'table'   => 'CANDIDATURE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
