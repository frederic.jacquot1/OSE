<?php

//@formatter:off

return [
    'name'    => 'DOSSIER_CHAMP_AUTRE_PK',
    'unique'  => TRUE,
    'table'   => 'DOSSIER_CHAMP_AUTRE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
