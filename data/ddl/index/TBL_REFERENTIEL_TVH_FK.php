<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_TVH_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
