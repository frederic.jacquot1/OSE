<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD_EFFECTIF_HDFK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD_EFFECTIF',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
