<?php

//@formatter:off

return [
    'name'    => 'AGREMENT__UN',
    'unique'  => TRUE,
    'table'   => 'AGREMENT',
    'columns' => [
        'TYPE_AGREMENT_ID',
        'INTERVENANT_ID',
        'STRUCTURE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
