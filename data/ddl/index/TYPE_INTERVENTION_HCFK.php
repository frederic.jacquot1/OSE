<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
