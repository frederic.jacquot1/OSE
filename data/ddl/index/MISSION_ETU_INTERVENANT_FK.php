<?php

//@formatter:off

return [
    'name'    => 'MISSION_ETU_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION_ETUDIANT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
