<?php

//@formatter:off

return [
    'name'    => 'MISE_EN_PAIEMENT_PERIODE_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'PERIODE_PAIEMENT_ID',
    ],
];

//@formatter:on
