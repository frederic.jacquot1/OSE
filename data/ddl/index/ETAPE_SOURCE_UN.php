<?php

//@formatter:off

return [
    'name'    => 'ETAPE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'ETAPE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
        'ANNEE_ID',
    ],
];

//@formatter:on
