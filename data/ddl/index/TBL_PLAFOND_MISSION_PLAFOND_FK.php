<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_MISSION_PLAFOND_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_MISSION',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
