<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_EP_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'CENTRE_COUT_EP',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
