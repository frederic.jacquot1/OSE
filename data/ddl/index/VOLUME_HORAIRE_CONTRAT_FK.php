<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_CONTRAT_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'CONTRAT_ID',
    ],
];

//@formatter:on
