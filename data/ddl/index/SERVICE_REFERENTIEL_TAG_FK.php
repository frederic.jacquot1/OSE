<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_TAG_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'TAG_ID',
    ],
];

//@formatter:on
