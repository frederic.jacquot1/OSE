<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_RAISON_SOCIALE_IDX',
    'unique'  => FALSE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'RAISON_SOCIALE',
    ],
];

//@formatter:on
