<?php

//@formatter:off

return [
    'name'    => 'SNE_SCENARIO_NOEUD_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD_EFFECTIF',
    'columns' => [
        'SCENARIO_NOEUD_ID',
    ],
];

//@formatter:on
