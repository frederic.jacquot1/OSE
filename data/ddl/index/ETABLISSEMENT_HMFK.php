<?php

//@formatter:off

return [
    'name'    => 'ETABLISSEMENT_HMFK',
    'unique'  => FALSE,
    'table'   => 'ETABLISSEMENT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
