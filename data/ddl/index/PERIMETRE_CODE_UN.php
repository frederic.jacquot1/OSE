<?php

//@formatter:off

return [
    'name'    => 'PERIMETRE_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'PERIMETRE',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
