<?php

//@formatter:off

return [
    'name'    => 'MOTIF_NON_PAIEMENT_HDFK',
    'unique'  => FALSE,
    'table'   => 'MOTIF_NON_PAIEMENT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
