<?php

//@formatter:off

return [
    'name'    => 'VH_CHARGE_HMFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
