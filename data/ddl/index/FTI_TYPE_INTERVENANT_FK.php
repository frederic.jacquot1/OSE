<?php

//@formatter:off

return [
    'name'    => 'FTI_TYPE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_TEST_INTERVENANT',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
