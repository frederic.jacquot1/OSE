<?php

//@formatter:off

return [
    'name'    => 'TI_STATUT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
