<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STATUT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STATUT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
