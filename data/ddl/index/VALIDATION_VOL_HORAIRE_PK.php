<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_VOL_HORAIRE_PK',
    'unique'  => TRUE,
    'table'   => 'VALIDATION_VOL_HORAIRE',
    'columns' => [
        'VALIDATION_ID',
        'VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
