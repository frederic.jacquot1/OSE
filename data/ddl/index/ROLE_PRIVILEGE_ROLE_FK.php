<?php

//@formatter:off

return [
    'name'    => 'ROLE_PRIVILEGE_ROLE_FK',
    'unique'  => FALSE,
    'table'   => 'ROLE_PRIVILEGE',
    'columns' => [
        'ROLE_ID',
    ],
];

//@formatter:on
