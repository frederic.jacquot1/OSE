<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_MEP_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'MISE_EN_PAIEMENT_ID',
    ],
];

//@formatter:on
