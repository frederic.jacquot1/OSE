<?php

//@formatter:off

return [
    'name'    => 'SOURCE_PK',
    'unique'  => TRUE,
    'table'   => 'SOURCE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
