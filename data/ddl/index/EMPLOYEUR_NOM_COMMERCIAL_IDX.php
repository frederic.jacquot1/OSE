<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_NOM_COMMERCIAL_IDX',
    'unique'  => FALSE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'NOM_COMMERCIAL',
    ],
];

//@formatter:on
