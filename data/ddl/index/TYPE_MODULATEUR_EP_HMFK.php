<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_EP',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
