<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
