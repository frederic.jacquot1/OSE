<?php

//@formatter:off

return [
    'name'    => 'PRIVILEGE_CATEGORIE_FK',
    'unique'  => FALSE,
    'table'   => 'PRIVILEGE',
    'columns' => [
        'CATEGORIE_ID',
    ],
];

//@formatter:on
