<?php

//@formatter:off

return [
    'name'    => 'SEUIL_CH_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'SEUIL_CHARGE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
