<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
