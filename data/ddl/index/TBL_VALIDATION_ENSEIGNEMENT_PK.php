<?php

//@formatter:off

return [
    'name'    => 'TBL_VALIDATION_ENSEIGNEMENT_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
