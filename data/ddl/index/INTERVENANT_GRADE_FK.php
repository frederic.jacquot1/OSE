<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_GRADE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'GRADE_ID',
    ],
];

//@formatter:on
