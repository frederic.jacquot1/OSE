<?php

//@formatter:off

return [
    'name'    => 'FRES_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
