<?php

//@formatter:off

return [
    'name'    => 'TAUX_REMU_TAUX_REMU_FK',
    'unique'  => FALSE,
    'table'   => 'TAUX_REMU',
    'columns' => [
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
