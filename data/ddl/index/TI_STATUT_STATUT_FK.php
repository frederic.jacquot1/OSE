<?php

//@formatter:off

return [
    'name'    => 'TI_STATUT_STATUT_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'columns' => [
        'STATUT_ID',
    ],
];

//@formatter:on
