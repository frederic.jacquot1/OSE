<?php

//@formatter:off

return [
    'name'    => 'VVHM_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION_VOL_HORAIRE_MISS',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
