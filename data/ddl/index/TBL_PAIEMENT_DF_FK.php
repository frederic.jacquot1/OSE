<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_DF_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'DOMAINE_FONCTIONNEL_ID',
    ],
];

//@formatter:on
