<?php

//@formatter:off

return [
    'name'    => 'TI_STATUT_STATUT_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'columns' => [
        'TYPE_INTERVENTION_ID',
        'STATUT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
