<?php

//@formatter:off

return [
    'name'    => 'TVE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
