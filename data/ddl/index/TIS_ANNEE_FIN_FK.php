<?php

//@formatter:off

return [
    'name'    => 'TIS_ANNEE_FIN_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'columns' => [
        'ANNEE_FIN_ID',
    ],
];

//@formatter:on
