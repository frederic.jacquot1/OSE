<?php

//@formatter:off

return [
    'name'    => 'TPJS_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
