<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_HMFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
