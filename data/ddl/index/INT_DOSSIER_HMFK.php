<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_HMFK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
