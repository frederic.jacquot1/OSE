<?php

//@formatter:off

return [
    'name'    => 'TBL_CHARGENS_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
