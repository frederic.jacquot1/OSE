<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_PK',
    'unique'  => TRUE,
    'table'   => 'PIECE_JOINTE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
