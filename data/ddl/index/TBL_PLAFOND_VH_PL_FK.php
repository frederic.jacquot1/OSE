<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_VH_PL_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_VOLUME_HORAIRE',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
