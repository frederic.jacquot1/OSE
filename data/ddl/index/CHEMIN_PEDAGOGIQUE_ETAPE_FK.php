<?php

//@formatter:off

return [
    'name'    => 'CHEMIN_PEDAGOGIQUE_ETAPE_FK',
    'unique'  => FALSE,
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'columns' => [
        'ETAPE_ID',
    ],
];

//@formatter:on
