<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_GROUPE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_FORMATION',
    'columns' => [
        'GROUPE_ID',
    ],
];

//@formatter:on
