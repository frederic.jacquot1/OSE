<?php

//@formatter:off

return [
    'name'    => 'TBL_PJF_TYPE_PIECE_JOINTE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
    ],
];

//@formatter:on
