<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_VFK',
    'unique'  => FALSE,
    'table'   => 'PIECE_JOINTE',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
