<?php

//@formatter:off

return [
    'name'    => 'TBL_PLA_INT_ETAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_INTERVENANT',
    'columns' => [
        'PLAFOND_ETAT_ID',
    ],
];

//@formatter:on
