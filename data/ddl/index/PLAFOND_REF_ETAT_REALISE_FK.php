<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REF_ETAT_REALISE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_REFERENTIEL',
    'columns' => [
        'PLAFOND_ETAT_REALISE_ID',
    ],
];

//@formatter:on
