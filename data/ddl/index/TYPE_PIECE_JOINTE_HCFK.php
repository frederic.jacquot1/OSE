<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_PIECE_JOINTE',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
