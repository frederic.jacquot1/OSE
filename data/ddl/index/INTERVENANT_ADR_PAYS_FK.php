<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_ADR_PAYS_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'ADRESSE_PAYS_ID',
    ],
];

//@formatter:on
