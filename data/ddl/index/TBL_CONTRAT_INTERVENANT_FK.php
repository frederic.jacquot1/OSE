<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
