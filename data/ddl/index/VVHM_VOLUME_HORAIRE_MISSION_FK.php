<?php

//@formatter:off

return [
    'name'    => 'VVHM_VOLUME_HORAIRE_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION_VOL_HORAIRE_MISS',
    'columns' => [
        'VOLUME_HORAIRE_MISSION_ID',
    ],
];

//@formatter:on
