<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'SERVICE_ID',
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
