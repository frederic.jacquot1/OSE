<?php

//@formatter:off

return [
    'name'    => 'CCEP_TYPE_HEURES_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_EP',
    'columns' => [
        'TYPE_HEURES_ID',
    ],
];

//@formatter:on
