<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'MISSION_ID',
    ],
];

//@formatter:on
