<?php

//@formatter:off

return [
    'name'    => 'VHM_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_MISSION',
    'columns' => [
        'MISSION_ID',
    ],
];

//@formatter:on
