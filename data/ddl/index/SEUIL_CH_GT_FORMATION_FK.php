<?php

//@formatter:off

return [
    'name'    => 'SEUIL_CH_GT_FORMATION_FK',
    'unique'  => FALSE,
    'table'   => 'SEUIL_CHARGE',
    'columns' => [
        'GROUPE_TYPE_FORMATION_ID',
    ],
];

//@formatter:on
