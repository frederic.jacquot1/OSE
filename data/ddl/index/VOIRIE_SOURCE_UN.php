<?php

//@formatter:off

return [
    'name'    => 'VOIRIE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'VOIRIE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
