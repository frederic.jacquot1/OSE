<?php

//@formatter:off

return [
    'name'    => 'ADRESSE_NUMERO_COMPL_UN',
    'unique'  => TRUE,
    'table'   => 'ADRESSE_NUMERO_COMPL',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
