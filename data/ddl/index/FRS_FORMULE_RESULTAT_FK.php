<?php

//@formatter:off

return [
    'name'    => 'FRS_FORMULE_RESULTAT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_SERVICE',
    'columns' => [
        'FORMULE_RESULTAT_ID',
    ],
];

//@formatter:on
