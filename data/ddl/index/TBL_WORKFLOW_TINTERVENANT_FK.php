<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_TINTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
