<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_TYPE_MISSION_HDFK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_TYPE_MISSION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
