<?php

//@formatter:off

return [
    'name'    => 'TBL_PIECE_JOINTE_DEMANDE_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PIECE_JOINTE_DEMANDE',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'INTERVENANT_ID',
    ],
];

//@formatter:on
