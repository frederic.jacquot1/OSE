<?php

//@formatter:off

return [
    'name'    => 'TVR_SERVICE_REFERENTIEL_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
