<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_ELEMENT_INT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_ELEMENT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
