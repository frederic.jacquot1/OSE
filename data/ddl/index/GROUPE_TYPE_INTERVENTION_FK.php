<?php

//@formatter:off

return [
    'name'    => 'GROUPE_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'GROUPE',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
