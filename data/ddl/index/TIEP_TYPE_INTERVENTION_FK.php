<?php

//@formatter:off

return [
    'name'    => 'TIEP_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_EP',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
