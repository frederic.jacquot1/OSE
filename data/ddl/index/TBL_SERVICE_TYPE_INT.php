<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_TYPE_INT',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'TYPE_INTERVENANT_CODE',
    ],
];

//@formatter:on
