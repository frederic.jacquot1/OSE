<?php

//@formatter:off

return [
    'name'    => 'MISSION_ETUDIANT_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION_ETUDIANT',
    'columns' => [
        'MISSION_ID',
    ],
];

//@formatter:on
