<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_HCFK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
