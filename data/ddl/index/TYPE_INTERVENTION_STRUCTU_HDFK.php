<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_STRUCTU_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
