<?php

//@formatter:off

return [
    'name'    => 'CS_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'CAMPAGNE_SAISIE',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
