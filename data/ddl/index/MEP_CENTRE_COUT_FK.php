<?php

//@formatter:off

return [
    'name'    => 'MEP_CENTRE_COUT_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'CENTRE_COUT_ID',
    ],
];

//@formatter:on
