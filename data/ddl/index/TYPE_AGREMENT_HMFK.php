<?php

//@formatter:off

return [
    'name'    => 'TYPE_AGREMENT_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_AGREMENT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
