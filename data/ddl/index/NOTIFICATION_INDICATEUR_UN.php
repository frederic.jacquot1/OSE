<?php

//@formatter:off

return [
    'name'    => 'NOTIFICATION_INDICATEUR_UN',
    'unique'  => TRUE,
    'table'   => 'NOTIFICATION_INDICATEUR',
    'columns' => [
        'INDICATEUR_ID',
        'AFFECTATION_ID',
    ],
];

//@formatter:on
