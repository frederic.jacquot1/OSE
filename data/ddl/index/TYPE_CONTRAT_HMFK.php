<?php

//@formatter:off

return [
    'name'    => 'TYPE_CONTRAT_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_CONTRAT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
