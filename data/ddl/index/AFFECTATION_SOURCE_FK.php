<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
