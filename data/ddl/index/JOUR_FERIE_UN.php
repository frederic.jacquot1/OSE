<?php

//@formatter:off

return [
    'name'    => 'JOUR_FERIE_UN',
    'unique'  => TRUE,
    'table'   => 'JOUR_FERIE',
    'columns' => [
        'DATE_JOUR',
    ],
];

//@formatter:on
