<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_NUMERO_AVENANT_UN',
    'unique'  => TRUE,
    'table'   => 'CONTRAT',
    'columns' => [
        'INTERVENANT_ID',
        'STRUCTURE_ID',
        'NUMERO_AVENANT',
        'VALIDATION_ID',
        'HISTO_DESTRUCTION',
        'MISSION_ID',
    ],
];

//@formatter:on
