<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_STATUT_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'STATUT_ID',
        'HISTO_DESTRUCTION',
        'NUM_REGLE',
    ],
];

//@formatter:on
