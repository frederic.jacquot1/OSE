<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_OFFRE_EMPL_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CANDIDATURE',
    'columns' => [
        'OFFRE_EMPLOI_ID',
    ],
];

//@formatter:on
