<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'PIECE_JOINTE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
