<?php

//@formatter:off

return [
    'name'    => 'DOTATION_TYPE_RESSOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'DOTATION',
    'columns' => [
        'TYPE_RESSOURCE_ID',
    ],
];

//@formatter:on
