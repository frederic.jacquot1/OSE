<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_STATUT_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
