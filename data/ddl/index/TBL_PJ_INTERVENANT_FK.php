<?php

//@formatter:off

return [
    'name'    => 'TBL_PJ_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
