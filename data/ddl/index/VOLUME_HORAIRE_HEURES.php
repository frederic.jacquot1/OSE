<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_HEURES',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'HEURES',
    ],
];

//@formatter:on
