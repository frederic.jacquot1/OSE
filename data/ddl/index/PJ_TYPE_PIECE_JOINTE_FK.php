<?php

//@formatter:off

return [
    'name'    => 'PJ_TYPE_PIECE_JOINTE_FK',
    'unique'  => FALSE,
    'table'   => 'PIECE_JOINTE',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
    ],
];

//@formatter:on
