<?php

//@formatter:off

return [
    'name'    => 'TME_TYPE_MODULATEUR_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_EP',
    'columns' => [
        'TYPE_MODULATEUR_ID',
    ],
];

//@formatter:on
