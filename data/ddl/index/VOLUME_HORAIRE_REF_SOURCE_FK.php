<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_REF_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_REF',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
