<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_STATUT_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'STATUT_ID',
    ],
];

//@formatter:on
