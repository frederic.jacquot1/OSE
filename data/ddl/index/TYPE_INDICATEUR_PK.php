<?php

//@formatter:off

return [
    'name'    => 'TYPE_INDICATEUR_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_INDICATEUR',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
