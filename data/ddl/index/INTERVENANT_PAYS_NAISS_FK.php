<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_PAYS_NAISS_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'PAYS_NAISSANCE_ID',
    ],
];

//@formatter:on
