<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STATUT_ETAT_REALISE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STATUT',
    'columns' => [
        'PLAFOND_ETAT_REALISE_ID',
    ],
];

//@formatter:on
