<?php

//@formatter:off

return [
    'name'    => 'TMS_ANNEE_DEBUT_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'ANNEE_DEBUT_ID',
    ],
];

//@formatter:on
