<?php

//@formatter:off

return [
    'name'    => 'NOTE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'NOTE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
