<?php

//@formatter:off

return [
    'name'    => 'TBL_DOSSIER_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_DOSSIER',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
