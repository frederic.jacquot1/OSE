<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCT_ETAT_REALISE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STRUCTURE',
    'columns' => [
        'PLAFOND_ETAT_REALISE_ID',
    ],
];

//@formatter:on
