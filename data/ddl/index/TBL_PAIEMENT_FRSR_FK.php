<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_FRSR_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'FORMULE_RES_SERVICE_REF_ID',
    ],
];

//@formatter:on
