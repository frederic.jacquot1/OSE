<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
