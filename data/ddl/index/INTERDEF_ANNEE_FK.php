<?php

//@formatter:off

return [
    'name'    => 'INTERDEF_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_PAR_DEFAUT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
