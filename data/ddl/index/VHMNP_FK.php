<?php

//@formatter:off

return [
    'name'    => 'VHMNP_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'MOTIF_NON_PAIEMENT_ID',
    ],
];

//@formatter:on
