<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_ETAPE_HCFK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS_ETAPE',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
