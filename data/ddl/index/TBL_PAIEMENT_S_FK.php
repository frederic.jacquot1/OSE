<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_S_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'SERVICE_ID',
    ],
];

//@formatter:on
