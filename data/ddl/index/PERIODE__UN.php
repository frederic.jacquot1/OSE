<?php

//@formatter:off

return [
    'name'    => 'PERIODE__UN',
    'unique'  => TRUE,
    'table'   => 'PERIODE',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
