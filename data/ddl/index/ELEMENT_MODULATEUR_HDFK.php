<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_MODULATEUR_HDFK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_MODULATEUR',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
