<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'CODE',
        'ANNEE_ID',
        'STATUT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
