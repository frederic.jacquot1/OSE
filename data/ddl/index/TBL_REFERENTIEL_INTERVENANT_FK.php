<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
