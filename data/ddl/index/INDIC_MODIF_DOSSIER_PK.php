<?php

//@formatter:off

return [
    'name'    => 'INDIC_MODIF_DOSSIER_PK',
    'unique'  => TRUE,
    'table'   => 'INDIC_MODIF_DOSSIER',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
