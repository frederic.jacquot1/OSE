<?php

//@formatter:off

return [
    'name'    => 'FORMULE__UN',
    'unique'  => TRUE,
    'table'   => 'FORMULE',
    'columns' => [
        'LIBELLE',
    ],
];

//@formatter:on
