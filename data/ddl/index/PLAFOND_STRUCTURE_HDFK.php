<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCTURE_HDFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STRUCTURE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
