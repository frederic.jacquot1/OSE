<?php

//@formatter:off

return [
    'name'    => 'TBL_PJ_TYPE_PIECE_JOINTE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
    ],
];

//@formatter:on
