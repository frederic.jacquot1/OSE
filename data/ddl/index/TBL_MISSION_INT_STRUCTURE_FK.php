<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_INT_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'INTERVENANT_STRUCTURE_ID',
    ],
];

//@formatter:on
