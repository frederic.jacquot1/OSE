<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_FRS_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'FORMULE_RES_SERVICE_ID',
    ],
];

//@formatter:on
