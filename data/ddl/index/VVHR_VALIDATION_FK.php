<?php

//@formatter:off

return [
    'name'    => 'VVHR_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION_VOL_HORAIRE_REF',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
