<?php

//@formatter:off

return [
    'name'    => 'MISSION_TYPE_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION',
    'columns' => [
        'TYPE_MISSION_ID',
    ],
];

//@formatter:on
