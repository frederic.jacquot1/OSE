<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_HMFK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
