<?php

//@formatter:off

return [
    'name'    => 'NOTIF_INDIC_INDICATEUR_FK',
    'unique'  => FALSE,
    'table'   => 'NOTIFICATION_INDICATEUR',
    'columns' => [
        'INDICATEUR_ID',
    ],
];

//@formatter:on
