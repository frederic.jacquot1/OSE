<?php

//@formatter:off

return [
    'name'    => 'INTERDEF_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_PAR_DEFAUT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
