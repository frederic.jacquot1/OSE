<?php

//@formatter:off

return [
    'name'    => 'VH_CHARGE_UN',
    'unique'  => TRUE,
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'TYPE_INTERVENTION_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
