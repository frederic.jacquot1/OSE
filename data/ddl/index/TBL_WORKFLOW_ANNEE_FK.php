<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
