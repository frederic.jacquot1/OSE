<?php

//@formatter:off

return [
    'name'    => 'TYPE_SERVICE_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_SERVICE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
