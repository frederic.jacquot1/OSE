<?php

//@formatter:off

return [
    'name'    => 'CPEP_FK',
    'unique'  => FALSE,
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
