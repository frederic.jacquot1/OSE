<?php

//@formatter:off

return [
    'name'    => 'TYPE_MISSION_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MISSION',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
