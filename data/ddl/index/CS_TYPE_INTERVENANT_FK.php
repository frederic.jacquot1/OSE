<?php

//@formatter:off

return [
    'name'    => 'CS_TYPE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'CAMPAGNE_SAISIE',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
