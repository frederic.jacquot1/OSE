<?php

//@formatter:off

return [
    'name'    => 'MEP_TYPE_HEURES_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'TYPE_HEURES_ID',
    ],
];

//@formatter:on
