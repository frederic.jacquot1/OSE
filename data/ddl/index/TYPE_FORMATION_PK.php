<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_FORMATION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
