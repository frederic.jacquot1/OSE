<?php

//@formatter:off

return [
    'name'    => 'TBL_LIEN_NOEUD_SUP_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_LIEN',
    'columns' => [
        'NOEUD_SUP_ID',
    ],
];

//@formatter:on
