<?php

//@formatter:off

return [
    'name'    => 'FRVHR_VOLUME_HORAIRE_REF_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_VH_REF',
    'columns' => [
        'VOLUME_HORAIRE_REF_ID',
    ],
];

//@formatter:on
