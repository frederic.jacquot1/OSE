<?php

//@formatter:off

return [
    'name'    => 'INDICATEUR_PK',
    'unique'  => TRUE,
    'table'   => 'INDICATEUR',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
