<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_RESSOURCE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
