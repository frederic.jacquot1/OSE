<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_HDFK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
