<?php

//@formatter:off

return [
    'name'    => 'TBL_PIECE_JOINTE_FOURNIE_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'INTERVENANT_ID',
        'VALIDATION_ID',
        'FICHIER_ID',
    ],
];

//@formatter:on
