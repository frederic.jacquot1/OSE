<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_FA',
    'unique'  => FALSE,
    'table'   => 'TYPE_RESSOURCE',
    'columns' => [
        'FA',
    ],
];

//@formatter:on
