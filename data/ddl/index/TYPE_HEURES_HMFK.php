<?php

//@formatter:off

return [
    'name'    => 'TYPE_HEURES_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_HEURES',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
