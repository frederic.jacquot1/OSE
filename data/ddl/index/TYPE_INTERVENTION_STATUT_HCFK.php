<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_STATUT_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
