<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_CANDIDATURE',
    'columns' => [
        'ANNEE_ID',
        'INTERVENANT_ID',
        'OFFRE_EMPLOI_ID',
    ],
];

//@formatter:on
