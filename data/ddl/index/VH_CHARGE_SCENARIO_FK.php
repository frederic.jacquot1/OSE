<?php

//@formatter:off

return [
    'name'    => 'VH_CHARGE_SCENARIO_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'columns' => [
        'SCENARIO_ID',
    ],
];

//@formatter:on
