<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_DEPARTEMENT_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'DEPARTEMENT_NAISSANCE_ID',
    ],
];

//@formatter:on
