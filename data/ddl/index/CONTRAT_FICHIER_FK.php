<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_FICHIER_FK',
    'unique'  => FALSE,
    'table'   => 'CONTRAT_FICHIER',
    'columns' => [
        'CONTRAT_ID',
    ],
];

//@formatter:on
