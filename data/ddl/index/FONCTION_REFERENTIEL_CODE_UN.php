<?php

//@formatter:off

return [
    'name'    => 'FONCTION_REFERENTIEL_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'FONCTION_REFERENTIEL',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
