<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_PRENOM',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'PRENOM',
    ],
];

//@formatter:on
