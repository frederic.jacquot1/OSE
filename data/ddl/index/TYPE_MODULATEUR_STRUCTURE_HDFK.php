<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_STRUCTURE_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
