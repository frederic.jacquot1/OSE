<?php

//@formatter:off

return [
    'name'    => 'TBL_VALIDATION_REFERENTIEL_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'INTERVENANT_ID',
        'STRUCTURE_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'SERVICE_REFERENTIEL_ID',
        'VOLUME_HORAIRE_REF_ID',
        'VALIDATION_ID',
    ],
];

//@formatter:on
