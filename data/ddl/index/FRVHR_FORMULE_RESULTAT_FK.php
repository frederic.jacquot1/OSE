<?php

//@formatter:off

return [
    'name'    => 'FRVHR_FORMULE_RESULTAT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_VH_REF',
    'columns' => [
        'FORMULE_RESULTAT_ID',
    ],
];

//@formatter:on
