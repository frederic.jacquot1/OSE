<?php

//@formatter:off

return [
    'name'    => 'TBL_DMLIQ_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_DMEP_LIQUIDATION',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
