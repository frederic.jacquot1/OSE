<?php

//@formatter:off

return [
    'name'    => 'TYPE_AGREMENT_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_AGREMENT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
