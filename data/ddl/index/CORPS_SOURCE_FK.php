<?php

//@formatter:off

return [
    'name'    => 'CORPS_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'CORPS',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
