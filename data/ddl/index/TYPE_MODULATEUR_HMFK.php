<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
