<?php

//@formatter:off

return [
    'name'    => 'ROLE_PERIMETRE_FK',
    'unique'  => FALSE,
    'table'   => 'ROLE',
    'columns' => [
        'PERIMETRE_ID',
    ],
];

//@formatter:on
