<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_LISTE_NOIRE_PK',
    'unique'  => TRUE,
    'table'   => 'LISTE_NOIRE',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
