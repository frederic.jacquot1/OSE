<?php

//@formatter:off

return [
    'name'    => 'ETAPE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'ETAPE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
