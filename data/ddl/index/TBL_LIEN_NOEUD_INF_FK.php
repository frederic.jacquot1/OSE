<?php

//@formatter:off

return [
    'name'    => 'TBL_LIEN_NOEUD_INF_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_LIEN',
    'columns' => [
        'NOEUD_INF_ID',
    ],
];

//@formatter:on
