<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REFERENTIEL_UN',
    'unique'  => TRUE,
    'table'   => 'PLAFOND_REFERENTIEL',
    'columns' => [
        'FONCTION_REFERENTIEL_ID',
        'PLAFOND_ID',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
