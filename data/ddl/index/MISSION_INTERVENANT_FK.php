<?php

//@formatter:off

return [
    'name'    => 'MISSION_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
