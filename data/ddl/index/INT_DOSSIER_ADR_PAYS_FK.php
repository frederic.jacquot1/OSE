<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_ADR_PAYS_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'ADRESSE_PAYS_ID',
    ],
];

//@formatter:on
