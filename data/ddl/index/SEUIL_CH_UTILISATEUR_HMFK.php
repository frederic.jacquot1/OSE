<?php

//@formatter:off

return [
    'name'    => 'SEUIL_CH_UTILISATEUR_HMFK',
    'unique'  => FALSE,
    'table'   => 'SEUIL_CHARGE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
