<?php

//@formatter:off

return [
    'name'    => 'PARAMETRE_PK',
    'unique'  => TRUE,
    'table'   => 'PARAMETRE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
