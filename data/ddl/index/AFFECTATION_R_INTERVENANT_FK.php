<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_R_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION_RECHERCHE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
