<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_MISSION_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_MISSION',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
