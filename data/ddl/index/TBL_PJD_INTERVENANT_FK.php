<?php

//@formatter:off

return [
    'name'    => 'TBL_PJD_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE_DEMANDE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
