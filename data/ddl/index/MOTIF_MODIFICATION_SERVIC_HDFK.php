<?php

//@formatter:off

return [
    'name'    => 'MOTIF_MODIFICATION_SERVIC_HDFK',
    'unique'  => FALSE,
    'table'   => 'MOTIF_MODIFICATION_SERVICE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
