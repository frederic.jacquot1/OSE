<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_FORMATION',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
