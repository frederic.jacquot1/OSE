<?php

//@formatter:off

return [
    'name'    => 'TBCH_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
