<?php

//@formatter:off

return [
    'name'    => 'MEP_FR_SERVICE_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'FORMULE_RES_SERVICE_ID',
    ],
];

//@formatter:on
