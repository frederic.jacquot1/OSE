<?php

//@formatter:off

return [
    'name'    => 'ETABLISSEMENT_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'ETABLISSEMENT',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
