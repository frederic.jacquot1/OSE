<?php

//@formatter:off

return [
    'name'    => 'DEPARTEMENT_PK',
    'unique'  => TRUE,
    'table'   => 'DEPARTEMENT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
