<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_ETAPE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'ETAPE_ID',
    ],
];

//@formatter:on
