<?php

//@formatter:off

return [
    'name'    => 'NOTE_PK',
    'unique'  => TRUE,
    'table'   => 'NOTE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
