<?php

//@formatter:off

return [
    'name'    => 'FTI_FORMULE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_TEST_INTERVENANT',
    'columns' => [
        'FORMULE_ID',
    ],
];

//@formatter:on
