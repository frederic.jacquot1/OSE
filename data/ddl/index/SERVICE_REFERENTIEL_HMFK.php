<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_HMFK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
