<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_HDFK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
