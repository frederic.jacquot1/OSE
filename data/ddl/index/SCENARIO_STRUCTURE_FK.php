<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
