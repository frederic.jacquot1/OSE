<?php

//@formatter:off

return [
    'name'    => 'TIEP_ELEMENT_PEDAGOGIQUE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_EP',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
