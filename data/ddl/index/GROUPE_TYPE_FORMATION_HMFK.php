<?php

//@formatter:off

return [
    'name'    => 'GROUPE_TYPE_FORMATION_HMFK',
    'unique'  => FALSE,
    'table'   => 'GROUPE_TYPE_FORMATION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
