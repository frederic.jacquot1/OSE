<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'INTERVENANT_ID',
        'SERVICE_REFERENTIEL_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'STRUCTURE_ID',
    ],
];

//@formatter:on
