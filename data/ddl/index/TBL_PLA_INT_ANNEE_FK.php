<?php

//@formatter:off

return [
    'name'    => 'TBL_PLA_INT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_INTERVENANT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
