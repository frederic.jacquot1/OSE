<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_STRUCTU_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
