<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCTURE_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STRUCTURE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
