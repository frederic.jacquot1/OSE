<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_MISSI_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_MISSION',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
