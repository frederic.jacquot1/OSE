<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_HCFK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
