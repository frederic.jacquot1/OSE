<?php

//@formatter:off

return [
    'name'    => 'NOEUD_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'NOEUD',
    'columns' => [
        'SOURCE_CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
