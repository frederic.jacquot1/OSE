<?php

//@formatter:off

return [
    'name'    => 'ETABLISSEMENT_PK',
    'unique'  => TRUE,
    'table'   => 'ETABLISSEMENT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
