<?php

//@formatter:off

return [
    'name'    => 'TEST_BUFFER_PK',
    'unique'  => TRUE,
    'table'   => 'TEST_BUFFER',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
