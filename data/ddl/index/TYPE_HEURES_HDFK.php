<?php

//@formatter:off

return [
    'name'    => 'TYPE_HEURES_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_HEURES',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
