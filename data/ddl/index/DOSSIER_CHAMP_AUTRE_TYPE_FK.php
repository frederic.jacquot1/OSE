<?php

//@formatter:off

return [
    'name'    => 'DOSSIER_CHAMP_AUTRE_TYPE_FK',
    'unique'  => FALSE,
    'table'   => 'DOSSIER_CHAMP_AUTRE',
    'columns' => [
        'DOSSIER_CHAMP_AUTRE_TYPE_ID',
    ],
];

//@formatter:on
