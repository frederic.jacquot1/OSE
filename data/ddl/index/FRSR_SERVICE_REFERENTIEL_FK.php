<?php

//@formatter:off

return [
    'name'    => 'FRSR_SERVICE_REFERENTIEL_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_SERVICE_REF',
    'columns' => [
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
