<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_EP_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_EP',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
