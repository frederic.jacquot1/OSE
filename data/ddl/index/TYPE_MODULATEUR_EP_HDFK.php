<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_EP',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
