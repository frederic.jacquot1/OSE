<?php

//@formatter:off

return [
    'name'    => 'EM_MODULATEUR_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_MODULATEUR',
    'columns' => [
        'MODULATEUR_ID',
    ],
];

//@formatter:on
