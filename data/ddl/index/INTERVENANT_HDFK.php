<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_HDFK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
