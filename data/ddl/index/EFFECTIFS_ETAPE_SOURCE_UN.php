<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_ETAPE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'EFFECTIFS_ETAPE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
