<?php

//@formatter:off

return [
    'name'    => 'TVE_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
