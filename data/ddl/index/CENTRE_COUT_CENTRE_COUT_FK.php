<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_CENTRE_COUT_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT',
    'columns' => [
        'PARENT_ID',
    ],
];

//@formatter:on
