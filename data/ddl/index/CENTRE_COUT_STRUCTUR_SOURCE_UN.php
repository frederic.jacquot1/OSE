<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_STRUCTUR_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
