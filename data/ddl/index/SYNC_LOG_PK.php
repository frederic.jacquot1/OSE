<?php

//@formatter:off

return [
    'name'    => 'SYNC_LOG_PK',
    'unique'  => TRUE,
    'table'   => 'SYNC_LOG',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
