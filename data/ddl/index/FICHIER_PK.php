<?php

//@formatter:off

return [
    'name'    => 'FICHIER_PK',
    'unique'  => TRUE,
    'table'   => 'FICHIER',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
