<?php

//@formatter:off

return [
    'name'    => 'SEUIL_CH_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'SEUIL_CHARGE',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
