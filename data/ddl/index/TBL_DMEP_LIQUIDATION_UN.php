<?php

//@formatter:off

return [
    'name'    => 'TBL_DMEP_LIQUIDATION_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_DMEP_LIQUIDATION',
    'columns' => [
        'ANNEE_ID',
        'TYPE_RESSOURCE_ID',
        'STRUCTURE_ID',
    ],
];

//@formatter:on
