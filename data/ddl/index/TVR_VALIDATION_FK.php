<?php

//@formatter:off

return [
    'name'    => 'TVR_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
