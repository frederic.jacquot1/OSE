<?php

//@formatter:off

return [
    'name'    => 'TBL_AGR_TYPE_AGREMENT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_AGREMENT',
    'columns' => [
        'TYPE_AGREMENT_ID',
    ],
];

//@formatter:on
