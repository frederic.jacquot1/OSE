<?php

//@formatter:off

return [
    'name'    => 'FTI_ETAT_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_TEST_INTERVENANT',
    'columns' => [
        'ETAT_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
