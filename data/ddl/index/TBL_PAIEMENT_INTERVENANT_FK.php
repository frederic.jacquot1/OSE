<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
