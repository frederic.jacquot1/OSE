<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCT_ETAT_PREVU_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STRUCTURE',
    'columns' => [
        'PLAFOND_ETAT_PREVU_ID',
    ],
];

//@formatter:on
