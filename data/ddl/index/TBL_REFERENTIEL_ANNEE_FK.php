<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
