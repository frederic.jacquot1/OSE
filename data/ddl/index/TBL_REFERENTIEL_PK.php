<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
