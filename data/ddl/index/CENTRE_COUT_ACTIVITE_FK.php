<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_ACTIVITE_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT',
    'columns' => [
        'ACTIVITE_ID',
    ],
];

//@formatter:on
