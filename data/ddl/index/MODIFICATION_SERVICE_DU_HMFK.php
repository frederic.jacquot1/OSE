<?php

//@formatter:off

return [
    'name'    => 'MODIFICATION_SERVICE_DU_HMFK',
    'unique'  => FALSE,
    'table'   => 'MODIFICATION_SERVICE_DU',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
