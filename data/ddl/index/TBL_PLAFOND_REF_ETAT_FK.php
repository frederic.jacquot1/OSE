<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_REF_ETAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_REFERENTIEL',
    'columns' => [
        'PLAFOND_ETAT_ID',
    ],
];

//@formatter:on
