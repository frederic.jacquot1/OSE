<?php

//@formatter:off

return [
    'name'    => 'NOEUD_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'NOEUD',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
