<?php

//@formatter:off

return [
    'name'    => 'TVE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
