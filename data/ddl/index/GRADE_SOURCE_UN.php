<?php

//@formatter:off

return [
    'name'    => 'GRADE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'GRADE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
