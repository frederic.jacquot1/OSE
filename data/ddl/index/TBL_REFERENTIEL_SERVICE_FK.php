<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_SERVICE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
