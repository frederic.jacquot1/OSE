<?php

//@formatter:off

return [
    'name'    => 'VVH_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION_VOL_HORAIRE',
    'columns' => [
        'VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
