<?php

//@formatter:off

return [
    'name'    => 'STATUT_ETAT_SORTIE_FK',
    'unique'  => FALSE,
    'table'   => 'STATUT',
    'columns' => [
        'CONTRAT_ETAT_SORTIE_ID',
    ],
];

//@formatter:on
