<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_FORMATION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
