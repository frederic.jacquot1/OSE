<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_RESSOURCE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
