<?php

//@formatter:off

return [
    'name'    => 'GRADE_PK',
    'unique'  => TRUE,
    'table'   => 'GRADE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
