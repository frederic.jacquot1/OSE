<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_STRUCTURE_HDFK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
