<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_MAIL_MAIL_PK',
    'unique'  => TRUE,
    'table'   => 'UNICAEN_MAIL_MAIL',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
