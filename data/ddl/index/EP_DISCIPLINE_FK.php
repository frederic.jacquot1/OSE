<?php

//@formatter:off

return [
    'name'    => 'EP_DISCIPLINE_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'DISCIPLINE_ID',
    ],
];

//@formatter:on
