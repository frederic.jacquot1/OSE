<?php

//@formatter:off

return [
    'name'    => 'FORMULE_RESULTAT_TYPE_INT',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT',
    'columns' => [
        'TYPE_INTERVENANT_CODE',
    ],
];

//@formatter:on
