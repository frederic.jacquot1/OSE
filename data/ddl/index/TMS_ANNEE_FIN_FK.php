<?php

//@formatter:off

return [
    'name'    => 'TMS_ANNEE_FIN_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'ANNEE_FIN_ID',
    ],
];

//@formatter:on
