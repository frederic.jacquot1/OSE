<?php

//@formatter:off

return [
    'name'    => 'FORMULE_TEST_INTERVENANT_PK',
    'unique'  => TRUE,
    'table'   => 'FORMULE_TEST_INTERVENANT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
