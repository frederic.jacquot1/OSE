<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_NOM_USUEL',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'NOM_USUEL',
    ],
];

//@formatter:on
