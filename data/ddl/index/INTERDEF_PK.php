<?php

//@formatter:off

return [
    'name'    => 'INTERDEF_PK',
    'unique'  => TRUE,
    'table'   => 'INTERVENANT_PAR_DEFAUT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
