<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STATUT_STATUT_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STATUT',
    'columns' => [
        'STATUT_ID',
    ],
];

//@formatter:on
