<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCTURE_HMFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STRUCTURE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
