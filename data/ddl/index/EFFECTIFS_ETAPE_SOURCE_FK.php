<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_ETAPE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS_ETAPE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
