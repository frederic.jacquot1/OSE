<?php

//@formatter:off

return [
    'name'    => 'DOTATION_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'DOTATION',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
