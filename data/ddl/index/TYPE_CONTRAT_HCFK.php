<?php

//@formatter:off

return [
    'name'    => 'TYPE_CONTRAT_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_CONTRAT',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
