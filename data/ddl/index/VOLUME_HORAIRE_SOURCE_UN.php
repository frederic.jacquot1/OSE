<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
