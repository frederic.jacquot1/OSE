<?php

//@formatter:off

return [
    'name'    => 'TBL_AGR_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_AGREMENT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
