<?php

//@formatter:off

return [
    'name'    => 'TBL_CSD_GTF_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS_SEUILS_DEF',
    'columns' => [
        'GROUPE_TYPE_FORMATION_ID',
    ],
];

//@formatter:on
