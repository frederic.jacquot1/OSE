<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'CONTRAT',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
