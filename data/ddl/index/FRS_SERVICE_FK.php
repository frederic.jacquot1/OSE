<?php

//@formatter:off

return [
    'name'    => 'FRS_SERVICE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_SERVICE',
    'columns' => [
        'SERVICE_ID',
    ],
];

//@formatter:on
