<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_ETAPE_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'ETAPE_ID',
    ],
];

//@formatter:on
