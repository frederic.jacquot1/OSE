<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
