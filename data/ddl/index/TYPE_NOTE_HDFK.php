<?php

//@formatter:off

return [
    'name'    => 'TYPE_NOTE_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_NOTE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
