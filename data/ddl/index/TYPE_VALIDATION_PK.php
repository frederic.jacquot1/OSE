<?php

//@formatter:off

return [
    'name'    => 'TYPE_VALIDATION_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_VALIDATION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
