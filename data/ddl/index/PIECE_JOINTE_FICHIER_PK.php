<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_FICHIER_PK',
    'unique'  => TRUE,
    'table'   => 'PIECE_JOINTE_FICHIER',
    'columns' => [
        'PIECE_JOINTE_ID',
        'FICHIER_ID',
    ],
];

//@formatter:on
