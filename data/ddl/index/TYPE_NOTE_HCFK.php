<?php

//@formatter:off

return [
    'name'    => 'TYPE_NOTE_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_NOTE',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
