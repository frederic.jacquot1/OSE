<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_HMFK',
    'unique'  => FALSE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
