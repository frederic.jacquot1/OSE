<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
