<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'MISSION_ID',
    ],
];

//@formatter:on
