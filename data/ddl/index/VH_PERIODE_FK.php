<?php

//@formatter:off

return [
    'name'    => 'VH_PERIODE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'PERIODE_ID',
    ],
];

//@formatter:on
