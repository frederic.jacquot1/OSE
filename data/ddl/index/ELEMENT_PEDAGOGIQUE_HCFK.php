<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_HCFK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
