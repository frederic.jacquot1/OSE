<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
