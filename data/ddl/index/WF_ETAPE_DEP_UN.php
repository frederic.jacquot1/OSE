<?php

//@formatter:off

return [
    'name'    => 'WF_ETAPE_DEP_UN',
    'unique'  => TRUE,
    'table'   => 'WF_ETAPE_DEP',
    'columns' => [
        'ETAPE_SUIV_ID',
        'ETAPE_PREC_ID',
    ],
];

//@formatter:on
