<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_FK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
