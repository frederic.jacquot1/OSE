<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_VOL_HORAIRE_MISS_PK',
    'unique'  => TRUE,
    'table'   => 'VALIDATION_VOL_HORAIRE_MISS',
    'columns' => [
        'VALIDATION_ID',
        'VOLUME_HORAIRE_MISSION_ID',
    ],
];

//@formatter:on
