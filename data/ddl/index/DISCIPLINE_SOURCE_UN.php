<?php

//@formatter:off

return [
    'name'    => 'DISCIPLINE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'DISCIPLINE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
