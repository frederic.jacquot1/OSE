<?php

//@formatter:off

return [
    'name'    => 'FONC_REF_DOMAINE_FONCT_FK',
    'unique'  => FALSE,
    'table'   => 'FONCTION_REFERENTIEL',
    'columns' => [
        'DOMAINE_FONCTIONNEL_ID',
    ],
];

//@formatter:on
