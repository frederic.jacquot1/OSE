<?php

//@formatter:off

return [
    'name'    => 'SERV_REF_MOTIF_NON_PAIEMENT_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'MOTIF_NON_PAIEMENT_ID',
    ],
];

//@formatter:on
