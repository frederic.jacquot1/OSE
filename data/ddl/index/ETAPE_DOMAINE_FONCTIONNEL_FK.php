<?php

//@formatter:off

return [
    'name'    => 'ETAPE_DOMAINE_FONCTIONNEL_FK',
    'unique'  => FALSE,
    'table'   => 'ETAPE',
    'columns' => [
        'DOMAINE_FONCTIONNEL_ID',
    ],
];

//@formatter:on
