<?php

//@formatter:off

return [
    'name'    => 'VH_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
