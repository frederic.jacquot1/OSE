<?php

//@formatter:off

return [
    'name'    => 'FTI_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_TEST_INTERVENANT',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
