<?php

//@formatter:off

return [
    'name'    => 'SERVICE_PK',
    'unique'  => TRUE,
    'table'   => 'SERVICE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
