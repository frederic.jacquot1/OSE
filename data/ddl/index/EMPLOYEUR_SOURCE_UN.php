<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
