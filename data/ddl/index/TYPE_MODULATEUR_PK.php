<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_MODULATEUR',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
