<?php

//@formatter:off

return [
    'name'    => 'ETAT_VOLUME_HORAIRE__UN',
    'unique'  => TRUE,
    'table'   => 'ETAT_VOLUME_HORAIRE',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
