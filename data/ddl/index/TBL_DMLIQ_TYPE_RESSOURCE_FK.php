<?php

//@formatter:off

return [
    'name'    => 'TBL_DMLIQ_TYPE_RESSOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_DMEP_LIQUIDATION',
    'columns' => [
        'TYPE_RESSOURCE_ID',
    ],
];

//@formatter:on
