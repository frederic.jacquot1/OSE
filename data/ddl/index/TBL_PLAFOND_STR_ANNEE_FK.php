<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_STR_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_STRUCTURE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
