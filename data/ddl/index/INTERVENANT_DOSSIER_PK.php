<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_DOSSIER_PK',
    'unique'  => TRUE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
