<?php

//@formatter:off

return [
    'name'    => 'WF_ETAPE_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'WF_ETAPE',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
