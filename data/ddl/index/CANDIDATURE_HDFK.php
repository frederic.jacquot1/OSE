<?php

//@formatter:off

return [
    'name'    => 'CANDIDATURE_HDFK',
    'unique'  => FALSE,
    'table'   => 'CANDIDATURE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
