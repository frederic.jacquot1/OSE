<?php

//@formatter:off

return [
    'name'    => 'TVR_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
