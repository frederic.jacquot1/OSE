<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_NOM_PATRONYMIQUE',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'NOM_PATRONYMIQUE',
    ],
];

//@formatter:on
