<?php

//@formatter:off

return [
    'name'    => 'TBL_AGREMENT_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_AGREMENT',
    'columns' => [
        'TYPE_AGREMENT_ID',
        'INTERVENANT_ID',
        'STRUCTURE_ID',
        'ANNEE_AGREMENT',
    ],
];

//@formatter:on
