<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_R_HDFK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION_RECHERCHE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
