<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_HMFK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
