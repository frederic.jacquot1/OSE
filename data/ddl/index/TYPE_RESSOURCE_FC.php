<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_FC',
    'unique'  => FALSE,
    'table'   => 'TYPE_RESSOURCE',
    'columns' => [
        'FC',
    ],
];

//@formatter:on
