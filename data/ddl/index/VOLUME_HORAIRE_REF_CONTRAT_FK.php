<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_REF_CONTRAT_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_REF',
    'columns' => [
        'CONTRAT_ID',
    ],
];

//@formatter:on
