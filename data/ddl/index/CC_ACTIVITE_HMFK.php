<?php

//@formatter:off

return [
    'name'    => 'CC_ACTIVITE_HMFK',
    'unique'  => FALSE,
    'table'   => 'CC_ACTIVITE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
