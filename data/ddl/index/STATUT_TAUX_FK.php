<?php

//@formatter:off

return [
    'name'    => 'STATUT_TAUX_FK',
    'unique'  => FALSE,
    'table'   => 'STATUT',
    'columns' => [
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
