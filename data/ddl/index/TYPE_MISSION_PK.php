<?php

//@formatter:off

return [
    'name'    => 'TYPE_MISSION_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_MISSION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
