<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_UN',
    'unique'  => TRUE,
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
