<?php

//@formatter:off

return [
    'name'    => 'TBL_CANDIDATURE_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_CANDIDATURE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
