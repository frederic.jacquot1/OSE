<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_HMFK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
