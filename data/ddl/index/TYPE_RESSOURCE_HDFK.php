<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_RESSOURCE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
