<?php

//@formatter:off

return [
    'name'    => 'TBL_PLA_INT_INT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_INTERVENANT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
