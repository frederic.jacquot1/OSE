<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_STR_STR_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_STRUCTURE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
