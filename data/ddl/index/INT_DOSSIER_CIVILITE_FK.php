<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_CIVILITE_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'CIVILITE_ID',
    ],
];

//@formatter:on
