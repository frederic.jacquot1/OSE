<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_STR_TVH_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_STRUCTURE',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
