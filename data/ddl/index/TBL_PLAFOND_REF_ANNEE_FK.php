<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_REF_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_REFERENTIEL',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
