<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_ETAPE_HDFK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS_ETAPE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
