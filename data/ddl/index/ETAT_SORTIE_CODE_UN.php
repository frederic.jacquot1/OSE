<?php

//@formatter:off

return [
    'name'    => 'ETAT_SORTIE_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'ETAT_SORTIE',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
