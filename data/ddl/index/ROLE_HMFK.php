<?php

//@formatter:off

return [
    'name'    => 'ROLE_HMFK',
    'unique'  => FALSE,
    'table'   => 'ROLE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
