<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
