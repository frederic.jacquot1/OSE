<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
