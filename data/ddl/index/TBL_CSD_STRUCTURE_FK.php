<?php

//@formatter:off

return [
    'name'    => 'TBL_CSD_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS_SEUILS_DEF',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
