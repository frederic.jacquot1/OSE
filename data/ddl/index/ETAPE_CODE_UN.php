<?php

//@formatter:off

return [
    'name'    => 'ETAPE_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'ETAPE',
    'columns' => [
        'CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
