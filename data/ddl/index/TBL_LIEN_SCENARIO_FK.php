<?php

//@formatter:off

return [
    'name'    => 'TBL_LIEN_SCENARIO_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_LIEN',
    'columns' => [
        'SCENARIO_ID',
    ],
];

//@formatter:on
