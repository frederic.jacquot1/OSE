<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REFERENTIEL_HMFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_REFERENTIEL',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
