<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD_EFFEC_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_NOEUD_EFFECTIF',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
