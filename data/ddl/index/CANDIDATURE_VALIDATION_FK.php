<?php

//@formatter:off

return [
    'name'    => 'CANDIDATURE_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'CANDIDATURE',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
