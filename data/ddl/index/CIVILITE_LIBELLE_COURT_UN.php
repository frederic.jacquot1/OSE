<?php

//@formatter:off

return [
    'name'    => 'CIVILITE_LIBELLE_COURT_UN',
    'unique'  => TRUE,
    'table'   => 'CIVILITE',
    'columns' => [
        'LIBELLE_COURT',
    ],
];

//@formatter:on
