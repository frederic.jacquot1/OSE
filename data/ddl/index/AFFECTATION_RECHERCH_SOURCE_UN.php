<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_RECHERCH_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'AFFECTATION_RECHERCHE',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
