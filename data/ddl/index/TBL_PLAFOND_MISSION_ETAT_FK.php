<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_MISSION_ETAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_MISSION',
    'columns' => [
        'PLAFOND_ETAT_ID',
    ],
];

//@formatter:on
