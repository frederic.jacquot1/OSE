<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_STATUT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'STATUT_ID',
    ],
];

//@formatter:on
