<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_REF_TVH_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_REFERENTIEL',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
