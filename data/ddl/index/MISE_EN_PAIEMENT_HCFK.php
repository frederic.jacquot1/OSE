<?php

//@formatter:off

return [
    'name'    => 'MISE_EN_PAIEMENT_HCFK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
