<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
