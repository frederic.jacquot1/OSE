<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_SR_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
