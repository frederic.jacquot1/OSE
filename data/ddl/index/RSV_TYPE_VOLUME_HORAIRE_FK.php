<?php

//@formatter:off

return [
    'name'    => 'RSV_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'REGLE_STRUCTURE_VALIDATION',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
