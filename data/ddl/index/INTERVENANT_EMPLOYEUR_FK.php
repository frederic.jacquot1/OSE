<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_EMPLOYEUR_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'EMPLOYEUR_ID',
    ],
];

//@formatter:on
