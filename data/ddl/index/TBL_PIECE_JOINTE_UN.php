<?php

//@formatter:off

return [
    'name'    => 'TBL_PIECE_JOINTE_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PIECE_JOINTE',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'INTERVENANT_ID',
    ],
];

//@formatter:on
