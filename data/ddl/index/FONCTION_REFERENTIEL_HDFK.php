<?php

//@formatter:off

return [
    'name'    => 'FONCTION_REFERENTIEL_HDFK',
    'unique'  => FALSE,
    'table'   => 'FONCTION_REFERENTIEL',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
