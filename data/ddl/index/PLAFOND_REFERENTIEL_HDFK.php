<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REFERENTIEL_HDFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_REFERENTIEL',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
