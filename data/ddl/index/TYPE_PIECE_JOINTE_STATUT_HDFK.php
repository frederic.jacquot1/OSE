<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_STATUT_HDFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
