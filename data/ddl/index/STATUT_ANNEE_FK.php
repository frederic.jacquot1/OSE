<?php

//@formatter:off

return [
    'name'    => 'STATUT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'STATUT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
