<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_ELEMENT_PLAFOND_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_ELEMENT',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
