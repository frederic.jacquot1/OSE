<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_ETAPE_HMFK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS_ETAPE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
