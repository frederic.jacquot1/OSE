<?php

//@formatter:off

return [
    'name'    => 'LIEN_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'LIEN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
