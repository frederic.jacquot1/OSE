<?php

//@formatter:off

return [
    'name'    => 'ETR_ELEMENT_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
