<?php

//@formatter:off

return [
    'name'    => 'DOTATION_HDFK',
    'unique'  => FALSE,
    'table'   => 'DOTATION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
