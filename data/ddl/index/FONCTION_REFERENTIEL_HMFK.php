<?php

//@formatter:off

return [
    'name'    => 'FONCTION_REFERENTIEL_HMFK',
    'unique'  => FALSE,
    'table'   => 'FONCTION_REFERENTIEL',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
