<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
