<?php

//@formatter:off

return [
    'name'    => 'INT_DOSSIER_EMPLOYEUR_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT_DOSSIER',
    'columns' => [
        'EMPLOYEUR_ID',
    ],
];

//@formatter:on
