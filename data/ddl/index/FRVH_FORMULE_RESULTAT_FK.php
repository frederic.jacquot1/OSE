<?php

//@formatter:off

return [
    'name'    => 'FRVH_FORMULE_RESULTAT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_VH',
    'columns' => [
        'FORMULE_RESULTAT_ID',
    ],
];

//@formatter:on
