<?php

//@formatter:off

return [
    'name'    => 'TBCH_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
