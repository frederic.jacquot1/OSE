<?php

//@formatter:off

return [
    'name'    => 'TBL_LIEN_ACTIF',
    'unique'  => FALSE,
    'table'   => 'TBL_LIEN',
    'columns' => [
        'ACTIF',
    ],
];

//@formatter:on
