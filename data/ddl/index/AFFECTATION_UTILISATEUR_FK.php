<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_UTILISATEUR_FK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION',
    'columns' => [
        'UTILISATEUR_ID',
    ],
];

//@formatter:on
