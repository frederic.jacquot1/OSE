<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_CONTRAT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'CONTRAT_ID',
    ],
];

//@formatter:on
