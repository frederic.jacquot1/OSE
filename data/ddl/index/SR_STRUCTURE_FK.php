<?php

//@formatter:off

return [
    'name'    => 'SR_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
