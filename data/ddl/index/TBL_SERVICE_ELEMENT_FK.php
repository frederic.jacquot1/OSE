<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_ELEMENT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
