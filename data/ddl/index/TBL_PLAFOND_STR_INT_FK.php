<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_STR_INT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_STRUCTURE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
