<?php

//@formatter:off

return [
    'name'    => 'TBL_CSD_SCENARIO_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS_SEUILS_DEF',
    'columns' => [
        'SCENARIO_ID',
    ],
];

//@formatter:on
