<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCTURE_UN',
    'unique'  => TRUE,
    'table'   => 'PLAFOND_STRUCTURE',
    'columns' => [
        'STRUCTURE_ID',
        'PLAFOND_ID',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
