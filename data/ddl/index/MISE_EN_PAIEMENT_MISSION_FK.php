<?php

//@formatter:off

return [
    'name'    => 'MISE_EN_PAIEMENT_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'MISSION_ID',
    ],
];

//@formatter:on
