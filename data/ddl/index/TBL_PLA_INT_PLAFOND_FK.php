<?php

//@formatter:off

return [
    'name'    => 'TBL_PLA_INT_PLAFOND_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_INTERVENANT',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
