<?php

//@formatter:off

return [
    'name'    => 'CCTM_TYPE_MISSION_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_TYPE_MISSION',
    'columns' => [
        'TYPE_MISSION_ID',
    ],
];

//@formatter:on
