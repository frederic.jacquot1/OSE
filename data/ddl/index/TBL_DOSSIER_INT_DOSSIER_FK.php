<?php

//@formatter:off

return [
    'name'    => 'TBL_DOSSIER_INT_DOSSIER_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_DOSSIER',
    'columns' => [
        'DOSSIER_ID',
    ],
];

//@formatter:on
