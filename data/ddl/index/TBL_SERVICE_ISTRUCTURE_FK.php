<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_ISTRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'INTERVENANT_STRUCTURE_ID',
    ],
];

//@formatter:on
