<?php

//@formatter:off

return [
    'name'    => 'DISCIPLINE_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'DISCIPLINE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
