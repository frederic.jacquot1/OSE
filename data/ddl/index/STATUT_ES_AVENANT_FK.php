<?php

//@formatter:off

return [
    'name'    => 'STATUT_ES_AVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'STATUT',
    'columns' => [
        'AVENANT_ETAT_SORTIE_ID',
    ],
];

//@formatter:on
