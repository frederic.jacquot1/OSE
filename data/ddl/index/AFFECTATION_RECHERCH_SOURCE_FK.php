<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_RECHERCH_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION_RECHERCHE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
