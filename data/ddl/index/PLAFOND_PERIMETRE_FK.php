<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_PERIMETRE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND',
    'columns' => [
        'PLAFOND_PERIMETRE_ID',
    ],
];

//@formatter:on
