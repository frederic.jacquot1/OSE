<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_TYPE_CONTRAT_FK',
    'unique'  => FALSE,
    'table'   => 'CONTRAT',
    'columns' => [
        'TYPE_CONTRAT_ID',
    ],
];

//@formatter:on
