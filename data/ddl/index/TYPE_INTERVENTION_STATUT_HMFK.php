<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_STATUT_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
