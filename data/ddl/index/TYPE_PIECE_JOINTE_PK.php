<?php

//@formatter:off

return [
    'name'    => 'TYPE_PIECE_JOINTE_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_PIECE_JOINTE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
