<?php

//@formatter:off

return [
    'name'    => 'GRADE_CORPS_FK',
    'unique'  => FALSE,
    'table'   => 'GRADE',
    'columns' => [
        'CORPS_ID',
    ],
];

//@formatter:on
