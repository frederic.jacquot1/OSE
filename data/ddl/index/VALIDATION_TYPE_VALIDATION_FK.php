<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_TYPE_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION',
    'columns' => [
        'TYPE_VALIDATION_ID',
    ],
];

//@formatter:on
