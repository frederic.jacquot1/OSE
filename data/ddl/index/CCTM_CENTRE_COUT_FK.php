<?php

//@formatter:off

return [
    'name'    => 'CCTM_CENTRE_COUT_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_TYPE_MISSION',
    'columns' => [
        'CENTRE_COUT_ID',
    ],
];

//@formatter:on
