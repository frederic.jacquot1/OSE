<?php

//@formatter:off

return [
    'name'    => 'MISSION_ETUDIANT_PK',
    'unique'  => TRUE,
    'table'   => 'MISSION_ETUDIANT',
    'columns' => [
        'INTERVENANT_ID',
        'MISSION_ID',
    ],
];

//@formatter:on
