<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_UN',
    'unique'  => TRUE,
    'table'   => 'EFFECTIFS',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
