<?php

//@formatter:off

return [
    'name'    => 'TPJS_STATUT_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_PIECE_JOINTE_STATUT',
    'columns' => [
        'STATUT_ID',
    ],
];

//@formatter:on
