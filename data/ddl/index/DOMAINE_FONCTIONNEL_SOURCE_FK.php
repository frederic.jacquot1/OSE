<?php

//@formatter:off

return [
    'name'    => 'DOMAINE_FONCTIONNEL_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'DOMAINE_FONCTIONNEL',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
