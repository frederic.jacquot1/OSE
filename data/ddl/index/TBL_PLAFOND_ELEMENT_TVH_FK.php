<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_ELEMENT_TVH_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_ELEMENT',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
