<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_ETAPE_ETAPE_FK',
    'unique'  => FALSE,
    'table'   => 'EFFECTIFS_ETAPE',
    'columns' => [
        'ETAPE_ID',
    ],
];

//@formatter:on
