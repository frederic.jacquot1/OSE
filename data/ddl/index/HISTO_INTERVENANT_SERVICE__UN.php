<?php

//@formatter:off

return [
    'name'    => 'HISTO_INTERVENANT_SERVICE__UN',
    'unique'  => TRUE,
    'table'   => 'HISTO_INTERVENANT_SERVICE',
    'columns' => [
        'INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'REFERENTIEL',
    ],
];

//@formatter:on
