<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_MODULATEUR__UN',
    'unique'  => TRUE,
    'table'   => 'ELEMENT_MODULATEUR',
    'columns' => [
        'ELEMENT_ID',
        'MODULATEUR_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
