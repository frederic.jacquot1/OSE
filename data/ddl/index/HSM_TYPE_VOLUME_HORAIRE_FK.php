<?php

//@formatter:off

return [
    'name'    => 'HSM_TYPE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'HISTO_INTERVENANT_SERVICE',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
