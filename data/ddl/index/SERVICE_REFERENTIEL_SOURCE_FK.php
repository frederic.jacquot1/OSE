<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
