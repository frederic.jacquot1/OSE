<?php

//@formatter:off

return [
    'name'    => 'VVH_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION_VOL_HORAIRE',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
