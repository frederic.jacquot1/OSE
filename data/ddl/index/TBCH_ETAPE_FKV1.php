<?php

//@formatter:off

return [
    'name'    => 'TBCH_ETAPE_FKV1',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'ETAPE_ENS_ID',
    ],
];

//@formatter:on
