<?php

//@formatter:off

return [
    'name'    => 'TVE_SERVICE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'columns' => [
        'SERVICE_ID',
    ],
];

//@formatter:on
