<?php

//@formatter:off

return [
    'name'    => 'PAYS_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'PAYS',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
