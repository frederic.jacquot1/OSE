<?php

//@formatter:off

return [
    'name'    => 'NOEUD_ETAPE_FK',
    'unique'  => FALSE,
    'table'   => 'NOEUD',
    'columns' => [
        'ETAPE_ID',
    ],
];

//@formatter:on
