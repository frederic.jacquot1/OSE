<?php

//@formatter:off

return [
    'name'    => 'CATEGORIE_PRIVILEGE__UN',
    'unique'  => TRUE,
    'table'   => 'CATEGORIE_PRIVILEGE',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
