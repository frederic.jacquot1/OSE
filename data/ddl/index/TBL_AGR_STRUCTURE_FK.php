<?php

//@formatter:off

return [
    'name'    => 'TBL_AGR_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_AGREMENT',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
