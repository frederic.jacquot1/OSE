<?php

//@formatter:off

return [
    'name'    => 'CANDIDATURE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'CANDIDATURE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
