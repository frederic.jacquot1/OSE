<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_SIREN_IDX',
    'unique'  => FALSE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'SIREN',
    ],
];

//@formatter:on
