<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_HDFK',
    'unique'  => FALSE,
    'table'   => 'PIECE_JOINTE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
