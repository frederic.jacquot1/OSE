<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_HDFK',
    'unique'  => FALSE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
