<?php

//@formatter:off

return [
    'name'    => 'MISSION_TAUX_REMU_MAJORE_FK',
    'unique'  => FALSE,
    'table'   => 'MISSION',
    'columns' => [
        'TAUX_REMU_MAJORE_ID',
    ],
];

//@formatter:on
