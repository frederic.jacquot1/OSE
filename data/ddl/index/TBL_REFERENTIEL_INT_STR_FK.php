<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_INT_STR_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'INTERVENANT_STRUCTURE_ID',
    ],
];

//@formatter:on
