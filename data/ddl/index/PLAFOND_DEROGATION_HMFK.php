<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_DEROGATION_HMFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_DEROGATION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
