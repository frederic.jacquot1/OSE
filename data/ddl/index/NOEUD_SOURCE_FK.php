<?php

//@formatter:off

return [
    'name'    => 'NOEUD_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'NOEUD',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
