<?php

//@formatter:off

return [
    'name'    => 'INTERDEF_UN',
    'unique'  => TRUE,
    'table'   => 'INTERVENANT_PAR_DEFAUT',
    'columns' => [
        'ANNEE_ID',
        'INTERVENANT_CODE',
    ],
];

//@formatter:on
