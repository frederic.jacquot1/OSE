<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_HMFK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
