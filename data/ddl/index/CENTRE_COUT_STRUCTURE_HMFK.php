<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_STRUCTURE_HMFK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
