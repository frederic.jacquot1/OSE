<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_TYPE_MISSION_HMFK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_TYPE_MISSION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
