<?php

//@formatter:off

return [
    'name'    => 'TBL_PIECE_JOINTE_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_PIECE_JOINTE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
