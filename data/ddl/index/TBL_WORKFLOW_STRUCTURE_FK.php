<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
