<?php

//@formatter:off

return [
    'name'    => 'DEPARTEMENT_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'DEPARTEMENT',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
