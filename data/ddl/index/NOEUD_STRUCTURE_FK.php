<?php

//@formatter:off

return [
    'name'    => 'NOEUD_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'NOEUD',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
