<?php

//@formatter:off

return [
    'name'    => 'ETABLISSEMENT_HDFK',
    'unique'  => FALSE,
    'table'   => 'ETABLISSEMENT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
