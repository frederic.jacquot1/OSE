<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'SOURCE_CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
