<?php

//@formatter:off

return [
    'name'    => 'MISE_EN_PAIEMENT_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
