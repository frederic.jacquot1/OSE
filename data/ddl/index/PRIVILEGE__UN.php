<?php

//@formatter:off

return [
    'name'    => 'PRIVILEGE__UN',
    'unique'  => TRUE,
    'table'   => 'PRIVILEGE',
    'columns' => [
        'CATEGORIE_ID',
        'CODE',
    ],
];

//@formatter:on
