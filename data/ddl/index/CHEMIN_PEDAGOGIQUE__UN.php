<?php

//@formatter:off

return [
    'name'    => 'CHEMIN_PEDAGOGIQUE__UN',
    'unique'  => TRUE,
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'ETAPE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
