<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
