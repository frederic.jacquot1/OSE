<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'INTERVENANT_ID',
        'MISE_EN_PAIEMENT_ID',
        'FORMULE_RES_SERVICE_ID',
        'FORMULE_RES_SERVICE_REF_ID',
        'MISSION_ID',
        'TAUX_REMU_ID',
        'TAUX_HORAIRE',
    ],
];

//@formatter:on
