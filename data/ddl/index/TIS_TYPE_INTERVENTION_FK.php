<?php

//@formatter:off

return [
    'name'    => 'TIS_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
