<?php

//@formatter:off

return [
    'name'    => 'DISCIPLINE_HMFK',
    'unique'  => FALSE,
    'table'   => 'DISCIPLINE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
