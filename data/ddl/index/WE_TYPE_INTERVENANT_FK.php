<?php

//@formatter:off

return [
    'name'    => 'WE_TYPE_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'WF_ETAPE_DEP',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
