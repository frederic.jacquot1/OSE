<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
