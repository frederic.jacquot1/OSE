<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'EFFECTIFS',
    'columns' => [
        'SOURCE_CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
