<?php

//@formatter:off

return [
    'name'    => 'TYPE_CONTRAT_PK',
    'unique'  => TRUE,
    'table'   => 'TYPE_CONTRAT',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
