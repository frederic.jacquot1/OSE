<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_WORKFLOW',
    'columns' => [
        'INTERVENANT_ID',
        'ETAPE_ID',
        'STRUCTURE_ID',
    ],
];

//@formatter:on
