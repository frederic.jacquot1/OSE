<?php

//@formatter:off

return [
    'name'    => 'HISTO_SERVICE_MODIFICATION_PK',
    'unique'  => TRUE,
    'table'   => 'HISTO_INTERVENANT_SERVICE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
