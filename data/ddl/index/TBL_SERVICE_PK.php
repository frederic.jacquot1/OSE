<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_PK',
    'unique'  => TRUE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
