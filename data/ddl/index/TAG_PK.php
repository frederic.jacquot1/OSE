<?php

//@formatter:off

return [
    'name'    => 'TAG_PK',
    'unique'  => TRUE,
    'table'   => 'TAG',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
