<?php

//@formatter:off

return [
    'name'    => 'TAUX_REMU_HMFK',
    'unique'  => FALSE,
    'table'   => 'TAUX_REMU',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
