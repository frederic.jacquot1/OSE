<?php

//@formatter:off

return [
    'name'    => 'VHIT_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
