<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STATUT_HDFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STATUT',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
