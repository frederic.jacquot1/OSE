<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_FICHIER_PJFK',
    'unique'  => FALSE,
    'table'   => 'PIECE_JOINTE_FICHIER',
    'columns' => [
        'PIECE_JOINTE_ID',
    ],
];

//@formatter:on
