<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_PERIODE_EP_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_PERIODE_ID',
    ],
];

//@formatter:on
