<?php

//@formatter:off

return [
    'name'    => 'FRVH_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_VH',
    'columns' => [
        'VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
