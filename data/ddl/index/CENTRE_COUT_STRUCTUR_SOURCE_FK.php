<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_STRUCTUR_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
