<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_ENS_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_ENS',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
