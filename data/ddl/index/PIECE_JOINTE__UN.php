<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE__UN',
    'unique'  => TRUE,
    'table'   => 'PIECE_JOINTE',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'INTERVENANT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
