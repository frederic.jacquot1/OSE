<?php

//@formatter:off

return [
    'name'    => 'TVR_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
