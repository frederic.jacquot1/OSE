<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_MISS_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION_MISSION',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
