<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
