<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
