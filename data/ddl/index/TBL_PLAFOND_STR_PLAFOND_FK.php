<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_STR_PLAFOND_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PLAFOND_STRUCTURE',
    'columns' => [
        'PLAFOND_ID',
    ],
];

//@formatter:on
