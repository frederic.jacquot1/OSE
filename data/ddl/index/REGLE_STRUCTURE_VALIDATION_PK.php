<?php

//@formatter:off

return [
    'name'    => 'REGLE_STRUCTURE_VALIDATION_PK',
    'unique'  => TRUE,
    'table'   => 'REGLE_STRUCTURE_VALIDATION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
