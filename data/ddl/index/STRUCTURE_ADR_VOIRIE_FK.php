<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_ADR_VOIRIE_FK',
    'unique'  => FALSE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'ADRESSE_VOIRIE_ID',
    ],
];

//@formatter:on
