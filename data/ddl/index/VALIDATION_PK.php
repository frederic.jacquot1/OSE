<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_PK',
    'unique'  => TRUE,
    'table'   => 'VALIDATION',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
