<?php

//@formatter:off

return [
    'name'    => 'REGLE_STRUCTURE_VALIDATION__UN',
    'unique'  => TRUE,
    'table'   => 'REGLE_STRUCTURE_VALIDATION',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
