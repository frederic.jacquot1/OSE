<?php

//@formatter:off

return [
    'name'    => 'DEPARTEMENT_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'DEPARTEMENT',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
