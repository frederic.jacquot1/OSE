<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_SIRET_IDX',
    'unique'  => TRUE,
    'table'   => 'EMPLOYEUR',
    'columns' => [
        'SIRET',
    ],
];

//@formatter:on
