<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_ROLE_FK',
    'unique'  => FALSE,
    'table'   => 'AFFECTATION',
    'columns' => [
        'ROLE_ID',
    ],
];

//@formatter:on
