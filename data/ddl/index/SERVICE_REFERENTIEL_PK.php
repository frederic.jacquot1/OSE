<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_PK',
    'unique'  => TRUE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
