<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_STR_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PLAFOND_STRUCTURE',
    'columns' => [
        'ANNEE_ID',
        'STRUCTURE_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'INTERVENANT_ID',
        'PLAFOND_ID',
    ],
];

//@formatter:on
