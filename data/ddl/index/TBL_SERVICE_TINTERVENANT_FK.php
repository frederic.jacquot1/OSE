<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_TINTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
