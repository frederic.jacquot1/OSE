<?php

//@formatter:off

return [
    'name'    => 'TYPE_MISSION_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MISSION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
