<?php

//@formatter:off

return [
    'name'    => 'TBL_PJD_TYPE_PIECE_JOINTE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE_DEMANDE',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
    ],
];

//@formatter:on
