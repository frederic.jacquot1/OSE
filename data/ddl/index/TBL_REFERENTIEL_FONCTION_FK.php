<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_FONCTION_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_REFERENTIEL',
    'columns' => [
        'FONCTION_REFERENTIEL_ID',
    ],
];

//@formatter:on
