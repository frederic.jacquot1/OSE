<?php

//@formatter:off

return [
    'name'    => 'TBCH_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
