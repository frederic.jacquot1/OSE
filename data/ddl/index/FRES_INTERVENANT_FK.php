<?php

//@formatter:off

return [
    'name'    => 'FRES_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
