<?php

//@formatter:off

return [
    'name'    => 'AGREMENT_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'AGREMENT',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
