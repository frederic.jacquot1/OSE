<?php

//@formatter:off

return [
    'name'    => 'MSD_INTERVENANT_FK',
    'unique'  => FALSE,
    'table'   => 'MODIFICATION_SERVICE_DU',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
