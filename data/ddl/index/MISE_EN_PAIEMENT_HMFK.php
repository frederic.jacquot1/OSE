<?php

//@formatter:off

return [
    'name'    => 'MISE_EN_PAIEMENT_HMFK',
    'unique'  => FALSE,
    'table'   => 'MISE_EN_PAIEMENT',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
