<?php

//@formatter:off

return [
    'name'    => 'TVE_VOLUME_HORAIRE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'columns' => [
        'VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
