<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_STRUCTURE_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'columns' => [
        'TYPE_MODULATEUR_ID',
        'STRUCTURE_ID',
        'ANNEE_DEBUT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
