<?php

//@formatter:off

return [
    'name'    => 'VH_ENS_ELEMENT_PEDAGOGIQUE_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_ENS',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
