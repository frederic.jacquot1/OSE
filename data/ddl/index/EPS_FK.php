<?php

//@formatter:off

return [
    'name'    => 'EPS_FK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
