<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_MODULATEUR_EP',
    'columns' => [
        'TYPE_MODULATEUR_ID',
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
