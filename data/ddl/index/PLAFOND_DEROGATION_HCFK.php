<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_DEROGATION_HCFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_DEROGATION',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
