<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCTURE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_STRUCTURE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
