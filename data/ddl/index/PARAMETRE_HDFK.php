<?php

//@formatter:off

return [
    'name'    => 'PARAMETRE_HDFK',
    'unique'  => FALSE,
    'table'   => 'PARAMETRE',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
