<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_PK',
    'unique'  => TRUE,
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
