<?php

//@formatter:off

return [
    'name'    => 'TBL_PJF_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
