<?php

//@formatter:off

return [
    'name'    => 'TYPE_MISSION_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_MISSION',
    'columns' => [
        'CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
