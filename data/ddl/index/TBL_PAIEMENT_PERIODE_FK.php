<?php

//@formatter:off

return [
    'name'    => 'TBL_PAIEMENT_PERIODE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_PAIEMENT',
    'columns' => [
        'PERIODE_PAIEMENT_ID',
    ],
];

//@formatter:on
