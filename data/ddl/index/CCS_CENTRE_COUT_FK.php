<?php

//@formatter:off

return [
    'name'    => 'CCS_CENTRE_COUT_FK',
    'unique'  => FALSE,
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'columns' => [
        'CENTRE_COUT_ID',
    ],
];

//@formatter:on
