<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_MISSION_UN',
    'unique'  => TRUE,
    'table'   => 'PLAFOND_MISSION',
    'columns' => [
        'TYPE_MISSION_ID',
        'PLAFOND_ID',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
