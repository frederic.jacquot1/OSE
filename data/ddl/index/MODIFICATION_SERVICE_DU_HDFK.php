<?php

//@formatter:off

return [
    'name'    => 'MODIFICATION_SERVICE_DU_HDFK',
    'unique'  => FALSE,
    'table'   => 'MODIFICATION_SERVICE_DU',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
