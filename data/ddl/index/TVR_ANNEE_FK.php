<?php

//@formatter:off

return [
    'name'    => 'TVR_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
