<?php

//@formatter:off

return [
    'name'    => 'TBL_LIEN_SCENARIO_LIEN_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_LIEN',
    'columns' => [
        'SCENARIO_LIEN_ID',
    ],
];

//@formatter:on
