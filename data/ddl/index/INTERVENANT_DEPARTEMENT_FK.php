<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_DEPARTEMENT_FK',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'DEPARTEMENT_NAISSANCE_ID',
    ],
];

//@formatter:on
