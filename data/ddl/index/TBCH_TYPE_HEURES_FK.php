<?php

//@formatter:off

return [
    'name'    => 'TBCH_TYPE_HEURES_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_CHARGENS',
    'columns' => [
        'TYPE_HEURES_ID',
    ],
];

//@formatter:on
