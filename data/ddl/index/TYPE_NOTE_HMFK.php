<?php

//@formatter:off

return [
    'name'    => 'TYPE_NOTE_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_NOTE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
