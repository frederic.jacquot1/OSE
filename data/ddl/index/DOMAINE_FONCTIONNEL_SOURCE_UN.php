<?php

//@formatter:off

return [
    'name'    => 'DOMAINE_FONCTIONNEL_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'DOMAINE_FONCTIONNEL',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
