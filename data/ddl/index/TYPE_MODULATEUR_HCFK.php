<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_HCFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_MODULATEUR',
    'columns' => [
        'HISTO_CREATEUR_ID',
    ],
];

//@formatter:on
