<?php

//@formatter:off

return [
    'name'    => 'NOTIF_INDIC_AFFECTATION_FK',
    'unique'  => FALSE,
    'table'   => 'NOTIFICATION_INDICATEUR',
    'columns' => [
        'AFFECTATION_ID',
    ],
];

//@formatter:on
