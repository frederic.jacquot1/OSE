<?php

//@formatter:off

return [
    'name'    => 'FRR_FORMULE_RESULTAT_FK',
    'unique'  => FALSE,
    'table'   => 'FORMULE_RESULTAT_SERVICE_REF',
    'columns' => [
        'FORMULE_RESULTAT_ID',
    ],
];

//@formatter:on
