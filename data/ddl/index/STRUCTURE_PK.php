<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_PK',
    'unique'  => TRUE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
