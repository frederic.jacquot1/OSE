<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'CONTRAT',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
