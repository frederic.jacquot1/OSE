<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_HMFK',
    'unique'  => FALSE,
    'table'   => 'VALIDATION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
