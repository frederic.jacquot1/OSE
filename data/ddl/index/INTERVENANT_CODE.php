<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_CODE',
    'unique'  => FALSE,
    'table'   => 'INTERVENANT',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
