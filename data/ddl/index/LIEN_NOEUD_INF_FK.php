<?php

//@formatter:off

return [
    'name'    => 'LIEN_NOEUD_INF_FK',
    'unique'  => FALSE,
    'table'   => 'LIEN',
    'columns' => [
        'NOEUD_INF_ID',
    ],
];

//@formatter:on
