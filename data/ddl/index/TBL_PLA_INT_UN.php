<?php

//@formatter:off

return [
    'name'    => 'TBL_PLA_INT_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_PLAFOND_INTERVENANT',
    'columns' => [
        'PLAFOND_ID',
        'ANNEE_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'INTERVENANT_ID',
    ],
];

//@formatter:on
