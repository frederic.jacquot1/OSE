<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'SERVICE_REFERENTIEL',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
