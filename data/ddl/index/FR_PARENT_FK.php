<?php

//@formatter:off

return [
    'name'    => 'FR_PARENT_FK',
    'unique'  => FALSE,
    'table'   => 'FONCTION_REFERENTIEL',
    'columns' => [
        'PARENT_ID',
    ],
];

//@formatter:on
