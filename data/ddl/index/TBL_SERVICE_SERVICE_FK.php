<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_SERVICE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'SERVICE_ID',
    ],
];

//@formatter:on
