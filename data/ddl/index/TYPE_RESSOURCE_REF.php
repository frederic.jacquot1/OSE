<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_REF',
    'unique'  => FALSE,
    'table'   => 'TYPE_RESSOURCE',
    'columns' => [
        'REFERENTIEL',
    ],
];

//@formatter:on
