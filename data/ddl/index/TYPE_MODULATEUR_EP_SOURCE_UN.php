<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_SOURCE_UN',
    'unique'  => TRUE,
    'table'   => 'TYPE_MODULATEUR_EP',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
