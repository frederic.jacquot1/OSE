<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_MISSION',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
