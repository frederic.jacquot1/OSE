<?php

//@formatter:off

return [
    'name'    => 'VH_CHARGE_TYPE_INTERVENTION_FK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'columns' => [
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
