<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_ANNEE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'ANNEE_ID',
    ],
];

//@formatter:on
