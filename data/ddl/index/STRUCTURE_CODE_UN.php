<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'STRUCTURE',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
