<?php

//@formatter:off

return [
    'name'    => 'VOIRIE_CODE_UN',
    'unique'  => TRUE,
    'table'   => 'VOIRIE',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
