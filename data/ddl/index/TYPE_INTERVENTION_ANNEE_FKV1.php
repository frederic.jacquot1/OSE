<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_ANNEE_FKV1',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'ANNEE_FIN_ID',
    ],
];

//@formatter:on
