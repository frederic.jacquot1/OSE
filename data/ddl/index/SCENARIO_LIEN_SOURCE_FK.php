<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_LIEN_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'SCENARIO_LIEN',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
