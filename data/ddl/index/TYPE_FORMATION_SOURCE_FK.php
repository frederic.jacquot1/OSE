<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_SOURCE_FK',
    'unique'  => FALSE,
    'table'   => 'TYPE_FORMATION',
    'columns' => [
        'SOURCE_ID',
    ],
];

//@formatter:on
