<?php

//@formatter:off

return [
    'name'    => 'CHEMIN_PEDAGOGIQUE_HMFK',
    'unique'  => FALSE,
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
