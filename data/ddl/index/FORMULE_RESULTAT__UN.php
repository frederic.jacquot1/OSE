<?php

//@formatter:off

return [
    'name'    => 'FORMULE_RESULTAT__UN',
    'unique'  => TRUE,
    'table'   => 'FORMULE_RESULTAT',
    'columns' => [
        'INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'ETAT_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
