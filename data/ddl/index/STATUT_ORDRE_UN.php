<?php

//@formatter:off

return [
    'name'    => 'STATUT_ORDRE_UN',
    'unique'  => TRUE,
    'table'   => 'STATUT',
    'columns' => [
        'HISTO_DESTRUCTION',
        'ORDRE',
        'ANNEE_ID',
    ],
];

//@formatter:on
