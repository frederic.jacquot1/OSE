<?php

//@formatter:off

return [
    'name'    => 'GROUPE_TYPE_FORMATION_HDFK',
    'unique'  => FALSE,
    'table'   => 'GROUPE_TYPE_FORMATION',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
