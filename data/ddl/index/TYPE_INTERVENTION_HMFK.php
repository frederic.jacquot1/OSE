<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_HMFK',
    'unique'  => FALSE,
    'table'   => 'TYPE_INTERVENTION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
