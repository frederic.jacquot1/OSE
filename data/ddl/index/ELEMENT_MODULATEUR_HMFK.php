<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_MODULATEUR_HMFK',
    'unique'  => FALSE,
    'table'   => 'ELEMENT_MODULATEUR',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
