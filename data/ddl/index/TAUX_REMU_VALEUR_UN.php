<?php

//@formatter:off

return [
    'name'    => 'TAUX_REMU_VALEUR_UN',
    'unique'  => TRUE,
    'table'   => 'TAUX_REMU_VALEUR',
    'columns' => [
        'DATE_EFFET',
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
