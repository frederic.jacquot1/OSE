<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_STRUCTURE_FK',
    'unique'  => FALSE,
    'table'   => 'TBL_SERVICE',
    'columns' => [
        'STRUCTURE_ID',
    ],
];

//@formatter:on
