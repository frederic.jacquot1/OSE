<?php

//@formatter:off

return [
    'name'    => 'OFFRE_EMPLOI_VALIDATION_FK',
    'unique'  => FALSE,
    'table'   => 'OFFRE_EMPLOI',
    'columns' => [
        'VALIDATION_ID',
    ],
];

//@formatter:on
