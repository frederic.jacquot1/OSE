<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_MISSION_HMFK',
    'unique'  => FALSE,
    'table'   => 'PLAFOND_MISSION',
    'columns' => [
        'HISTO_MODIFICATEUR_ID',
    ],
];

//@formatter:on
