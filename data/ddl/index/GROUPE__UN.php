<?php

//@formatter:off

return [
    'name'    => 'GROUPE__UN',
    'unique'  => TRUE,
    'table'   => 'GROUPE',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'HISTO_DESTRUCTEUR_ID',
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
