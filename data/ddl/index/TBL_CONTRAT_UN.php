<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_UN',
    'unique'  => TRUE,
    'table'   => 'TBL_CONTRAT',
    'columns' => [
        'INTERVENANT_ID',
        'STRUCTURE_ID',
    ],
];

//@formatter:on
