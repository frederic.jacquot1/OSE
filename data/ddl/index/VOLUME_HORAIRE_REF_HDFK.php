<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_REF_HDFK',
    'unique'  => FALSE,
    'table'   => 'VOLUME_HORAIRE_REF',
    'columns' => [
        'HISTO_DESTRUCTEUR_ID',
    ],
];

//@formatter:on
