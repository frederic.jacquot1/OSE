CREATE OR REPLACE TRIGGER "F_ELEMENT_PEDAGOGIQUE"
  AFTER DELETE OR UPDATE OF ID, STRUCTURE_ID, PERIODE_ID, TAUX_FOAD, FI, FC, FA, HISTO_CREATION, HISTO_DESTRUCTION, TAUX_FA, TAUX_FC, TAUX_FI, ANNEE_ID ON "ELEMENT_PEDAGOGIQUE"
  REFERENCING FOR EACH ROW
  BEGIN

  IF NOT UNICAEN_TBL.ACTIV_TRIGGERS THEN RETURN; END IF;

  FOR p IN
    ( SELECT DISTINCT s.intervenant_id
    FROM service s
    WHERE (s.element_pedagogique_id = :NEW.id
    OR s.element_pedagogique_id     = :OLD.id)
    AND s.histo_destruction IS NULL
    ) LOOP UNICAEN_TBL.DEMANDE_CALCUL('formule', 'INTERVENANT_ID', p.intervenant_id );
END LOOP;
END;