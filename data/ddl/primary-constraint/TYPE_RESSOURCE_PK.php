<?php

//@formatter:off

return [
    'name'    => 'TYPE_RESSOURCE_PK',
    'table'   => 'TYPE_RESSOURCE',
    'index'   => 'TYPE_RESSOURCE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
