<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_PK',
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'index'   => 'ELEMENT_TAUX_REGIMES_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
