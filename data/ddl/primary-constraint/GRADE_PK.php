<?php

//@formatter:off

return [
    'name'    => 'GRADE_PK',
    'table'   => 'GRADE',
    'index'   => 'GRADE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
