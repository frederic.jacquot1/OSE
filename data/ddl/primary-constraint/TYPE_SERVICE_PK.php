<?php

//@formatter:off

return [
    'name'    => 'TYPE_SERVICE_PK',
    'table'   => 'TYPE_SERVICE',
    'index'   => 'TYPE_SERVICE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
