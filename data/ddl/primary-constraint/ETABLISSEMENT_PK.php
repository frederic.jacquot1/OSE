<?php

//@formatter:off

return [
    'name'    => 'ETABLISSEMENT_PK',
    'table'   => 'ETABLISSEMENT',
    'index'   => 'ETABLISSEMENT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
