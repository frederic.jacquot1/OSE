<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_PK',
    'table'   => 'STRUCTURE',
    'index'   => 'STRUCTURE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
