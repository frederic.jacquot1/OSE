<?php

//@formatter:off

return [
    'name'    => 'TYPE_AGREMENT_PK',
    'table'   => 'TYPE_AGREMENT',
    'index'   => 'TYPE_AGREMENT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
