<?php

//@formatter:off

return [
    'name'    => 'TBL_DMEP_LIQUIDATION_PK',
    'table'   => 'TBL_DMEP_LIQUIDATION',
    'index'   => 'TBL_DMEP_LIQUIDATION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
