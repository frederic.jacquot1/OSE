<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_VOL_HORAIRE_REF_PK',
    'table'   => 'VALIDATION_VOL_HORAIRE_REF',
    'index'   => 'VALIDATION_VOL_HORAIRE_REF_PK',
    'columns' => [
        'VALIDATION_ID',
        'VOLUME_HORAIRE_REF_ID',
    ],
];

//@formatter:on
