<?php

//@formatter:off

return [
    'name'    => 'TBL_VALIDATION_REFERENTIEL_PK',
    'table'   => 'TBL_VALIDATION_REFERENTIEL',
    'index'   => 'TBL_VALIDATION_REFERENTIEL_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
