<?php

//@formatter:off

return [
    'name'    => 'TBL_VALIDATION_ENSEIGNEMENT_PK',
    'table'   => 'TBL_VALIDATION_ENSEIGNEMENT',
    'index'   => 'TBL_VALIDATION_ENSEIGNEMENT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
