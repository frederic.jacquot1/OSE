<?php

//@formatter:off

return [
    'name'    => 'TYPE_VOLUME_HORAIRE_PK',
    'table'   => 'TYPE_VOLUME_HORAIRE',
    'index'   => 'TYPE_VOLUME_HORAIRE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
