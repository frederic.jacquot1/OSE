<?php

//@formatter:off

return [
    'name'    => 'PARAMETRE_PK',
    'table'   => 'PARAMETRE',
    'index'   => 'PARAMETRE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
