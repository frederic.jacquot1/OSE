<?php

//@formatter:off

return [
    'name'    => 'GROUPE_PK',
    'table'   => 'GROUPE',
    'index'   => 'GROUPE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
