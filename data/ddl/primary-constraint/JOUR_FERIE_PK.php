<?php

//@formatter:off

return [
    'name'    => 'JOUR_FERIE_PK',
    'table'   => 'JOUR_FERIE',
    'index'   => 'JOUR_FERIE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
