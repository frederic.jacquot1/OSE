<?php

//@formatter:off

return [
    'name'    => 'NOTIF_INDICATEUR_PK',
    'table'   => 'NOTIFICATION_INDICATEUR',
    'index'   => 'NOTIF_INDICATEUR_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
