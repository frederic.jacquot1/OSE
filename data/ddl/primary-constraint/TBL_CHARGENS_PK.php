<?php

//@formatter:off

return [
    'name'    => 'TBL_CHARGENS_PK',
    'table'   => 'TBL_CHARGENS',
    'index'   => 'TBL_CHARGENS_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
