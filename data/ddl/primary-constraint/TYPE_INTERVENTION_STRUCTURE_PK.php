<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_STRUCTURE_PK',
    'table'   => 'TYPE_INTERVENTION_STRUCTURE',
    'index'   => 'TYPE_INTERVENTION_STRUCTURE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
