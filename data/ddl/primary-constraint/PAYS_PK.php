<?php

//@formatter:off

return [
    'name'    => 'PAYS_PK',
    'table'   => 'PAYS',
    'index'   => 'PAYS_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
