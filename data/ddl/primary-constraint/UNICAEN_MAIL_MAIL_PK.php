<?php

//@formatter:off

return [
    'name'    => 'UNICAEN_MAIL_MAIL_PK',
    'table'   => 'UNICAEN_MAIL_MAIL',
    'index'   => 'UNICAEN_MAIL_MAIL_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
