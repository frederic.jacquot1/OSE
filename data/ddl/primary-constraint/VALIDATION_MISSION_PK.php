<?php

//@formatter:off

return [
    'name'    => 'VALIDATION_MISSION_PK',
    'table'   => 'VALIDATION_MISSION',
    'index'   => 'VALIDATION_MISSION_PK',
    'columns' => [
        'VALIDATION_ID',
        'MISSION_ID',
    ],
];

//@formatter:on
