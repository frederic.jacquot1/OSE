<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_PK',
    'table'   => 'EFFECTIFS',
    'index'   => 'EFFECTIFS_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
