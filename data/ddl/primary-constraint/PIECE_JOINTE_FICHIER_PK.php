<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE_FICHIER_PK',
    'table'   => 'PIECE_JOINTE_FICHIER',
    'index'   => 'PIECE_JOINTE_FICHIER_PK',
    'columns' => [
        'PIECE_JOINTE_ID',
        'FICHIER_ID',
    ],
];

//@formatter:on
