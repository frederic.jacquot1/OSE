<?php

//@formatter:off

return [
    'name'    => 'CATEGORIE_PRIVILEGE_PK',
    'table'   => 'CATEGORIE_PRIVILEGE',
    'index'   => 'CATEGORIE_PRIVILEGE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
