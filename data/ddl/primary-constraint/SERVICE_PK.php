<?php

//@formatter:off

return [
    'name'    => 'SERVICE_PK',
    'table'   => 'SERVICE',
    'index'   => 'SERVICE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
