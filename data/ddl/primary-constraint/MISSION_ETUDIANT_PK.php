<?php

//@formatter:off

return [
    'name'    => 'MISSION_ETUDIANT_PK',
    'table'   => 'MISSION_ETUDIANT',
    'index'   => 'MISSION_ETUDIANT_PK',
    'columns' => [
        'INTERVENANT_ID',
        'MISSION_ID',
    ],
];

//@formatter:on
