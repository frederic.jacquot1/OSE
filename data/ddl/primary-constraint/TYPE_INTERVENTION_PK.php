<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_PK',
    'table'   => 'TYPE_INTERVENTION',
    'index'   => 'TYPE_INTERVENTION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
