<?php

//@formatter:off

return [
    'name'    => 'NOTE_PK',
    'table'   => 'NOTE',
    'index'   => 'NOTE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
