<?php

//@formatter:off

return [
    'name'    => 'ETAT_VOLUME_HORAIRE_PK',
    'table'   => 'ETAT_VOLUME_HORAIRE',
    'index'   => 'ETAT_VOLUME_HORAIRE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
