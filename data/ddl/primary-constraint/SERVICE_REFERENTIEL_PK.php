<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_PK',
    'table'   => 'SERVICE_REFERENTIEL',
    'index'   => 'SERVICE_REFERENTIEL_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
