<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_PK',
    'table'   => 'TBL_WORKFLOW',
    'index'   => 'TBL_WORKFLOW_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
