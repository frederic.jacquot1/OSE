<?php

//@formatter:off

return [
    'name'    => 'TAG_PK',
    'table'   => 'TAG',
    'index'   => 'TAG_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
