<?php

//@formatter:off

return [
    'name'    => 'TBL_PK',
    'table'   => 'TBL',
    'index'   => 'TBL_PK',
    'columns' => [
        'TBL_NAME',
    ],
];

//@formatter:on
