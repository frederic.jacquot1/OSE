<?php

//@formatter:off

return [
    'name'    => 'INDIC_MODIF_DOSSIER_PK',
    'table'   => 'INDIC_MODIF_DOSSIER',
    'index'   => 'INDIC_MODIF_DOSSIER_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
