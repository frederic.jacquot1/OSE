<?php

//@formatter:off

return [
    'name'    => 'TYPE_MISSION_PK',
    'table'   => 'TYPE_MISSION',
    'index'   => 'TYPE_MISSION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
