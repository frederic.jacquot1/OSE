<?php

//@formatter:off

return [
    'name'    => 'GROUPE_TYPE_FORMATION_PK',
    'table'   => 'GROUPE_TYPE_FORMATION',
    'index'   => 'GROUPE_TYPE_FORMATION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
