<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_PK',
    'table'   => 'TYPE_FORMATION',
    'index'   => 'TYPE_FORMATION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
