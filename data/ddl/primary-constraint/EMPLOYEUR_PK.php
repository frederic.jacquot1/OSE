<?php

//@formatter:off

return [
    'name'    => 'EMPLOYEUR_PK',
    'table'   => 'EMPLOYEUR',
    'index'   => 'EMPLOYEUR_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
