<?php

//@formatter:off

return [
    'name'    => 'SYNC_LOG_PK',
    'table'   => 'SYNC_LOG',
    'index'   => 'SYNC_LOG_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
