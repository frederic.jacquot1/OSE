<?php

//@formatter:off

return [
    'name'    => 'ROLE_PRIVILEGE_PK',
    'table'   => 'ROLE_PRIVILEGE',
    'index'   => 'ROLE_PRIVILEGE_PK',
    'columns' => [
        'PRIVILEGE_ID',
        'ROLE_ID',
    ],
];

//@formatter:on
