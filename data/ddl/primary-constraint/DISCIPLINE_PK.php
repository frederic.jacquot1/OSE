<?php

//@formatter:off

return [
    'name'    => 'DISCIPLINE_PK',
    'table'   => 'DISCIPLINE',
    'index'   => 'DISCIPLINE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
