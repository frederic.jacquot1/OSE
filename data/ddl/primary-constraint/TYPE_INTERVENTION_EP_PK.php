<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_EP_PK',
    'table'   => 'TYPE_INTERVENTION_EP',
    'index'   => 'TYPE_INTERVENTION_EP_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
