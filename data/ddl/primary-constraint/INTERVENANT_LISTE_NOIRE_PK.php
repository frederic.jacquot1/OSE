<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_LISTE_NOIRE_PK',
    'table'   => 'LISTE_NOIRE',
    'index'   => 'INTERVENANT_LISTE_NOIRE_PK',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
