<?php

//@formatter:off

return [
    'name'    => 'PRIVILEGE_PK',
    'table'   => 'PRIVILEGE',
    'index'   => 'PRIVILEGE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
