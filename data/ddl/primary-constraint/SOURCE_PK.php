<?php

//@formatter:off

return [
    'name'    => 'SOURCE_PK',
    'table'   => 'SOURCE',
    'index'   => 'SOURCE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
