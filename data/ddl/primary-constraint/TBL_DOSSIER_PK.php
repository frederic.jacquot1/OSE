<?php

//@formatter:off

return [
    'name'    => 'TBL_DOSSIER_PK',
    'table'   => 'TBL_DOSSIER',
    'index'   => 'TBL_DOSSIER_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
