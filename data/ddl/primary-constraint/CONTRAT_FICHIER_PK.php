<?php

//@formatter:off

return [
    'name'    => 'CONTRAT_FICHIER_PK',
    'table'   => 'CONTRAT_FICHIER',
    'index'   => 'CONTRAT_FICHIER_PK',
    'columns' => [
        'CONTRAT_ID',
        'FICHIER_ID',
    ],
];

//@formatter:on
