<?php

//@formatter:off

return [
    'name'    => 'TYPE_CONTRAT_PK',
    'table'   => 'TYPE_CONTRAT',
    'index'   => 'TYPE_CONTRAT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
