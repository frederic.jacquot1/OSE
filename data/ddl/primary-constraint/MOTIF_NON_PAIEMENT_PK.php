<?php

//@formatter:off

return [
    'name'    => 'MOTIF_NON_PAIEMENT_PK',
    'table'   => 'MOTIF_NON_PAIEMENT',
    'index'   => 'MOTIF_NON_PAIEMENT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
