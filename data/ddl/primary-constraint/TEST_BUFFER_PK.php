<?php

//@formatter:off

return [
    'name'    => 'TEST_BUFFER_PK',
    'table'   => 'TEST_BUFFER',
    'index'   => 'TEST_BUFFER_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
