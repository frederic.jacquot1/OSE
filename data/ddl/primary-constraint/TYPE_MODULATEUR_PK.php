<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_PK',
    'table'   => 'TYPE_MODULATEUR',
    'index'   => 'TYPE_MODULATEUR_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
