<?php

//@formatter:off

return [
    'name'    => 'TYPE_NOTE_PK',
    'table'   => 'TYPE_NOTE',
    'index'   => 'TYPE_NOTE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
