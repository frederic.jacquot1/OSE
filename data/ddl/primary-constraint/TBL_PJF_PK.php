<?php

//@formatter:off

return [
    'name'    => 'TBL_PJF_PK',
    'table'   => 'TBL_PIECE_JOINTE_FOURNIE',
    'index'   => 'TBL_PJF_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
