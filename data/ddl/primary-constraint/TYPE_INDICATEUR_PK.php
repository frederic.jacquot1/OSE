<?php

//@formatter:off

return [
    'name'    => 'TYPE_INDICATEUR_PK',
    'table'   => 'TYPE_INDICATEUR',
    'index'   => 'TYPE_INDICATEUR_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
