<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_STRUCTURE_PK',
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'index'   => 'TYPE_MODULATEUR_STRUCTURE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
