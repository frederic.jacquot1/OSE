<?php

//@formatter:off

return [
    'name'    => 'DOSSIER_CHAMP_AUTRE_TYPE_PK',
    'table'   => 'DOSSIER_CHAMP_AUTRE_TYPE',
    'index'   => 'DOSSIER_CHAMP_AUTRE_TYPE_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
