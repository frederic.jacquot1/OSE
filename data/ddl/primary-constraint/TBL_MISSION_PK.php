<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_PK',
    'table'   => 'TBL_MISSION',
    'index'   => 'TBL_MISSION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
