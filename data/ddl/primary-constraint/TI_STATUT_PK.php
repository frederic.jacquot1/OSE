<?php

//@formatter:off

return [
    'name'    => 'TI_STATUT_PK',
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'index'   => 'TI_STATUT_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
