<?php

//@formatter:off

return [
    'name'    => 'REGLE_STRUCTURE_VALIDATION_PK',
    'table'   => 'REGLE_STRUCTURE_VALIDATION',
    'index'   => 'REGLE_STRUCTURE_VALIDATION_PK',
    'columns' => [
        'ID',
    ],
];

//@formatter:on
