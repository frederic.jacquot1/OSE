<?php

//@formatter:off

return [
    'name'    => 'WF_ETAPE_CODE_UN',
    'table'   => 'WF_ETAPE',
    'index'   => 'WF_ETAPE_CODE_UN',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
