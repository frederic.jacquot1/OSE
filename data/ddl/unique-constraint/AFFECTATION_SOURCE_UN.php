<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_SOURCE_UN',
    'table'   => 'AFFECTATION',
    'index'   => 'AFFECTATION_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
