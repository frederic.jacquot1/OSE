<?php

//@formatter:off

return [
    'name'    => 'CATEGORIE_PRIVILEGE__UN',
    'table'   => 'CATEGORIE_PRIVILEGE',
    'index'   => 'CATEGORIE_PRIVILEGE__UN',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
