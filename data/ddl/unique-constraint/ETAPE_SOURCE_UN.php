<?php

//@formatter:off

return [
    'name'    => 'ETAPE_SOURCE_UN',
    'table'   => 'ETAPE',
    'index'   => 'ETAPE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
        'ANNEE_ID',
    ],
];

//@formatter:on
