<?php

//@formatter:off

return [
    'name'    => 'AGREMENT__UN',
    'table'   => 'AGREMENT',
    'index'   => 'AGREMENT__UN',
    'columns' => [
        'TYPE_AGREMENT_ID',
        'INTERVENANT_ID',
        'STRUCTURE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
