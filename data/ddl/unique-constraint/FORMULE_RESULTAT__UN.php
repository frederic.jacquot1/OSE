<?php

//@formatter:off

return [
    'name'    => 'FORMULE_RESULTAT__UN',
    'table'   => 'FORMULE_RESULTAT',
    'index'   => 'FORMULE_RESULTAT__UN',
    'columns' => [
        'INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'ETAT_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
