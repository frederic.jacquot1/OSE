<?php

//@formatter:off

return [
    'name'    => 'INTERVENANT_SOURCE_UN',
    'table'   => 'INTERVENANT',
    'index'   => 'INTERVENANT_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
        'STATUT_ID',
    ],
];

//@formatter:on
