<?php

//@formatter:off

return [
    'name'    => 'NOTIFICATION_INDICATEUR_UN',
    'table'   => 'NOTIFICATION_INDICATEUR',
    'index'   => 'NOTIFICATION_INDICATEUR_UN',
    'columns' => [
        'INDICATEUR_ID',
        'AFFECTATION_ID',
    ],
];

//@formatter:on
