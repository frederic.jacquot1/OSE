<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION_RECHERCH_SOURCE_UN',
    'table'   => 'AFFECTATION_RECHERCHE',
    'index'   => 'AFFECTATION_RECHERCH_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
