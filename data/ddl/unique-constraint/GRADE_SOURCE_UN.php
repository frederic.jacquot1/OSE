<?php

//@formatter:off

return [
    'name'    => 'GRADE_SOURCE_UN',
    'table'   => 'GRADE',
    'index'   => 'GRADE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
