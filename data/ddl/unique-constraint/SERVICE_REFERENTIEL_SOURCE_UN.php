<?php

//@formatter:off

return [
    'name'    => 'SERVICE_REFERENTIEL_SOURCE_UN',
    'table'   => 'SERVICE_REFERENTIEL',
    'index'   => 'SERVICE_REFERENTIEL_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
