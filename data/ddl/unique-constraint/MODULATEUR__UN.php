<?php

//@formatter:off

return [
    'name'    => 'MODULATEUR__UN',
    'table'   => 'MODULATEUR',
    'index'   => 'MODULATEUR__UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
