<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_EP_SOURCE_UN',
    'table'   => 'CENTRE_COUT_EP',
    'index'   => 'CENTRE_COUT_EP_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
