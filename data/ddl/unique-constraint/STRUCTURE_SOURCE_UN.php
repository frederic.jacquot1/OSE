<?php

//@formatter:off

return [
    'name'    => 'STRUCTURE_SOURCE_UN',
    'table'   => 'STRUCTURE',
    'index'   => 'STRUCTURE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
