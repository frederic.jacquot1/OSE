<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_LIEN_UN',
    'table'   => 'SCENARIO_LIEN',
    'index'   => 'SCENARIO_LIEN_UN',
    'columns' => [
        'SCENARIO_ID',
        'LIEN_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
