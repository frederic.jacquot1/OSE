<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_NOEUD_SOURCE_UN',
    'table'   => 'SCENARIO_NOEUD',
    'index'   => 'SCENARIO_NOEUD_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
