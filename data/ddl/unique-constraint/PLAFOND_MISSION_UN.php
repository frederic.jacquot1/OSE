<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_MISSION_UN',
    'table'   => 'PLAFOND_MISSION',
    'index'   => 'PLAFOND_MISSION_UN',
    'columns' => [
        'TYPE_MISSION_ID',
        'PLAFOND_ID',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
