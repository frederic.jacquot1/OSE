<?php

//@formatter:off

return [
    'name'    => 'TAUX_REMU_VALEUR_UN',
    'table'   => 'TAUX_REMU_VALEUR',
    'index'   => 'TAUX_REMU_VALEUR_UN',
    'columns' => [
        'DATE_EFFET',
        'TAUX_REMU_ID',
    ],
];

//@formatter:on
