<?php

//@formatter:off

return [
    'name'    => 'WF_ETAPE_DEP_UN',
    'table'   => 'WF_ETAPE_DEP',
    'index'   => 'WF_ETAPE_DEP_UN',
    'columns' => [
        'ETAPE_SUIV_ID',
        'ETAPE_PREC_ID',
    ],
];

//@formatter:on
