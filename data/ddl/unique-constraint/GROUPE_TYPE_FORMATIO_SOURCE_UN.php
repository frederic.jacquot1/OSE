<?php

//@formatter:off

return [
    'name'    => 'GROUPE_TYPE_FORMATIO_SOURCE_UN',
    'table'   => 'GROUPE_TYPE_FORMATION',
    'index'   => 'GROUPE_TYPE_FORMATIO_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
