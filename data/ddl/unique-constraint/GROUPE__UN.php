<?php

//@formatter:off

return [
    'name'    => 'GROUPE__UN',
    'table'   => 'GROUPE',
    'index'   => 'GROUPE__UN',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'HISTO_DESTRUCTEUR_ID',
        'TYPE_INTERVENTION_ID',
    ],
];

//@formatter:on
