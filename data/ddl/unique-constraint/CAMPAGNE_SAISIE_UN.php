<?php

//@formatter:off

return [
    'name'    => 'CAMPAGNE_SAISIE_UN',
    'table'   => 'CAMPAGNE_SAISIE',
    'index'   => 'CAMPAGNE_SAISIE_UN',
    'columns' => [
        'ANNEE_ID',
        'TYPE_INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
