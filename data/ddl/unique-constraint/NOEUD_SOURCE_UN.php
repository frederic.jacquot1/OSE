<?php

//@formatter:off

return [
    'name'    => 'NOEUD_SOURCE_UN',
    'table'   => 'NOEUD',
    'index'   => 'NOEUD_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
