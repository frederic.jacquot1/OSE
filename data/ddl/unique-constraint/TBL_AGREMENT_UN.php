<?php

//@formatter:off

return [
    'name'    => 'TBL_AGREMENT_UN',
    'table'   => 'TBL_AGREMENT',
    'index'   => 'TBL_AGREMENT_UN',
    'columns' => [
        'TYPE_AGREMENT_ID',
        'INTERVENANT_ID',
        'STRUCTURE_ID',
        'ANNEE_AGREMENT',
    ],
];

//@formatter:on
