<?php

//@formatter:off

return [
    'name'    => 'TBL_WORKFLOW_UN',
    'table'   => 'TBL_WORKFLOW',
    'index'   => 'TBL_WORKFLOW_UN',
    'columns' => [
        'INTERVENANT_ID',
        'ETAPE_ID',
        'STRUCTURE_ID',
    ],
];

//@formatter:on
