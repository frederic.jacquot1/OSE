<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_VH_UN',
    'table'   => 'TBL_PLAFOND_VOLUME_HORAIRE',
    'index'   => 'TBL_PLAFOND_VH_UN',
    'columns' => [
        'ANNEE_ID',
        'ELEMENT_PEDAGOGIQUE_ID',
        'TYPE_INTERVENTION_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'INTERVENANT_ID',
        'PLAFOND_ID',
    ],
];

//@formatter:on
