<?php

//@formatter:off

return [
    'name'    => 'PAYS_SOURCE_UN',
    'table'   => 'PAYS',
    'index'   => 'PAYS_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
