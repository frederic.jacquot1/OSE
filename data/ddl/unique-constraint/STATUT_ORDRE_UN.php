<?php

//@formatter:off

return [
    'name'    => 'STATUT_ORDRE_UN',
    'table'   => 'STATUT',
    'index'   => 'STATUT_ORDRE_UN',
    'columns' => [
        'HISTO_DESTRUCTION',
        'ORDRE',
        'ANNEE_ID',
    ],
];

//@formatter:on
