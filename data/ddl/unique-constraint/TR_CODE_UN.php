<?php

//@formatter:off

return [
    'name'    => 'TR_CODE_UN',
    'table'   => 'TAUX_REMU',
    'index'   => 'TR_CODE_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
