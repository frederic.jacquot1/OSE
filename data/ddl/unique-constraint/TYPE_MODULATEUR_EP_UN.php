<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_UN',
    'table'   => 'TYPE_MODULATEUR_EP',
    'index'   => 'TYPE_MODULATEUR_EP_UN',
    'columns' => [
        'TYPE_MODULATEUR_ID',
        'ELEMENT_PEDAGOGIQUE_ID',
    ],
];

//@formatter:on
