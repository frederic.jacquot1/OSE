<?php

//@formatter:off

return [
    'name'    => 'TBL_MISSION_UN',
    'table'   => 'TBL_MISSION',
    'index'   => 'TBL_MISSION_UN',
    'columns' => [
        'INTERVENANT_ID',
        'MISSION_ID',
    ],
];

//@formatter:on
