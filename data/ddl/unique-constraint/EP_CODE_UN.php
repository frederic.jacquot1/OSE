<?php

//@formatter:off

return [
    'name'    => 'EP_CODE_UN',
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'index'   => 'EP_CODE_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
        'ANNEE_ID',
    ],
];

//@formatter:on
