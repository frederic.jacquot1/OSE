<?php

//@formatter:off

return [
    'name'    => 'SERVICE_SOURCE_UN',
    'table'   => 'SERVICE',
    'index'   => 'SERVICE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
