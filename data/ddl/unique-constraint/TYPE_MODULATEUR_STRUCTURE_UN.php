<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_STRUCTURE_UN',
    'table'   => 'TYPE_MODULATEUR_STRUCTURE',
    'index'   => 'TYPE_MODULATEUR_STRUCTURE_UN',
    'columns' => [
        'TYPE_MODULATEUR_ID',
        'STRUCTURE_ID',
        'ANNEE_DEBUT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
