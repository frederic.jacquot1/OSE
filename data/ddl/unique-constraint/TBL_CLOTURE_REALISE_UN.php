<?php

//@formatter:off

return [
    'name'    => 'TBL_CLOTURE_REALISE_UN',
    'table'   => 'TBL_CLOTURE_REALISE',
    'index'   => 'TBL_CLO_REAL_INTERVENANT_FK',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
