<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_CODE_UN',
    'table'   => 'TYPE_INTERVENTION',
    'index'   => 'TYPE_INTERVENTION_CODE_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
