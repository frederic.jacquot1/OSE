<?php

//@formatter:off

return [
    'name'    => 'PERIODE__UN',
    'table'   => 'PERIODE',
    'index'   => 'PERIODE__UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
