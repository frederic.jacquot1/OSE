<?php

//@formatter:off

return [
    'name'    => 'TYPE_FORMATION_SOURCE_UN',
    'table'   => 'TYPE_FORMATION',
    'index'   => 'TYPE_FORMATION_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
