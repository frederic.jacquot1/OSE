<?php

//@formatter:off

return [
    'name'    => 'SCENARIO_LIEN_SOURCE_UN',
    'table'   => 'SCENARIO_LIEN',
    'index'   => 'SCENARIO_LIEN_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
