<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STATUT_UN',
    'table'   => 'PLAFOND_STATUT',
    'index'   => 'PLAFOND_STATUT_UN',
    'columns' => [
        'STATUT_ID',
        'PLAFOND_ID',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
