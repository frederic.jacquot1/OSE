<?php

//@formatter:off

return [
    'name'    => 'VOLUME_HORAIRE_CHARG_SOURCE_UN',
    'table'   => 'VOLUME_HORAIRE_CHARGE',
    'index'   => 'VOLUME_HORAIRE_CHARG_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
