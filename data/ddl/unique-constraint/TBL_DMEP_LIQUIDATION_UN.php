<?php

//@formatter:off

return [
    'name'    => 'TBL_DMEP_LIQUIDATION_UN',
    'table'   => 'TBL_DMEP_LIQUIDATION',
    'index'   => 'TBL_DMEP_LIQUIDATION_UN',
    'columns' => [
        'ANNEE_ID',
        'TYPE_RESSOURCE_ID',
        'STRUCTURE_ID',
    ],
];

//@formatter:on
