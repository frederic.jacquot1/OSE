<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_STRUCTUR_SOURCE_UN',
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'index'   => 'CENTRE_COUT_STRUCTUR_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
