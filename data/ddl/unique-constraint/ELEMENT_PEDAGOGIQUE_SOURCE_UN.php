<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_PEDAGOGIQUE_SOURCE_UN',
    'table'   => 'ELEMENT_PEDAGOGIQUE',
    'index'   => 'ELEMENT_PEDAGOGIQUE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
