<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_SOURCE_UN',
    'table'   => 'EFFECTIFS',
    'index'   => 'EFFECTIFS_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
