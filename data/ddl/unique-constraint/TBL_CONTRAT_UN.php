<?php

//@formatter:off

return [
    'name'    => 'TBL_CONTRAT_UN',
    'table'   => 'TBL_CONTRAT',
    'index'   => 'TBL_CONTRAT_UN',
    'columns' => [
        'INTERVENANT_ID',
        'STRUCTURE_ID',
    ],
];

//@formatter:on
