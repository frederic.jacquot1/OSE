<?php

//@formatter:off

return [
    'name'    => 'TYPE_INTERVENTION_EP_SOURCE_UN',
    'table'   => 'TYPE_INTERVENTION_EP',
    'index'   => 'TYPE_INTERVENTION_EP_SOURCE_UN',
    'columns' => [
        'HISTO_DESTRUCTION',
        'SOURCE_CODE',
    ],
];

//@formatter:on
