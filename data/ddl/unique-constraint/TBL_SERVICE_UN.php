<?php

//@formatter:off

return [
    'name'    => 'TBL_SERVICE_UN',
    'table'   => 'TBL_SERVICE',
    'index'   => 'TBL_SERVICE_UN',
    'columns' => [
        'SERVICE_ID',
        'TYPE_VOLUME_HORAIRE_ID',
    ],
];

//@formatter:on
