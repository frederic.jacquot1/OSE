<?php

//@formatter:off

return [
    'name'    => 'PERIMETRE_LIBELLE_UN',
    'table'   => 'PERIMETRE',
    'index'   => 'PERIMETRE_LIBELLE_UN',
    'columns' => [
        'LIBELLE',
    ],
];

//@formatter:on
