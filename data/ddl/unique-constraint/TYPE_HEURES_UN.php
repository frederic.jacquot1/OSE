<?php

//@formatter:off

return [
    'name'    => 'TYPE_HEURES_UN',
    'table'   => 'TYPE_HEURES',
    'index'   => 'TYPE_HEURES_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
