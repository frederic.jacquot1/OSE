<?php

//@formatter:off

return [
    'name'    => 'SERVICE__UN',
    'table'   => 'SERVICE',
    'index'   => 'SERVICE__UN',
    'columns' => [
        'INTERVENANT_ID',
        'ELEMENT_PEDAGOGIQUE_ID',
        'ETABLISSEMENT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
