<?php

//@formatter:off

return [
    'name'    => 'FONCTION_REFERENTIEL_CODE_UN',
    'table'   => 'FONCTION_REFERENTIEL',
    'index'   => 'FONCTION_REFERENTIEL_CODE_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
