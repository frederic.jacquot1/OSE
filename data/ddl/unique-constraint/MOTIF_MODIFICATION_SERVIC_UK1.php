<?php

//@formatter:off

return [
    'name'    => 'MOTIF_MODIFICATION_SERVIC_UK1',
    'table'   => 'MOTIF_MODIFICATION_SERVICE',
    'index'   => 'MOTIF_MODIFICATION_SERVIC_UK1',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
