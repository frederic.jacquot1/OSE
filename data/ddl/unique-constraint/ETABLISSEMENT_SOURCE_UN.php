<?php

//@formatter:off

return [
    'name'    => 'ETABLISSEMENT_SOURCE_UN',
    'table'   => 'ETABLISSEMENT',
    'index'   => 'ETABLISSEMENT_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
