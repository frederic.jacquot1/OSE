<?php

//@formatter:off

return [
    'name'    => 'PIECE_JOINTE__UN',
    'table'   => 'PIECE_JOINTE',
    'index'   => 'PIECE_JOINTE__UN',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'INTERVENANT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
