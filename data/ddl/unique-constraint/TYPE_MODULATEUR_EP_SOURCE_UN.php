<?php

//@formatter:off

return [
    'name'    => 'TYPE_MODULATEUR_EP_SOURCE_UN',
    'table'   => 'TYPE_MODULATEUR_EP',
    'index'   => 'TYPE_MODULATEUR_EP_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
