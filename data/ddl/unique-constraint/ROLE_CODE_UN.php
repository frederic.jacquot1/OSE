<?php

//@formatter:off

return [
    'name'    => 'ROLE_CODE_UN',
    'table'   => 'ROLE',
    'index'   => 'ROLE_CODE_UN',
    'columns' => [
        'CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
