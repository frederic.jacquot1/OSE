<?php

//@formatter:off

return [
    'name'    => 'TR_LIB_UN',
    'table'   => 'TAUX_REMU',
    'index'   => 'TR_LIB_UN',
    'columns' => [
        'LIBELLE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
