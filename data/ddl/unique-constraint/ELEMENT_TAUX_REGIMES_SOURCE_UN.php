<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_TAUX_REGIMES_SOURCE_UN',
    'table'   => 'ELEMENT_TAUX_REGIMES',
    'index'   => 'ELEMENT_TAUX_REGIMES_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
