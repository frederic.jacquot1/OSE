<?php

//@formatter:off

return [
    'name'    => 'TBL_PIECE_JOINTE_UN',
    'table'   => 'TBL_PIECE_JOINTE',
    'index'   => 'TBL_PIECE_JOINTE_UN',
    'columns' => [
        'TYPE_PIECE_JOINTE_ID',
        'INTERVENANT_ID',
    ],
];

//@formatter:on
