<?php

//@formatter:off

return [
    'name'    => 'ETAPE_CODE_UN',
    'table'   => 'ETAPE',
    'index'   => 'ETAPE_CODE_UN',
    'columns' => [
        'CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
