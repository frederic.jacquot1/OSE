<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_MISSION_UN',
    'table'   => 'TBL_PLAFOND_MISSION',
    'index'   => 'TBL_PLAFOND_MISSION_UN',
    'columns' => [
        'TYPE_MISSION_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'INTERVENANT_ID',
        'ANNEE_ID',
        'PLAFOND_ID',
    ],
];

//@formatter:on
