<?php

//@formatter:off

return [
    'name'    => 'TBL_REFERENTIEL_UN',
    'table'   => 'TBL_REFERENTIEL',
    'index'   => 'TBL_REFERENTIEL_UN',
    'columns' => [
        'INTERVENANT_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'STRUCTURE_ID',
        'SERVICE_REFERENTIEL_ID',
    ],
];

//@formatter:on
