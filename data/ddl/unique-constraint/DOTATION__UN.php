<?php

//@formatter:off

return [
    'name'    => 'DOTATION__UN',
    'table'   => 'DOTATION',
    'index'   => 'DOTATION__UN',
    'columns' => [
        'TYPE_RESSOURCE_ID',
        'ANNEE_ID',
        'ANNEE_CIVILE',
        'STRUCTURE_ID',
        'LIBELLE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
