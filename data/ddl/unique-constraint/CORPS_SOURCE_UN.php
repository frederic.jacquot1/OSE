<?php

//@formatter:off

return [
    'name'    => 'CORPS_SOURCE_UN',
    'table'   => 'CORPS',
    'index'   => 'CORPS_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
