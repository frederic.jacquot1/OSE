<?php

//@formatter:off

return [
    'name'    => 'DEPARTEMENT_SOURCE_UN',
    'table'   => 'DEPARTEMENT',
    'index'   => 'DEPARTEMENT_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
