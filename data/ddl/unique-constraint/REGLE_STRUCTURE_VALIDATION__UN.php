<?php

//@formatter:off

return [
    'name'    => 'REGLE_STRUCTURE_VALIDATION__UN',
    'table'   => 'REGLE_STRUCTURE_VALIDATION',
    'index'   => 'REGLE_STRUCTURE_VALIDATION__UN',
    'columns' => [
        'TYPE_VOLUME_HORAIRE_ID',
        'TYPE_INTERVENANT_ID',
    ],
];

//@formatter:on
