<?php

//@formatter:off

return [
    'name'    => 'CHEMIN_PEDAGOGIQUE_SOURCE_UN',
    'table'   => 'CHEMIN_PEDAGOGIQUE',
    'index'   => 'CHEMIN_PEDAGOGIQUE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
