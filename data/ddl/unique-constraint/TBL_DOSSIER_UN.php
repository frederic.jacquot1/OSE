<?php

//@formatter:off

return [
    'name'    => 'TBL_DOSSIER_UN',
    'table'   => 'TBL_DOSSIER',
    'index'   => 'TBL_DOSSIER_INTERVENANT_FK',
    'columns' => [
        'INTERVENANT_ID',
    ],
];

//@formatter:on
