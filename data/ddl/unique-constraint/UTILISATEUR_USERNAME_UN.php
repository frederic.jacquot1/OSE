<?php

//@formatter:off

return [
    'name'    => 'UTILISATEUR_USERNAME_UN',
    'table'   => 'UTILISATEUR',
    'index'   => 'UTILISATEUR_USERNAME_UN',
    'columns' => [
        'USERNAME',
    ],
];

//@formatter:on
