<?php

//@formatter:off

return [
    'name'    => 'INTERDEF_UN',
    'table'   => 'INTERVENANT_PAR_DEFAUT',
    'index'   => 'INTERDEF_UN',
    'columns' => [
        'ANNEE_ID',
        'INTERVENANT_CODE',
    ],
];

//@formatter:on
