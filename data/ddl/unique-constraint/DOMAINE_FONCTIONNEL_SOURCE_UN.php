<?php

//@formatter:off

return [
    'name'    => 'DOMAINE_FONCTIONNEL_SOURCE_UN',
    'table'   => 'DOMAINE_FONCTIONNEL',
    'index'   => 'DOMAINE_FONCTIONNEL_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
