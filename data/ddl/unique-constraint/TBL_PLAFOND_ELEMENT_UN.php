<?php

//@formatter:off

return [
    'name'    => 'TBL_PLAFOND_ELEMENT_UN',
    'table'   => 'TBL_PLAFOND_ELEMENT',
    'index'   => 'TBL_PLAFOND_ELEMENT_UN',
    'columns' => [
        'ELEMENT_PEDAGOGIQUE_ID',
        'TYPE_VOLUME_HORAIRE_ID',
        'INTERVENANT_ID',
        'ANNEE_ID',
        'PLAFOND_ID',
    ],
];

//@formatter:on
