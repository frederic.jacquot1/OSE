<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_EP__UN',
    'table'   => 'CENTRE_COUT_EP',
    'index'   => 'CENTRE_COUT_EP__UN',
    'columns' => [
        'CENTRE_COUT_ID',
        'ELEMENT_PEDAGOGIQUE_ID',
        'TYPE_HEURES_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
