<?php

//@formatter:off

return [
    'name'    => 'AFFECTATION__UN',
    'table'   => 'AFFECTATION',
    'index'   => 'AFFECTATION__UN',
    'columns' => [
        'ROLE_ID',
        'STRUCTURE_ID',
        'HISTO_DESTRUCTION',
        'UTILISATEUR_ID',
    ],
];

//@formatter:on
