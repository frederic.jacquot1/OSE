<?php

//@formatter:off

return [
    'name'    => 'EFFECTIFS_ETAPE_SOURCE_UN',
    'table'   => 'EFFECTIFS_ETAPE',
    'index'   => 'EFFECTIFS_ETAPE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
