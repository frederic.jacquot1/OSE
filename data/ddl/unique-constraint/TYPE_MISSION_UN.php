<?php

//@formatter:off

return [
    'name'    => 'TYPE_MISSION_UN',
    'table'   => 'TYPE_MISSION',
    'index'   => 'TYPE_MISSION_UN',
    'columns' => [
        'CODE',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
