<?php

//@formatter:off

return [
    'name'    => 'CENTRE_COUT_STRUCTURE_UN',
    'table'   => 'CENTRE_COUT_STRUCTURE',
    'index'   => 'CENTRE_COUT_STRUCTURE_UN',
    'columns' => [
        'CENTRE_COUT_ID',
        'STRUCTURE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
