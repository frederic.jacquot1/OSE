<?php

//@formatter:off

return [
    'name'    => 'PERIMETRE_CODE_UN',
    'table'   => 'PERIMETRE',
    'index'   => 'PERIMETRE_CODE_UN',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
