<?php

//@formatter:off

return [
    'name'    => 'SOURCE_CODE_UN',
    'table'   => 'SOURCE',
    'index'   => 'SOURCE_CODE_UN',
    'columns' => [
        'CODE',
    ],
];

//@formatter:on
