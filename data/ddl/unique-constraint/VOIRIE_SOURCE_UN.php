<?php

//@formatter:off

return [
    'name'    => 'VOIRIE_SOURCE_UN',
    'table'   => 'VOIRIE',
    'index'   => 'VOIRIE_SOURCE_UN',
    'columns' => [
        'SOURCE_CODE',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
