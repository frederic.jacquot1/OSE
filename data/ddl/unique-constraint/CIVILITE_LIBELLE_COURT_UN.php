<?php

//@formatter:off

return [
    'name'    => 'CIVILITE_LIBELLE_COURT_UN',
    'table'   => 'CIVILITE',
    'index'   => 'CIVILITE_LIBELLE_COURT_UN',
    'columns' => [
        'LIBELLE_COURT',
    ],
];

//@formatter:on
