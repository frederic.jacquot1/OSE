<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_STRUCTURE_UN',
    'table'   => 'PLAFOND_STRUCTURE',
    'index'   => 'PLAFOND_STRUCTURE_UN',
    'columns' => [
        'STRUCTURE_ID',
        'PLAFOND_ID',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
