<?php

//@formatter:off

return [
    'name'    => 'TI_STATUT_STATUT_UN',
    'table'   => 'TYPE_INTERVENTION_STATUT',
    'index'   => 'TI_STATUT_STATUT_UN',
    'columns' => [
        'TYPE_INTERVENTION_ID',
        'STATUT_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
