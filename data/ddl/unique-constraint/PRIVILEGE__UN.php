<?php

//@formatter:off

return [
    'name'    => 'PRIVILEGE__UN',
    'table'   => 'PRIVILEGE',
    'index'   => 'PRIVILEGE__UN',
    'columns' => [
        'CATEGORIE_ID',
        'CODE',
    ],
];

//@formatter:on
