<?php

//@formatter:off

return [
    'name'    => 'ELEMENT_MODULATEUR__UN',
    'table'   => 'ELEMENT_MODULATEUR',
    'index'   => 'ELEMENT_MODULATEUR__UN',
    'columns' => [
        'ELEMENT_ID',
        'MODULATEUR_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
