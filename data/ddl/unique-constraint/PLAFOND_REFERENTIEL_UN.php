<?php

//@formatter:off

return [
    'name'    => 'PLAFOND_REFERENTIEL_UN',
    'table'   => 'PLAFOND_REFERENTIEL',
    'index'   => 'PLAFOND_REFERENTIEL_UN',
    'columns' => [
        'FONCTION_REFERENTIEL_ID',
        'PLAFOND_ID',
        'ANNEE_ID',
        'HISTO_DESTRUCTION',
    ],
];

//@formatter:on
