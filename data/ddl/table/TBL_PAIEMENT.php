<?php

//@formatter:off

return [
    'name'        => 'TBL_PAIEMENT',
    'temporary'   => FALSE,
    'logging'     => FALSE,
    'commentaire' => 'Données liées aux paiements et demandes de mises en paiement',
    'sequence'    => 'TBL_PAIEMENT_ID_SEQ',
    'columns'     => [
        'ANNEE_ID'                   => [
            'name'        => 'ANNEE_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => FALSE,
            'default'     => NULL,
            'position'    => 2,
            'commentaire' => NULL,
        ],
        'DOMAINE_FONCTIONNEL_ID'     => [
            'name'        => 'DOMAINE_FONCTIONNEL_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 12,
            'commentaire' => NULL,
        ],
        'FORMULE_RES_SERVICE_ID'     => [
            'name'        => 'FORMULE_RES_SERVICE_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 5,
            'commentaire' => NULL,
        ],
        'FORMULE_RES_SERVICE_REF_ID' => [
            'name'        => 'FORMULE_RES_SERVICE_REF_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 6,
            'commentaire' => NULL,
        ],
        'HEURES_AA'                  => [
            'name'        => 'HEURES_AA',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '0',
            'position'    => 19,
            'commentaire' => NULL,
        ],
        'HEURES_AC'                  => [
            'name'        => 'HEURES_AC',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '0',
            'position'    => 20,
            'commentaire' => NULL,
        ],
        'HEURES_A_PAYER'             => [
            'name'        => 'HEURES_A_PAYER',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '0',
            'position'    => 13,
            'commentaire' => 'HETD à payer',
        ],
        'HEURES_A_PAYER_POND'        => [
            'name'        => 'HEURES_A_PAYER_POND',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '0',
            'position'    => 14,
            'commentaire' => 'HETD à payer (en %)',
        ],
        'HEURES_DEMANDEES'           => [
            'name'        => 'HEURES_DEMANDEES',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '0',
            'position'    => 15,
            'commentaire' => 'HETD demandées',
        ],
        'HEURES_PAYEES'              => [
            'name'        => 'HEURES_PAYEES',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '0',
            'position'    => 16,
            'commentaire' => 'HETD payées',
        ],
        'ID'                         => [
            'name'        => 'ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => FALSE,
            'default'     => NULL,
            'position'    => 1,
            'commentaire' => NULL,
        ],
        'INTERVENANT_ID'             => [
            'name'        => 'INTERVENANT_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => FALSE,
            'default'     => NULL,
            'position'    => 8,
            'commentaire' => NULL,
        ],
        'MISE_EN_PAIEMENT_ID'        => [
            'name'        => 'MISE_EN_PAIEMENT_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 10,
            'commentaire' => NULL,
        ],
        'MISSION_ID'                 => [
            'name'        => 'MISSION_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 7,
            'commentaire' => NULL,
        ],
        'PERIODE_PAIEMENT_ID'        => [
            'name'        => 'PERIODE_PAIEMENT_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 11,
            'commentaire' => NULL,
        ],
        'POURC_EXERCICE_AA'          => [
            'name'        => 'POURC_EXERCICE_AA',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '4/10',
            'position'    => 17,
            'commentaire' => NULL,
        ],
        'POURC_EXERCICE_AC'          => [
            'name'        => 'POURC_EXERCICE_AC',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '6/10',
            'position'    => 18,
            'commentaire' => NULL,
        ],
        'SERVICE_ID'                 => [
            'name'        => 'SERVICE_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 3,
            'commentaire' => NULL,
        ],
        'SERVICE_REFERENTIEL_ID'     => [
            'name'        => 'SERVICE_REFERENTIEL_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 4,
            'commentaire' => NULL,
        ],
        'STRUCTURE_ID'               => [
            'name'        => 'STRUCTURE_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => '0',
            'precision'   => NULL,
            'nullable'    => FALSE,
            'default'     => NULL,
            'position'    => 9,
            'commentaire' => NULL,
        ],
        'TAUX_CONGES_PAYES'          => [
            'name'        => 'TAUX_CONGES_PAYES',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '1',
            'position'    => 23,
            'commentaire' => NULL,
        ],
        'TAUX_HORAIRE'               => [
            'name'        => 'TAUX_HORAIRE',
            'type'        => 'float',
            'bdd-type'    => 'FLOAT',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => 126,
            'nullable'    => FALSE,
            'default'     => '1',
            'position'    => 22,
            'commentaire' => NULL,
        ],
        'TAUX_REMU_ID'               => [
            'name'        => 'TAUX_REMU_ID',
            'type'        => 'int',
            'bdd-type'    => 'NUMBER',
            'length'      => 0,
            'scale'       => NULL,
            'precision'   => NULL,
            'nullable'    => TRUE,
            'default'     => NULL,
            'position'    => 21,
            'commentaire' => NULL,
        ],
    ],
];

//@formatter:on
