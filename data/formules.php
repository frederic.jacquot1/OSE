<?php

return [
    2  => [
        'LIBELLE'      => 'Université de Montpellier',
        'PACKAGE_NAME' => 'FORMULE_MONTPELLIER',
    ],
    3  => [
        'LIBELLE'            => 'Université Le Havre Normandie',
        'PACKAGE_NAME'       => 'FORMULE_ULHN',
        'VH_PARAM_1_LIBELLE' => 'Code enseignement',
    ],
    4  => [
        'LIBELLE'      => 'Université de Nanterre',
        'PACKAGE_NAME' => 'FORMULE_NANTERRE',
    ],
    5  => [
        'LIBELLE'           => 'Université de Bretagne Occidentale',
        'PACKAGE_NAME'      => 'FORMULE_UBO',
        'I_PARAM_1_LIBELLE' => 'EC/Associé/Docteur',
        'I_PARAM_2_LIBELLE' => 'Lecteur/ATER',
    ],
    6  => [
        'LIBELLE'      => 'Université de Strasbourg',
        'PACKAGE_NAME' => 'FORMULE_UNISTRA',
    ],
    7  => [
        'LIBELLE'      => 'Université Lumière Lyon 2',
        'PACKAGE_NAME' => 'FORMULE_LYON2',
    ],
    8  => [
        'LIBELLE'      => 'Université Jean Monnet (Saint-Étienne)',
        'PACKAGE_NAME' => 'FORMULE_ST_ETIENNE',
    ],
    9  => [
        'LIBELLE'      => 'Université Côté d\'azur',
        'PACKAGE_NAME' => 'FORMULE_COTE_AZUR',
    ],
    10 => [
        'LIBELLE'            => 'Université Rennes 2',
        'PACKAGE_NAME'       => 'FORMULE_RENNES2',
        'VH_PARAM_1_LIBELLE' => 'Maquettes Apogée',
        'VH_PARAM_2_LIBELLE' => 'Nbre etudiants',
    ],
    11 => [
        'LIBELLE'      => 'INSA de Lyon',
        'PACKAGE_NAME' => 'FORMULE_INSA_LYON',
    ],
    12 => [
        'LIBELLE'      => 'Université de Poitiers',
        'PACKAGE_NAME' => 'FORMULE_POITIERS',
    ],
    13 => [
        'LIBELLE'            => 'Université Paris 8',
        'PACKAGE_NAME'       => 'FORMULE_PARIS8',
        'VH_PARAM_1_LIBELLE' => 'Code EP',
    ],
    14 => [
        'LIBELLE'      => 'Université d\'Artois',
        'PACKAGE_NAME' => 'FORMULE_ARTOIS',
    ],
    15 => [
        'LIBELLE'      => 'Université de Paris',
        'PACKAGE_NAME' => 'FORMULE_PARIS',
    ],
    16 => [
        'LIBELLE'            => 'Université de Lille',
        'PACKAGE_NAME'       => 'FORMULE_LILLE',
        'VH_PARAM_2_LIBELLE' => 'Code fonction Référentiel',
    ],
    17 => [
        'LIBELLE'      => 'Université de la Réunion',
        'PACKAGE_NAME' => 'FORMULE_REUNION',
    ],
    18 => [
        'LIBELLE'      => 'Université Sorbonne Nouvelle',
        'PACKAGE_NAME' => 'FORMULE_SORBONNE_NOUVELLE',
    ],
    19 => [
        'LIBELLE'      => 'Université de Caen',
        'PACKAGE_NAME' => 'FORMULE_UNICAEN',
    ],
    20 => [
        'LIBELLE'           => 'Université Paris-Est Créteil',
        'PACKAGE_NAME'      => 'FORMULE_UPEC',
        'I_PARAM_1_LIBELLE' => 'EC/Associé/Docteur',
        'I_PARAM_2_LIBELLE' => 'Lecteur/ATER',
    ],
    21 => [
        'LIBELLE'      => 'Université De Guyane',
        'PACKAGE_NAME' => 'FORMULE_GUYANE',
    ],
    22 => [
        'LIBELLE'      => 'Université Rennes 1',
        'PACKAGE_NAME' => 'FORMULE_RENNES1',
    ],
    23 => [
        'LIBELLE'      => 'Université de Versailles Saint-Quentin-en-Yvelines',
        'PACKAGE_NAME' => 'FORMULE_UVSQ',
    ],
    24 => [
        'LIBELLE'      => 'Université Paris-Saclay',
        'PACKAGE_NAME' => 'FORMULE_SACLAY',
    ],
    25 => [
        'LIBELLE'            => 'Université de ROUEN',
        'PACKAGE_NAME'       => 'FORMULE_ROUEN',
        'VH_PARAM_1_LIBELLE' => 'Type de référentiel',
    ],
    26 => [
        'LIBELLE'            => 'Université de Picardie Jules Verne',
        'PACKAGE_NAME'       => 'FORMULE_PICARDIE',
    ],
    27 => [
        'LIBELLE'            => 'Université de Paris 1 Panthéon-Sorbonne',
        'PACKAGE_NAME'       => 'FORMULE_PARIS1',
        'I_PARAM_1_LIBELLE'  => 'Statut',
        'VH_PARAM_1_LIBELLE' => 'Heures DU',
        'VH_PARAM_2_LIBELLE' => 'Code référentiel',
    ],
];